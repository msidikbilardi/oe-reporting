<?php
session_start();
$conn_string = "host=".$_SESSION['host']." dbname=".$_POST['db']." user=".$_SESSION['user_db']." password=".$_SESSION['pass_db']."";
$dbconn = pg_connect($conn_string);
$c = pg_query($dbconn,"SELECT * FROM res_company");
$comp = pg_fetch_array($c);
$db = $_POST['db'];
$start = $_POST['date_start'];
$end =  $_POST['date_end'];
$type =  $_POST['type'];
$state =  $_POST['state'];
$so = $_POST['so'];
$type_report = $_POST['type_report'];

$type_string = "Document : Sales Order Category Salesperson Report";


if($type_report == 'so'){
	$state_sales = "AND A.state != 'cancel' AND A.state != 'draft' AND A.state != 'sent'";
	$string_state = 'ALL SALES ORDER';
	$filter = "AND B.state != 'cancel' AND B.state != 'draft' AND B.state != 'sent'";
}else{
	$state_sales = "AND A.state != 'cancel' AND (A.state = 'draft' OR A.state = 'sent')";
	$string_state = 'DRAFT AND SENT QUOTATION';	
	$filter = "AND B.state != 'cancel' AND (B.state = 'draft' or B.state = 'sent' )";
}
$no = 1;

	define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
		
		require_once '../assets/Classes/PHPExcel.php';
		/******************END DATE ADDITIONAL FILTER*************************/
		
		$objPHPExcel = new PHPExcel();

		$objPHPExcel->getProperties()->setCreator("Sidik Bilardi")
									 ->setLastModifiedBy("Sidik Bilardi")
									 ->setTitle("Laporan Penjualan")
									 ->setSubject("Laporan Penjualan")
									 ->setDescription("Delta Report")
									 ->setKeywords("office PHPExcel php")
									 ->setCategory("Laporan Penjualan Seluruh Area");
									 
		$objPHPExcel->getDefaultStyle()
					->getAlignment()
					->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);	
		$objPHPExcel->setActiveSheetIndex(0)
							->setCellValue('A1', $comp['name'])
							->setCellValue('A2', $comp['msb_po_address_1'])
							->setCellValue('A3', $comp['msb_po_address_2'])
							->setCellValue('A4', $comp['msb_po_phone'])
							->setCellValue('A5', $comp['msb_po_city'])
							->setCellValue('A6', $comp['msb_po_email'])
							->setCellValue('A8', $type_string)
							// ->setCellValue('E8', $info)
							->setCellValue('A9', $string_state)
							->setCellValue('E9', 'Invoice Date : '.$start.' to '.$end)							
							;

		$row = 10;
		$sum = 0;
		$sumall = 0;

	//	$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':F'.$row)->getFont()->setBold(true);			
		$l = pg_query($dbconn,"SELECT B.login,B.id FROM sale_order A,res_users B WHERE A.user_id = B.id  $state_sales AND A.date_order >= '$start' AND A.date_order <= '$end' GROUP by B.id ORDER BY B.login ");
		while($list = pg_fetch_assoc($l)){
			$subtotal = 0;

			$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':F'.$row)->getFont()->setBold(true);			
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('B'.$row, $no)
				->setCellValue('C'.$row, $list['login'])
				->setCellValue('E'.$row, '')
				;
			$row++;
			$no++;
		    $q = "SELECT W.id,W.name,sum(Z.sm_price_unit_after_disc3 * Z.product_uom_qty) as after3,sum(Z.sm_price_unit_before_disc * Z.product_uom_qty) as no_categ FROM(
		            SELECT A.*,B.name,B.date_order FROM sale_order_line A,sale_order B where A.order_id = B.id $filter AND B.date_order BETWEEN '$start 00:00:00' AND '$end 23:59:59' AND B.user_id = '".$list['id']."'
		            ) Z 
		            LEFT JOIN (select id,default_code,product_tmpl_id from product_product) Y ON Z.product_id = Y.id
		            LEFT JOIN (SELECT id,categ_id FROM product_template) X ON Y.product_tmpl_id = X.id
		            LEFT JOIN product_category W ON W.id = X.categ_id
		            GROUP BY W.id ORDER BY W.name";
		    $m = pg_query($dbconn,$q);	
		    $sum = 0;
		    while($main = pg_fetch_assoc($m)){
		        if($main['id']){
		          $nm = $main['name'];
		          $nom = $main['after3'];
		        }else{
		          $nm = 'No Category';
		          $nom = $main['no_categ'];
		        }

		        $objPHPExcel->setActiveSheetIndex(0)
		                      ->setCellValue('C'.$row, $nm)           
		                      ->setCellValue('D'.$row, round($nom))           
		                      ;
		        
		        $row++;
		        
		        $sum += round($nom);

		    }	
		    $objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('B'.$row, 'Subtotal')
			 ->setCellValue('D'.$row, $sum)
			;
			$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':F'.$row)->getFont()->setBold(true);		    		
			$row++;
			$row++;
			$sumall += $sum;
		}
				
		$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A'.$row, 'SUMMARY ALL')
			 ->setCellValue('E'.$row, $sumall)
			;
			$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':F'.$row)->getFont()->setBold(true);		    		
			


$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(5);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
					
$filename = "Category SO Salesperson Report ";
// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;
