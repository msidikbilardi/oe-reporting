<?php
session_start();
$conn_string = "host=".$_SESSION['host']." dbname=".$_POST['db']." user=".$_SESSION['user_db']." password=".$_SESSION['pass_db']."";
$dbconn = pg_connect($conn_string);
$c = pg_query($dbconn,"SELECT * FROM res_company");
$comp = pg_fetch_array($c);
$db = $_POST['db'];
$start = $_POST['date_start'];
$end =  $_POST['date_end'];
$cat_parent =  $_POST['categ_parent'];
$cat =  $_POST['categ'];
$parent =  $_POST['parent_field'];

$no = 1;
if($parent == 'on'){
	$val = 'where Y.parent_id';
	$category = $cat_parent;
	$prt = 'Parent';
	$filter_prn = $val.'='.$category;
	$t = pg_query($dbconn,"SELECT * FROM res_partner_category where id='$category' ");
	$tags	= pg_fetch_array($t);			
	$info = 'Company Category'.$prt.' : '.$tags['name'];
	$filter = '';

}elseif($parent != 'on' && $cat != ''){
	$val = 'AND a.msb_category';
	$category = $cat;
	$prt = '';
	$filter = $val.'='.$category;
	$t = pg_query($dbconn,"SELECT * FROM res_partner_category where id='$category' ");
	$tags	= pg_fetch_array($t);
	$info = 'Company Category'.$prt.' : '.$tags['name'];	
	$filter_prn = '';
}else{
	$filter = '';
	$filter_prn = '';
	$info = 'No Company Category';
}

	$state_list = "b.state != 'cancel' AND b.state != 'draft' AND b.state != 'sent'";
	$type_string = "Document : Category SO Report";
	$string_state = 'ALL SALES ORDER';
	$row = 11;
	$sum = 0;


	define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
		
		require_once '../assets/Classes/PHPExcel.php';
		/******************END DATE ADDITIONAL FILTER*************************/
		
		$objPHPExcel = new PHPExcel();

		$objPHPExcel->getProperties()->setCreator("Sidik Bilardi")
									 ->setLastModifiedBy("Sidik Bilardi")
									 ->setTitle("Laporan Penjualan")
									 ->setSubject("Laporan Penjualan")
									 ->setDescription("Delta Report")
									 ->setKeywords("office PHPExcel php")
									 ->setCategory("Laporan Penjualan Seluruh Area");
									 
		$objPHPExcel->getDefaultStyle()
					->getAlignment()
					->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);	
		$objPHPExcel->setActiveSheetIndex(0)
							->setCellValue('A1', $comp['name'])
							->setCellValue('A2', $comp['msb_po_address_1'])
							->setCellValue('A3', $comp['msb_po_address_2'])
							->setCellValue('A4', $comp['msb_po_phone'])
							->setCellValue('A5', $comp['msb_po_city'])
							->setCellValue('A6', $comp['msb_po_email'])
							->setCellValue('A8', $type_string)
							->setCellValue('E8', $info)
							->setCellValue('A9', $string_state)
							->setCellValue('E9', 'Invoice Date : '.$start.' to '.$end)							
							;

	    $q = "select * from (SELECT a.msb_category,a.name,COALESCE(sum(amount_untaxed),'0') as total, a.id
	        FROM res_partner a, purchase_order b
	        WHERE b.currency_id = '13'  AND b.date_order >= '$start' AND b.date_order <= '$end'  $filter  AND $state_list AND b.partner_id = a.id 
	        GROUP BY  a.name,a.id) Z LEFT JOIN (select id as cat_id,name as name_cat,parent_id from res_partner_category) Y ON Z.msb_category = Y.cat_id $filter_prn";


		$l = pg_query($dbconn,$q);
		while($list = pg_fetch_assoc($l)){ 
		$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A'.$row, $no)
			->setCellValue('B'.$row, $list['name'])
			->setCellValue('F'.$row, round($list['total']))
			;
			$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':F'.$row)->getFont()->setBold(true);			
			$row++;
			$q2 ="
				SELECT name,SUM(total) as final,
				SUM(not_diskon) as not_diskon 
				FROM
				(SELECT A.product_id ,sum(A.product_qty*A.sm_price_unit_after_disc3) as total,SUM(A.product_qty*A.price_unit) as not_diskon FROM purchase_order_line A,purchase_order B,res_partner F where B.partner_id = F.id AND B.date_order   >= '$start' AND B.date_order  <= '$end' AND B.date_order IS NOT NULL  AND B.currency_id = '13' AND  F.id='".$list['id']."' AND $state_list AND A.order_id = B.id group by A.product_id) Z LEFT JOIN (SELECT A.id,C.name,A.default_code,A.name_template,A.product_tmpl_id FROM product_product A,product_template B,product_category C WHERE B.categ_id = C.id AND A.product_tmpl_id = B.id) Y ON Z.product_id = Y.id GROUP BY name";

/*			$q2 = "
                select COALESCE(sum(A.sm_price_unit_after_disc3 * quantity),'0') as final,D.name 
                FROM res_partner F,account_invoice E,account_invoice_line A, product_product B,product_template C,product_category D
	        	WHERE E.partner_id = F.id AND $state_dt AND C.categ_id = D.id AND A.product_id = B.id AND B.product_tmpl_id = C.id AND E.partner_id='".$list['id']."' AND A.invoice_id = E.id AND E.currency_id = '13' AND E.date_invoice >= '$start' AND E.date_invoice <= '$end' AND E.type = '$type' GROUP BY D.name ORDER BY D.name";
*/
			 $d = pg_query($dbconn,$q2);
            while($dt = pg_fetch_assoc($d)){ 	
            	if($dt['name'] == ''){
            		$nom = $dt['not_diskon'];
            		 $sum += $dt['not_diskon'];
            		 $nm = 'No Product ID';
            	}else{
            		$nom = $dt['final'];
            		 $sum += $dt['final'];
            		 $nm = $dt['name'];
            	}

				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('C'.$row, $nm)
					->setCellValue('E'.$row, round($nom))
					;
            
            $row++;
            }	
       // $sum += round($list['total']);
		$row++;
		$no++;
		}
		$row_end = $row-1;
		$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('B'.$row, 'SUMMARY')
			->setCellValue('F'.$row, '=SUM(F11:F'.$row_end.')')
			;
			$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':F'.$row)->getFont()->setBold(true);			


$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
					
$filename = "Category Detail SO Report ";
// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;
