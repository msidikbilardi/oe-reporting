<?php
session_start();
$conn_string = "host=".$_SESSION['host']." dbname=".$_POST['db']." user=".$_SESSION['user_db']." password=".$_SESSION['pass_db']."";
$dbconn = pg_connect($conn_string);
$c = pg_query($dbconn,"SELECT * FROM res_company");
$comp = pg_fetch_array($c);
$db = $_POST['db'];
$start = $_POST['date_start'];
$end =  $_POST['date_end'];
$cat_parent =  $_POST['categ_parent'];
$cat =  $_POST['categ'];
$parent =  $_POST['parent_field'];
$type_report =  $_POST['type_report'];


$no = 1;
if($parent == 'on'){
	$val = 'where Y.parent_id';
	$category = $cat_parent;
	$prt = 'Parent';
	$filter_prn = $val.'='.$category;
	$t = pg_query($dbconn,"SELECT * FROM res_partner_category where id='$category' ");
	$tags	= pg_fetch_array($t);			
	$info = 'Company Category'.$prt.' : '.$tags['name'];
	$filter = '';

}elseif($parent != 'on' && $cat != ''){
	$val = 'AND a.msb_category';
	$category = $cat;
	$prt = '';
	$filter = $val.'='.$category;
	$t = pg_query($dbconn,"SELECT * FROM res_partner_category where id='$category' ");
	$tags	= pg_fetch_array($t);
	$info = 'Company Category'.$prt.' : '.$tags['name'];	
	$filter_prn = '';
}else{
	$filter = '';
	$filter_prn = '';
	$info = 'No Company Category';
}

	//$state_list = "b.state != 'cancel' AND b.state != 'draft' AND b.state != 'sent'";
	//$type_string = "Document : Category SO Report";
	//$string_state = 'ALL SALES ORDER';

  if($type_report == 'so'){
    $state_list = "b.state != 'cancel' AND b.state != 'draft' AND b.state != 'sent'";
  
    $state_dt = "AND state != 'cancel' AND state != 'draft' AND state != 'sent'";
    $string_state = 'ALL SALES ORDER';
    $type_string = "Document : Sales Order Category Salesperson Report";

  }else{
    $state_list = "b.state != 'cancel' AND (b.state = 'draft' OR b.state = 'sent') ";

    $state_dt = "AND state != 'cancel' AND (state = 'draft' OR state != 'sent')";
    $string_state = 'DRAFT AND SENT QUOTATION';
    $type_string = "Document : Quotation Category Salesperson Report";
  }

if($type_report == 'so'){
  $filter_detail = "AND B.state != 'cancel' AND B.state != 'draft' AND B.state != 'sent'";
}else{
  $filter_detail = "AND B.state != 'cancel' AND (B.state = 'draft' or B.state = 'sent' )";
}
	$row = 11;
	$sum = 0;


	define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
		
		require_once '../assets/Classes/PHPExcel.php';
		/******************END DATE ADDITIONAL FILTER*************************/
		
		$objPHPExcel = new PHPExcel();

		$objPHPExcel->getProperties()->setCreator("Sidik Bilardi")
									 ->setLastModifiedBy("Sidik Bilardi")
									 ->setTitle("Laporan Penjualan")
									 ->setSubject("Laporan Penjualan")
									 ->setDescription("Delta Report")
									 ->setKeywords("office PHPExcel php")
									 ->setCategory("Laporan Penjualan Seluruh Area");
									 
		$objPHPExcel->getDefaultStyle()
					->getAlignment()
					->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);	
		$objPHPExcel->setActiveSheetIndex(0)
							->setCellValue('A1', $comp['name'])
							->setCellValue('A2', $comp['msb_po_address_1'])
							->setCellValue('A3', $comp['msb_po_address_2'])
							->setCellValue('A4', $comp['msb_po_phone'])
							->setCellValue('A5', $comp['msb_po_city'])
							->setCellValue('A6', $comp['msb_po_email'])
							->setCellValue('A8', $type_string)
							
							->setCellValue('A9', $string_state)
							->setCellValue('D9', 'Date : '.$start.' to '.$end)							
							;

	    $q = "select * from (SELECT a.msb_category,a.name,COALESCE(sum(amount_untaxed),'0') as total, a.id
	        FROM res_partner a, sale_order b
	        WHERE b.date_order >= '$start' AND b.date_order <= '$end'  $filter  AND $state_list AND b.partner_id = a.id 
	        GROUP BY  a.name,a.id) Z LEFT JOIN (select id as cat_id,name as name_cat,parent_id from res_partner_category) Y ON Z.msb_category = Y.cat_id $filter_prn";

        $gt = 0;
        $no = 1;
		$l = pg_query($dbconn,$q);
		while($list = pg_fetch_assoc($l)){ 
		$sum = 0;
		$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A'.$row, $no)
			->setCellValue('B'.$row, $list['name'])
			;
			$row++;
		      $q = "SELECT W.id,W.name,sum(Z.sm_price_unit_after_disc3 * Z.product_uom_qty) as after3,sum(Z.sm_price_unit_before_disc * Z.product_uom_qty) as no_categ FROM(
		            SELECT A.*,B.name,B.date_order FROM sale_order_line A,sale_order B where A.order_id = B.id $filter_detail AND B.date_order BETWEEN '$start 00:00:00' AND '$end 23:59:59' AND B.partner_id = '".$list['id']."'
		            ) Z 
		            LEFT JOIN (select id,default_code,product_tmpl_id from product_product) Y ON Z.product_id = Y.id
		            LEFT JOIN (SELECT id,categ_id FROM product_template) X ON Y.product_tmpl_id = X.id
		            LEFT JOIN product_category W ON W.id = X.categ_id
		            GROUP BY W.id ORDER BY W.name";
		      $m = pg_query($dbconn,$q);
		      //echo $q;
		      
		      
		      while($main = pg_fetch_assoc($m)){
		        if($main['id']){
		          $nm = $main['name'];
		          $nom = $main['after3'];
		        }else{
		          $nm = 'No Category';
		          $nom = $main['no_categ'];
		        }

		        $objPHPExcel->setActiveSheetIndex(0)
		                      
		                      ->setCellValue('B'.$row, $nm)           
		                      ->setCellValue('C'.$row, round($nom))           
		                      ;
		        
		        $row++;
		       
		        $sum += $nom;
		      }
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('B'.$row, 'Sub Total')
				 ->setCellValue('C'.$row, round($sum))
				;
			$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':F'.$row)->getFont()->setBold(true);			
			$row++;
			$row++;
			$no++;
			$gt += $sum;
		}
		$row_end = $row-1;
		$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('B'.$row, 'SUMMARY')
			 ->setCellValue('D'.$row, round($gt))
			;
			$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':F'.$row)->getFont()->setBold(true);			


$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
					
$filename = "Category Detail SO Report ";
// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;
