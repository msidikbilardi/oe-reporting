<?php
$id = $_GET['id'];
session_start();
include"connect_pg.php";
define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

require_once '../assets/Classes/PHPExcel.php';
/******************END DATE ADDITIONAL FILTER*************************/

$objPHPExcel = new PHPExcel();
		$objPHPExcel->getDefaultStyle()
					->getAlignment()
					->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);	

/*		$objPHPExcel->setActiveSheetIndex(0)
							->setCellValue('A1', 'FK')						
							->setCellValue('B1', 'KD_JENIS')						
							->setCellValue('C1', 'FG_PENGGANTI')
							->setCellValue('D1', 'NOMOR_FAKTUR')						
							->setCellValue('E1', 'MASA_PAJAK')						
							->setCellValue('F1', 'TAHUN_PAJAK')						
							->setCellValue('G1', 'TANGGAL_FAKTUR')						
							->setCellValue('H1', 'NPWP')						
							->setCellValue('I1', 'NAMA')						
							->setCellValue('J1', 'ALAMAT_LENGKAP')						
							->setCellValue('K1', 'JUMLAH_DPP')						
							->setCellValue('L1', 'JUMLAH_PPN')						
							->setCellValue('M1', 'JUMLAH_PPNBM')						
							->setCellValue('N1', 'ID_KETERANGAN_TAMBAHAN')
							->setCellValue('O1', 'FG_UANG_MUKA')						
							->setCellValue('P1', 'UANG_MUKA_DPP')						
							->setCellValue('Q1', 'UANG_MUKA_PPN')						
							->setCellValue('R1', 'UANG_MUKA_PPNBM')						
							->setCellValue('S1', 'REFERENSI')						
							;

		$objPHPExcel->setActiveSheetIndex(0)
							->setCellValue('A2', 'LT')						
							->setCellValue('B2', 'NPWP')						
							->setCellValue('C2', 'NAMA')
							->setCellValue('D2', 'JALAN')						
							->setCellValue('E2', 'BLOK')						
							->setCellValue('F2', 'NOMOR')						
							->setCellValue('G2', 'RT')						
							->setCellValue('H2', 'RW')						
							->setCellValue('I2', 'KECAMATAN')						
							->setCellValue('J2', 'KELURAHAN')						
							->setCellValue('K2', 'KABUPATEN')						
							->setCellValue('L2', 'PROPINSI')						
							->setCellValue('M2', 'KODE_POS')						
							->setCellValue('N2', 'NOMOR_TELEPON')				
							;
		$objPHPExcel->setActiveSheetIndex(0)
							->setCellValue('A3', 'OF')						
							->setCellValue('B3', 'KODE_OBJEK')						
							->setCellValue('C3', 'NAMA')
							->setCellValue('D3', 'HARGA_SATUAN')						
							->setCellValue('E3', 'JUMLAH_BARANG')						
							->setCellValue('F3', 'HARGA_TOTAL')						
							->setCellValue('G3', 'DISKON')						
							->setCellValue('H3', 'DPP')						
							->setCellValue('I3', 'PPN')						
							->setCellValue('J3', 'TARIK_PPNBM')				
							;
*/


$row = 4;
$query = "SELECT * FROM(SELECT number AS value, id AS data,date_invoice,date_due,company_id FROM account_invoice WHERE state ='open' and number = '$id' LIMIT 5) A
	LEFT JOIN smcus_faktur_pajak B ON A.data = B.invoice_id
	LEFT JOIN (SELECT id,name as partner,street,street2 from res_partner) C ON B.partnerid = C.id
	LEFT JOIN (SELECT id,name as comp_name,street as comp_street,street2 as comp_street2,city as comp_city from res_partner) D ON D.id = A.company_id
";

$submit = pg_query($query);	
while ($result = pg_fetch_assoc($submit)) 
{
	$inv_number = $result['value'];
	$inv_date = $result['date_invoice'];
	$due_date = $result['date_due'];
	$no_faktur = $result['name'];
	$partner = $result['partner'];
	$faktur_date = $result['date_faktur'];
	$masa = $result['masa_pajak'];
	$tahun = $result['tahun_pajak'];
	$npwp = $result['npwp'];
	$ttd = $result['ttd'];

/*	$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A'.$row, 'FK')	
				->setCellValue('B'.$row, $result['kode_transaksi'])	
				->setCellValue('C'.$row, $result['jenis_dokumen'])	
				->setCellValue('D'.$row, $no_faktur)	
				->setCellValue('E'.$row, $masa)	
				->setCellValue('F'.$row, $tahun)	
				->setCellValue('G'.$row, $faktur_date)	
				->setCellValue('H'.$row, $npwp)	
				->setCellValue('I'.$row, strtoupper($result['partner']))	
				->setCellValue('J'.$row, strtoupper($result['street'].' '.$result['street2']))	
				->setCellValue('K'.$row, round($result['dpp']) )	
				->setCellValue('L'.$row, round($result['ppn']) )	
				;					
*/
	$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A'.$row, "FK,".$result['kode_transaksi'].','.$result['jenis_dokumen'].','.$no_faktur.','.$masa.','.$tahun.','.$faktur_date.','.$npwp.','.strtoupper($result['partner']).','.strtoupper($result['street'].' '.$result['street2']).','.round($result['dpp']).','.round($result['ppn']))	
				;	
	$row++;

/*	$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A'.$row, 'FAPR')	
				->setCellValue('B'.$row, strtoupper($result['comp_name']))	
				->setCellValue('C'.$row, strtoupper($result['comp_street'].' '.$result['comp_street2'].' '.$result['comp_city']))	
				->setCellValue('D'.$row, $ttd)	
				->setCellValue('E'.$row, $result['comp_city'])	
				->setCellValue('F'.$row, $tahun)	
				->setCellValue('G'.$row, $faktur_date)	
				->setCellValue('H'.$row, $npwp)	
				->setCellValue('I'.$row, strtoupper($result['partner']))	
				->setCellValue('J'.$row, strtoupper($result['street'].' '.$result['street2']))	
				->setCellValue('K'.$row, round($result['dpp']) )	
				->setCellValue('L'.$row, round($result['ppn']) )	
				;					
	$row++;	
	*/
	$q = "SELECT * FROM (SELECT id,name,product_id,quantity,smcus_is_not_diskon,sm_price_unit_after_disc3,price_unit,sm_disc1_amount,sm_disc2_amount,sm_disc3_amount FROM account_invoice_line where invoice_id = '".$result['data']."' ) A
		LEFT JOIN (select id,default_code from product_product) B ON A.product_id = B.id
		LEFT JOIN account_invoice_line_tax C ON C.invoice_line_id = A.id 
		LEFT JOIN (select id,type as type_tax,amount as amount_tax from account_tax) D ON D.id = C.tax_id";
	$d = pg_query($q);
	while($det = pg_fetch_assoc($d)){
		if($det['smcus_is_not_diskon'] == 't'){
			$hrg_sat = $det['price_unit'];
		}else{
			$hrg_sat = $det['sm_price_unit_after_disc3'];
		}

		$hrg_ttl = $hrg_sat * $det['quantity'];
		$disc_item = $det['sm_disc1_amount']+$det['sm_disc2_amount']+$det['sm_disc3_amount'];
		if($det['type_tax'] == "percent"){
			$tax_nom = $hrg_ttl * $det['amount_tax'];
		}else{
			$tax_nom = $det['amount_tax'];
		}		
/*		$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A'.$row, 'OF')	
					->setCellValue('B'.$row, $det['default_code'])	
					->setCellValue('C'.$row, $det['name'])	
					->setCellValue('D'.$row, $hrg_sat)	
					->setCellValue('E'.$row, $det['quantity'])	
					->setCellValue('F'.$row, $hrg_ttl)	
					->setCellValue('G'.$row, $disc_item)	
					->setCellValue('H'.$row, $hrg_ttl)	
					->setCellValue('I'.$row, $tax_nom)	
					->setCellValue('J'.$row, '0')	
					->setCellValue('K'.$row, '0')	
					;					
*/
		$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A'.$row, 'OF,'.$det['default_code'].','.$det['name'].','.$hrg_sat.','.$det['quantity'].','.$hrg_ttl.','.$disc_item.','.$hrg_ttl.','.$tax_nom.',0,0')	
					;
		$row++;

	}
}

$filename="efaktur ".$id;
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
ob_end_clean();
// We'll be outputting an excel file
header('Content-type: application/vnd.ms-excel');
// It will be called file.xls
header('Content-Disposition: attachment; filename="'.$filename.'.xlsx"');
$objWriter->save('php://output');