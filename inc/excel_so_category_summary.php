<?php
session_start();
$conn_string = "host=".$_SESSION['host']." dbname=".$_POST['db']." user=".$_SESSION['user_db']." password=".$_SESSION['pass_db']."";
$dbconn = pg_connect($conn_string);
$c = pg_query($dbconn,"SELECT * FROM res_company");
$comp = pg_fetch_array($c);
$db = $_POST['db'];
$start = $_POST['date_start'];
$end =  $_POST['date_end'];
$type =  $_POST['type'];
$state =  $_POST['state'];
$type_report =  $_POST['type_report'];


$no = 1;
$type_string = "Document : Sales Order Category Summary Report";
if($type_report == 'so'){
  $string_state = 'ALL SALES ORDER';
}else{
  $string_state = 'DRAFT AND SENT QUOTATION';
}

if($type_report == 'so'){
  $filter = "AND B.state != 'cancel' AND B.state != 'draft' AND B.state != 'sent'";
}else{
  $filter = "AND B.state != 'cancel' AND (B.state = 'draft' or B.state = 'sent' )";
}

  define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
    
    require_once '../assets/Classes/PHPExcel.php';
    /******************END DATE ADDITIONAL FILTER*************************/
    
    $objPHPExcel = new PHPExcel();

    $objPHPExcel->getProperties()->setCreator("Sidik Bilardi")
                   ->setLastModifiedBy("Sidik Bilardi")
                   ->setTitle("Laporan Penjualan")
                   ->setSubject("Laporan Penjualan")
                   ->setDescription("Delta Report")
                   ->setKeywords("office PHPExcel php")
                   ->setCategory("Laporan Penjualan Seluruh Area");
                   
    $objPHPExcel->getDefaultStyle()
          ->getAlignment()
          ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT); 
    $objPHPExcel->setActiveSheetIndex(0)
              ->setCellValue('A1', $comp['name'])
              ->setCellValue('A2', $comp['msb_po_address_1'])
              ->setCellValue('A3', $comp['msb_po_address_2'])
              ->setCellValue('A4', $comp['msb_po_phone'])
              ->setCellValue('A5', $comp['msb_po_city'])
              ->setCellValue('A6', $comp['msb_po_email'])
              ->setCellValue('A8', $type_string)
              // ->setCellValue('E8', $info)
              ->setCellValue('A9', $string_state)
              ->setCellValue('D9', 'SO Date : '.$start.' to '.$end)             
              ;

    $row = 11;
   
    $sum = 0;
      $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A'.$row,"No")
                    ->setCellValue('B'.$row, "Nama Kategori")           
                    ->setCellValue('C'.$row, "Nominal")           
                    ;
      $objPHPExcel->getActiveSheet()->getStyle('A'.$row.':F'.$row)->getFont()->setBold(true);     
      $row++;
      $row_start = $row;
      $q = "SELECT W.id,W.name,sum(Z.sm_price_unit_after_disc3 * Z.product_uom_qty) as after3,sum(Z.sm_price_unit_before_disc * Z.product_uom_qty) as no_categ FROM(
            SELECT A.*,B.name,B.date_order FROM sale_order_line A,sale_order B where A.order_id = B.id $filter AND B.date_order BETWEEN '$start 00:00:00' AND '$end 23:59:59'
            ) Z 
            LEFT JOIN (select id,default_code,product_tmpl_id from product_product) Y ON Z.product_id = Y.id
            LEFT JOIN (SELECT id,categ_id FROM product_template) X ON Y.product_tmpl_id = X.id
            LEFT JOIN product_category W ON W.id = X.categ_id
            GROUP BY W.id ORDER BY W.name";
      $m = pg_query($dbconn,$q);
      //echo $q;
      $no = 1;
      $sum = 0;
      while($main = pg_fetch_assoc($m)){
        if($main['id']){
          $nm = $main['name'];
          $nom = $main['after3'];
        }else{
          $nm = 'No Category';
          $nom = $main['no_categ'];
        }

        $objPHPExcel->setActiveSheetIndex(0)
                      ->setCellValue('A'.$row, $no)
                      ->setCellValue('B'.$row, $nm)           
                      ->setCellValue('C'.$row, round($nom))           
                      ;
        
        $row++;
        $no++;
        $sum += $nom;
      }
      $row_end = $row-1;
      $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A'.$row, '')
                    ->setCellValue('B'.$row, 'Total')           
                    ->setCellValue('C'.$row, '=sum(C'.$row_start.':C'.$row_end.')')           
                    ;      
      $objPHPExcel->getActiveSheet()->getStyle('A'.$row.':F'.$row)->getFont()->setBold(true);     


$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
          
$filename = "Category SO Summary Report ";
// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;
