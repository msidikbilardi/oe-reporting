<?php 
session_start();

//drop and create table
	date_default_timezone_set('Asia/Jakarta');
	$localhostdb = '192.168.1.70';
	$usernamedb  = 'root';
	$passworddb  = 'Qwert!123';
	$databasedb  = 'oereporting';
		
	$con = mysqli_connect($localhostdb,$usernamedb,$passworddb,$databasedb)or die("cannot connect"); 

	$query = "DROP TABLE IF EXISTS `so_sls".$_SERVER['REMOTE_ADDR']."`";
	$submit = mysqli_query($con,$query);

	$query = "CREATE TABLE `so_sls".$_SERVER['REMOTE_ADDR']."` (`userid` VARCHAR(50) NOT NULL,`category` VARCHAR(50) NOT NULL,`nominal` DOUBLE NOT NULL,`db` VARCHAR(50) NOT NULL) ENGINE=InnoDB";

	$submit = mysqli_query($con,$query);
//end drop and create table


//set filter
	if ($_POST['periode'] == 'Q1'){
		$start = $_POST['tahun'] . "-01-01";
		$end = $_POST['tahun'] . "-03-31";
	}else if ($_POST['periode'] == 'Q2'){
		$start = $_POST['tahun'] . "-04-01";
		$end = $_POST['tahun'] . "-06-30";
	}else if ($_POST['periode'] == 'Q3'){
		$start = $_POST['tahun'] . "-07-01";
		$end = $_POST['tahun'] . "-09-30";
	}else if ($_POST['periode'] == 'Q4'){
		$start = $_POST['tahun'] . "-10-01";
		$end = $_POST['tahun'] . "-12-31";
	}

	$so = "yes";// $_GET['so'];
	$db = "TOTALINPRO_2016"; //$_GET['db'];
	$type =  "out_invoice"; //$_GET['type'];
	$state =  "all"; //$_GET['state'];
	$state_so =  "all"; //$_GET['state'];
	$type_report=  "so"; //$_GET['type_report'];
	$values = '';
	$string_state = 'ALL SALES ORDER';
	$type_string = "Document : SO Category Consolidate Sales Report " . $_POST['periode'] . " - " . $_POST['tahun'];
    if($type_report == 'so'){
      $filter = "AND B.state != 'cancel' AND B.state != 'draft' AND B.state != 'sent'";
    }else{
      $filter = "AND B.state != 'cancel' AND (B.state = 'draft' OR B.state = 'sent' ) ";
    }	

//end set filter
	if ($_POST['idusr'] == ''){		
		//$val = '12312';


		$q_main = mysqli_query($con,"SELECT * FROM tblmasterdatabase where status = 1");
		while($main = mysqli_fetch_assoc($q_main)){
			if($main['nama'] == 'GBU'){
				$ip = $_SESSION['ip_vps'];
			}else{
				$ip = $_SESSION['ip_local'];
			}
			$conn_string = "host=".$ip." dbname=".$main['nama']." user=".$_SESSION['user_pg']." password=";
			$dbconn = pg_connect($conn_string);	
			$query = pg_query($dbconn,"SELECT  Z.user_id,W.id,W.name,sum(Z.sm_price_unit_after_disc3 * Z.product_uom_qty) as after3,sum(Z.sm_price_unit_before_disc * Z.product_uom_qty) as no_categ FROM(
	            SELECT A.*,B.name,B.date_order,B.user_id FROM sale_order_line A,sale_order B where A.order_id = B.id $filter AND B.date_order BETWEEN '$start 00:00:00' AND '$end 23:59:59' 
	            ) Z 
	            LEFT JOIN (select id,default_code,product_tmpl_id from product_product) Y ON Z.product_id = Y.id
	            LEFT JOIN (SELECT id,categ_id FROM product_template) X ON Y.product_tmpl_id = X.id
	            LEFT JOIN product_category W ON W.id = X.categ_id
	            GROUP BY W.id, Z.user_id ORDER BY W.name");
	        while($data = pg_fetch_assoc($query)){
	        	$u = mysqli_query($con,"SELECT * FROM tblmasteruser_detail A INNER JOIN tblmasterdatabase B ON A.dbid = B.id WHERE B.nama = '".$main['nama']."' AND A.idoe = '".$data['user_id']."'");
	        	
	        	$user = mysqli_fetch_array($u);
		        if($data['id']){
		          $nm = $data['name'];
		          $nom = $data['after3'];
		        }else{
		          $nm = 'No Category';
		          $nom = $data['no_categ'];
		        }    
		        $categid = $data['id'];
		        $values = $values . "('".$user['iduser']."', '$nm', $nom, '".$main['second_name']."'),";    	
	        	//echo $data['name'];
	        }

		}


	}else{
		//$val = 'asdasd';


		$q_main = mysqli_query($con,"SELECT * FROM( SELECT * FROM `tblmasteruser_detail` where iduser = '".$_GET['usr']."' ) A LEFT JOIN tblmasterdatabase B ON A.dbid = B.id");
		while($main = mysqli_fetch_assoc($q_main)){
			if($main['nama'] == 'GBU'){
				$ip = $_SESSION['ip_vps'];
			}else{
				$ip = $_SESSION['ip_local'];
			}
			$conn_string = "host=".$ip." dbname=".$main['nama']." user=".$_SESSION['user_pg']." password=";
			$dbconn = pg_connect($conn_string);	
			$query = pg_query($dbconn,"SELECT W.id,W.name,sum(Z.sm_price_unit_after_disc3 * Z.product_uom_qty) as after3,sum(Z.sm_price_unit_before_disc * Z.product_uom_qty) as no_categ FROM(
	            SELECT A.*,B.name,B.date_order FROM sale_order_line A,sale_order B where A.order_id = B.id $filter AND B.date_order BETWEEN '$start 00:00:00' AND '$end 23:59:59' AND B.user_id = '".$main['idoe']."'
	            ) Z 
	            LEFT JOIN (select id,default_code,product_tmpl_id from product_product) Y ON Z.product_id = Y.id
	            LEFT JOIN (SELECT id,categ_id FROM product_template) X ON Y.product_tmpl_id = X.id
	            LEFT JOIN product_category W ON W.id = X.categ_id
	            GROUP BY W.id ORDER BY W.name");
	        while($data = pg_fetch_assoc($query)){
		        if($data['id']){
		          $nm = $data['name'];
		          $nom = $data['after3'];
		        }else{
		          $nm = 'No Category';
		          $nom = $data['no_categ'];
		        }    
		        $categid = $data['id'];
		        $values = $values . "('".$_GET['usr']."', '$nm', $nom, '".$main['second_name']."'),";    	
	        	//echo $data['name'];
	        }
			
			//echo $main['nama'].' - '.$values;
		}

	}

//start variable
	$sum = 0;
	$sumall = 0;
	$filter = '';
	$filter_prn = '';
	$info = 'No Company Category';
	$no = 1;
	$cat = "";
	

		$values = substr($values,0,-1) . ";";
		$query = "INSERT INTO `so_sls".$_SERVER['REMOTE_ADDR']."`(`userid`,`category`, `nominal`, `db`) VALUES " . $values;
		$submit = mysqli_query($con,$query);
//end loop query


//start display

  define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
    
    require_once '../assets/Classes/PHPExcel.php';
    /******************END DATE ADDITIONAL FILTER*************************/
    
    $objPHPExcel = new PHPExcel();

    $objPHPExcel->getProperties()->setCreator("OE Reporting")
                   ->setLastModifiedBy("OE Reporting")
                   ->setTitle("Laporan Penjualan")
                   ->setSubject("Laporan Penjualan")
                   ->setDescription("OE Reporting")
                   ->setKeywords("office PHPExcel php")
                   ->setCategory("Laporan Penjualan Seluruh Area");
                   
    $objPHPExcel->getDefaultStyle()
          ->getAlignment()
          ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT); 
    $objPHPExcel->setActiveSheetIndex(0)
              ->setCellValue('A1', $type_string)
              ->setCellValue('E1', $info)
              ->setCellValue('A2', $string_state)
              ->setCellValue('E2', 'SO Date : '.$start.' to '.$end);

    $row = 4;
    $sum = 0;


    if ($_POST['idusr'] == ''){	
		$u = mysqli_query($con,"SELECT A.userid , B.*, C.*,". $_POST['periode'] . " target FROM(
							    SELECT * FROM `so_sls".$_SERVER['REMOTE_ADDR']."` WHERE userid != '' GROUP BY userid
							    ) A LEFT JOIN tblmastertarget B ON A.userid = B.iduser
							    LEFT JOIN tblmasteruser C ON C.iduser = A.userid
							     WHERE tahun = '".$_POST['tahun']."' AND B.idcat = ''");	
		while($usr = mysqli_fetch_assoc($u)){
			$sum = 0;
			$no = 1;
		    $objPHPExcel->setActiveSheetIndex(0)
		              ->setCellValue('A'.$row, strtoupper($usr['nama']));
          	$row++;
			$sq = mysqli_query($con,"SELECT * FROM `so_sls".$_SERVER['REMOTE_ADDR']."` WHERE userid = '".$usr['userid']."' ");
			while($res = mysqli_fetch_assoc($sq)){
				$objPHPExcel->setActiveSheetIndex(0)
						  ->setCellValue('A'.$row, $no)
						  ->setCellValue('B'.$row, $res['category'] . " - " . $res['db'])
						  ->setCellValue('D'.$row, round($res['nominal']));
				$row++;
				$no++;
				$sum = $sum + $res['nominal'];
			}
			$target = $usr['target'];
			if ($target-$sum < 0){
				$selisih = $sum-$target;
			}else{
				$selisih = $sum-$target; 
			}
			$pencapaian = $target==0?0:($sum / $target * 100);
			$kontribusi = $sumall==0?0:($sum / $sumall * 100);

			//$row++;	
			$objPHPExcel->setActiveSheetIndex(0)
				  ->setCellValue('A'.$row, "GRAND TOTAL")
				  ->setCellValue('B'.$row, round($sum))
				  ->setCellValue('C'.$row, "TARGET")
				  ->setCellValue('D'.$row, round($target))
				  ->setCellValue('E'.$row, "SELISIH")
				  ->setCellValue('F'.$row, round($selisih))
				  ->setCellValue('G'.$row, "PENCAPAIAN")
				  ->setCellValue('H'.$row, round($sum / $target * 100));

			$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':I'.$row)->getFont()->setBold(true);  					
			$row++;
			$row++;
		}							                   
    }else{
	    $objPHPExcel->setActiveSheetIndex(0)
	              ->setCellValue('A'.$row, 'engga');
    }
 //    $objPHPExcel->getActiveSheet()->getStyle('A'.$row.':F'.$row)->getFont()->setBold(true);     
 //    $row++;
	// $sq = "SELECT nama usr, tahun, ". $_POST['periode'] . " target, category, nominal, db,
	// 	(SELECT SUM(nominal) nominal FROM `so_sls".$_SERVER['REMOTE_ADDR']."` A CROSS JOIN 
	// 	tblmasteruser B INNER JOIN tblmastertarget C ON B.iduser = C.iduser
	// 	WHERE B.iduser = '".$_POST['idusr']."' AND C.tahun = '".$_POST['tahun']."' AND C.idcat = '') sumall
	// 	FROM `so_sls".$_SERVER['REMOTE_ADDR']."` A CROSS JOIN 
	// 	tblmasteruser B INNER JOIN tblmastertarget C ON B.iduser = C.iduser
	// 	WHERE B.iduser = '".$_POST['idusr']."' AND C.tahun = '".$_POST['tahun']."' AND C.idcat = ''";

 
	// $rs = mysqli_query($con,$sq);
	// 	while($res = mysqli_fetch_assoc($rs)){			
	// 		if ($no == 1){
	// 			$cat = $res['nama'];
	// 			$sum = 0;
	// 			$objPHPExcel->setActiveSheetIndex(0)
	// 				->setCellValue('A4', $res['usr']);
	// 		}

	// 		$objPHPExcel->setActiveSheetIndex(0)
	// 				  ->setCellValue('A'.$row, $no)
	// 				  ->setCellValue('B'.$row, $res['category'] . " - " . $res['db'])
	// 				  ->setCellValue('D'.$row, round($res['nominal']));

	// 		$sum = $sum + $res['nominal'];
	// 		$target = $res['target'];
	// 		$no++;
	// 		$row = $row + 1;
	// 	}
		
	// 	if ($target-$sum < 0){
	// 		$selisih = $sum-$target;
	// 	}else{
	// 		$selisih = $sum-$target; 
	// 	}
	// 	$pencapaian = $target==0?0:($sum / $target * 100);
	// 	$kontribusi = $sumall==0?0:($sum / $sumall * 100);

	// 	$row = $row + 2;
	// 	$objPHPExcel->setActiveSheetIndex(0)
	// 		  ->setCellValue('B'.$row, "GRAND TOTAL")
	// 		  ->setCellValue('C'.$row, round($sum))
	// 		  ->setCellValue('D'.$row, "TARGET")
	// 		  ->setCellValue('E'.$row, round($target))
	// 		  ->setCellValue('F'.$row, "SELISIH")
	// 		  ->setCellValue('G'.$row, round($selisih))
	// 		  ->setCellValue('H'.$row, "PENCAPAIAN")
	// 		  ->setCellValue('I'.$row, round($sum / $target * 100));

	// 	$objPHPExcel->getActiveSheet()->getStyle('B'.$row.':I'.$row)->getFont()->setBold(true);     


// foreach(range('B','K') as $columnID) {
//     $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
//         ->setAutoSize(true);
// }
          
$filename = "Category SO Consolidate Sales Report ";
// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;
