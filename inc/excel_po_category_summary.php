<?php
session_start();
$conn_string = "host=".$_SESSION['host']." dbname=".$_POST['db']." user=".$_SESSION['user_db']." password=".$_SESSION['pass_db']."";
$dbconn = pg_connect($conn_string);
$c = pg_query($dbconn,"SELECT * FROM res_company");
$comp = pg_fetch_array($c);
$db = $_POST['db'];
$start = $_POST['date_start'];
$end =  $_POST['date_end'];
//$type =  $_POST['type'];
//$state =  $_POST['state'];

	$filter = '';
	$filter_prn = '';
	$info = '';

/*if($state == 'all'){
	$state_list = "b.state != 'cancel'";
	$state_dt = "AND B.state != 'cancel' AND B.state != 'draft' AND B.state != 'sent'";
	$string_state = 'ALL SALES ORDER';
}else{
	$state_list = "b.state = 'paid'";
	$state_dt = "E.state = 'paid'";
	$string_state = 'ONLY PAID INVOICES';
} */
	$string_state = 'ALL PURCHASE ORDER';
	$type_string = "Document : Purchase Order Category Summary Report";
	$state_dt = "AND B.state != 'cancel' AND B.state != 'draft' AND B.state != 'sent'";
	
$no = 1;

	define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
		
		require_once '../assets/Classes/PHPExcel.php';
		/******************END DATE ADDITIONAL FILTER*************************/
		
		$objPHPExcel = new PHPExcel();

		$objPHPExcel->getProperties()->setCreator("Sidik Bilardi")
									 ->setLastModifiedBy("Sidik Bilardi")
									 ->setTitle("Laporan Penjualan")
									 ->setSubject("Laporan Penjualan")
									 ->setDescription("Delta Report")
									 ->setKeywords("office PHPExcel php")
									 ->setCategory("Laporan Penjualan Seluruh Area");
									 
		$objPHPExcel->getDefaultStyle()
					->getAlignment()
					->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);	
		$objPHPExcel->setActiveSheetIndex(0)
							->setCellValue('A1', $comp['name'])
							->setCellValue('A2', $comp['msb_po_address_1'])
							->setCellValue('A3', $comp['msb_po_address_2'])
							->setCellValue('A4', $comp['msb_po_phone'])
							->setCellValue('A5', $comp['msb_po_city'])
							->setCellValue('A6', $comp['msb_po_email'])
							->setCellValue('A8', $type_string)
							->setCellValue('E8', $info)
							->setCellValue('A9', $string_state)
							->setCellValue('E9', 'PO Date : '.$start.' to '.$end)							
							;

		$row = 10;
		$sum = 0;

			$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':F'.$row)->getFont()->setBold(true);			
			$row++;
/*			$q = "
                select COALESCE(sum(A.sm_price_unit_after_disc3 * product_uom_qty),'0') as final,D.name 
                FROM res_partner F,sale_order E,sale_order_line A, product_product B,product_template C,product_category D
	        	WHERE E.partner_id = F.id $state_dt AND C.categ_id = D.id AND A.product_id = B.id AND B.product_tmpl_id = C.id  AND A.order_id = E.id AND E.currency_id = '13' AND E.date_order >= '$start' AND E.date_order <= '$end' GROUP BY D.name ORDER BY D.name";
*/
			$q ="SELECT name,SUM(total) as final,SUM(not_diskon) as not_diskon FROM (SELECT A.product_id ,sum(A.product_qty*A.sm_price_unit_after_disc3) as total,SUM(A.product_qty*A.price_unit) as not_diskon FROM purchase_order_line A,purchase_order B where B.date_order >= '$start' AND B.date_order <= '$end' AND B.date_order IS NOT NULL AND B.currency_id = '13' $state_dt AND A.order_id = B.id group by A.product_id) Z LEFT JOIN (SELECT A.id,C.name,A.default_code,A.name_template,A.product_tmpl_id FROM product_product A,product_template B,product_category C WHERE B.categ_id = C.id AND A.product_tmpl_id = B.id) Y ON Z.product_id = Y.id GROUP BY name";


			 $d = pg_query($dbconn,$q);
			
            while($dt = pg_fetch_assoc($d)){ 		
            	if($dt['name'] == ''){
            		$nom = $dt['not_diskon'];
            		 $sum += $dt['not_diskon'];
            		 $nm = 'No Product ID';
            	}else{
            		$nom = $dt['final'];
            		 $sum += $dt['final'];
            		 $nm = $dt['name'];
            	}            	

				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('B'.$row, $no)
					->setCellValue('C'.$row, $nm)
					->setCellValue('E'.$row, round($nom))
					;
            
            $row++;
           // $sum += round($dt['final']);
            $no++;
            }	

		$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A'.$row, 'SUMMARY')
			->setCellValue('F'.$row, round($sum))
			;
			$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':F'.$row)->getFont()->setBold(true);			


$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(5);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
					
$filename = "Category PO Summary Report ";
// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;
