<?php

include"inc/class.func.php";
    $_SESSION['logged'] = 'true';
    $iduser = $_SESSION['iduser'] = '1';
    $name = $_SESSION['name'] = 'Guest';
$_SESSION['host'] = '192.168.1.111';

$_GET['db'] = 'TOTALINPRO_2016';
$_SESSION['user_db'] = 'ext';
$_SESSION['pass_db'] = 'ext';

$_SESSION['new_host'] = '101.50.1.185';
$_SESSION['new_user'] = 'openerp';

if(isset($_SESSION['iduser'])){
    $iduser = $_SESSION['iduser'];
    $name = $_SESSION['name'];

}


?>

<!DOCTYPE html>
<html>
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Willert | OE Reporting</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/ionicons.min.css">
    <link rel="stylesheet" href="assets/css/AdminLTE.css">
    <link rel="stylesheet" href="assets/css/vinzlee.css">
    <link rel="stylesheet" href="assets/css/skins/skin-green.min.css">
    <link href="assets/img/willert/favicon.png" rel="shortcut icon">
    <link rel="stylesheet" type="text/css" href="assets/datatables.css">
    <link rel="stylesheet" type="text/css" href="assets/datatables.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/jquery-multiselect.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    </head>
    <!--
    BODY TAG OPTIONS:
    =================
    Apply one or more of the following classes to get the
    desired effect
    |---------------------------------------------------------|
    | SKINS         | skin-blue                               |
    |               | skin-black                              |
    |               | skin-purple                             |
    |               | skin-yellow                             |
    |               | skin-red                                |
    |               | skin-green                              |
    |---------------------------------------------------------|
    |LAYOUT OPTIONS | fixed                                   |
    |               | layout-boxed                            |
    |               | layout-top-nav                          |
    |               | sidebar-collapse                        |
    |               | sidebar-mini                            |
    |---------------------------------------------------------|
    -->

 <?php
  if(isset($_SESSION['logged']) == ''){
    include "page/login.php";

  }else{
  ?>
    <body class="hold-transition skin-green sidebar-mini">
        <div class="wrapper">
            <!-- Main Header -->
            <header class="main-header">
                <!-- Logo -->
                <a href="index.php" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini">
                        <img class="logonavmini" src="assets/img/willert/favicon.png">
                    </span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b>OE</b> Reporting</span>
                </a>

                <!-- Header Navbar -->
                <nav class="navbar navbar-static-top" role="navigation">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>
                    <!-- Navbar Right Menu -->
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <!-- Messages: style can be found in dropdown.less-->
                            <!--li class="dropdown messages-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-envelope-o"></i>
                                    <span class="label label-success">4</span>
                                                        Design some buttons
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="header">You have 4 messages</li>
                                    <li>

                                        <ul class="menu">
                                            <li>
                                                <a href="#">
                                                    <div class="pull-left">
                                                        <img src="assets/img/profile/user.jpg" class="img-circle" alt="User Image">
                                                    </div>

                                                    <h4>
                                                        Support Team
                                                        <small><i class="fa fa-clock-o"></i> 5 mins</small>
                                                    </h4>

                                                    <p>Why not buy a new awesome theme?</p>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="footer"><a href="#">See All Messages</a></li>
                                </ul>
                            </li--><!-- /.messages-menu -->
                            <!-- Notifications Menu -->
                            <li class="dropdown notifications-menu">
                                <!-- Menu toggle button -->
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                  <i class="fa fa-bell-o"></i>
                                  <span class="label label-warning">10</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="header">You have 10 notifications</li>
                                    <li>
                                        <!-- Inner Menu: contains the notifications -->
                                        <ul class="menu">
                                            <li><!-- start notification -->
                                                <a href="#">
                                                    <i class="fa fa-users text-aqua"></i> 5 new members joined today
                                                </a>
                                            </li><!-- end notification -->
                                        </ul>
                                    </li>
                                    <li class="footer"><a href="#">View all</a></li>
                                </ul>
                            </li>
                            <!-- Tasks Menu -->
                            <li class="dropdown tasks-menu">
                                <!-- Menu Toggle Button -->
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-flag-o"></i>
                                    <span class="label label-danger">9</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="header">You have 9 tasks</li>
                                    <li>
                                        <!-- Inner menu: contains the tasks -->
                                        <ul class="menu">
                                            <li><!-- Task item -->
                                                <a href="#">
                                                    <!-- Task title and progress text -->
                                                    <h3>
                                                        <small class="pull-right">20%</small>
                                                    </h3>
                                                    <!-- The progress bar -->
                                                    <div class="progress xs">
                                                        <!-- Change the css width attribute to simulate progress -->
                                                        <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                            <span class="sr-only">20% Complete</span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li><!-- end task item -->
                                        </ul>
                                    </li>
                                    <li class="footer">
                                        <a href="#">View all tasks</a>
                                    </li>
                                </ul>
                            </li>
                        <!-- User Account Menu -->
                        <li class="dropdown user user-menu">
                            <!-- Menu Toggle Button -->
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <!-- The user image in the navbar--><div width="160px" height="160px">
                                <img src="assets/img/profile/user.jpg" class="user-image" alt="User Image">
                                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                                <span class="hidden-xs"><?php echo $name;?></span></div>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- The user image in the menu -->
                                <li class="user-header">
                                    <img src="assets/img/profile/user.jpg" class="img-circle" alt="User Image">
                                    <p>
                                        <?php echo $name;?> - Web Developer
                                        <small>Last Login </small>
                                    </p>
                                </li>
                                <!-- Menu Body -->
                                <li class="user-body">
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Followers</a>
                                    </div>
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Sales</a>
                                    </div>
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Friends</a>
                                    </div>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="?page=profile" class="btn btn-default btn-flat">Profile</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="?action=doLogout" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                            </li>
                                <!-- Control Sidebar Toggle Button -->
                            <li>
                                <a href="#" id="btnconf" data-toggle="control-sidebar"><i class="fa fa-cog"></i></a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel (optional) -->
                    <div class="user-panel">
                        <div class="pull-left image" width="160px" height="160px">
                            <img src="assets/img/profile/user.jpg" class="img-circle" alt="User Image">
                        </div>
                        <div class="pull-left info">
                            <p><?php echo $name;?></p>
                            <!-- Status -->
                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>

                    <!-- search form (Optional) -->
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                    <!-- /.search form -->

                    <!-- Sidebar Menu -->
                    <ul class="sidebar-menu">
                        <li class="header">MAIN NAVIGATION</li>
                        <!-- Optionally, you can add icons to the links -->

                        <!--<li class="active"><a href="?page=inv"><i class="glyphicon glyphicon-shopping-cart"></i> <span>Inventory</span></a></li>-->
						<!--li class="active"><a href="?page=import"><i class="glyphicon glyphicon-comment"></i> <span>Import Data</span></a></li-->

						<li class="treeview">
                            <a href="#"><i class="fa fa-link"></i> <span>Inventory</span> <i class="fa fa-angle-left pull-right"></i></a>
                            <ul class="treeview-menu">
                                <li class="active"><a href="?page=inv"><i class="glyphicon glyphicon-shopping-cart"></i> <span>Inventory</span></a></li>
                                <li class="active"><a href="?page=inv_cons"><i class="glyphicon glyphicon-shopping-cart"></i> <span>Inventory Cons</span></a></li>
                            </ul>
                        </li>
                       <li class="treeview">
                            <a href="#"><i class="fa fa-link"></i> <span>Category Invoice Report</span> <i class="fa fa-angle-left pull-right"></i></a>
                            <ul class="treeview-menu">
                                <li class="active"><a href="?page=catsummary&v=category_summary_filter&s=Category Summary Report&so=no"><i class="glyphicon glyphicon-list-alt"></i> <span>Category Summary Report</span></a></li>
                                <li class="active"><a href="?page=catsalesperson&v=category_salesperson_filter&s=Category Sales Person Report&so=no"><i class="glyphicon glyphicon-user"></i> <span>Category Salesperson Report</span></a></li>

                                <li class="active"><a href="?page=catdetail"><i class="glyphicon glyphicon-th-list"></i> <span>Category Detail Report</span></a></li>
                            </ul>
                        </li>
                       <li class="treeview">
                            <a href="#"><i class="fa fa-link"></i> <span>Category SO Report</span> <i class="fa fa-angle-left pull-right"></i></a>
                            <ul class="treeview-menu">
                                <li class="active"><a href="?page=cat_so_summary&v=category_summary_filter&s=Category SO Summary Report&so=yes"><i class="glyphicon glyphicon-list-alt"></i> <span>SO Summary Report</span></a></li>

                                <li class="active"><a href="?page=catsosalesperson&v=category_salesperson_filter&s=Category SO Salesperson Report&so=yes"><i class="glyphicon glyphicon-user"></i> <span>SO Salesperson Report</span></a></li>
                                <li class="active"><a href=" ?page=catsodetail&v=category_so_detail_filter&s=Category SO Detail Report&so=yes"><i class="glyphicon glyphicon-th-list"></i> <span>SO Detail Report</span></a></li>
								<li class="active"><a href="?page=category_cons&v=category_cons_summary_filter&s=Category SO Consolidate Summary Report"><i class="glyphicon glyphicon-list-alt"></i> <span>SO Cons Product Report</span></a></li>
                                <li class="active"><a href="?page=category_cons&v=category_cons_salesperson_filter&s=Category SO Consolidate Salesperson Report"><i class="glyphicon glyphicon-user"></i> <span>SO Cons Sales Summary Report</span></a></li>
				<?php
				//if ($_SERVER['REMOTE_ADDR'] == '192.168.1.160'){
				?>

								<li class="active"><a href="?page=category_cons_det&v=category_cons_detail_filter&s=Category SO Consolidate Detail Report"><i class="glyphicon glyphicon-th-list"></i> <span>SO Cons Detail Report</span></a></li>
				<?php
				//}
				?>
                           </ul>
                        </li>
                       <li class="treeview">
                            <a href="#"><i class="fa fa-link"></i> <span>Category PO Report</span> <i class="fa fa-angle-left pull-right"></i></a>
                            <ul class="treeview-menu">
                                <li class="active"><a href="?page=cat_po_summary&v=category_po_summary_filter&s=Category PO Summary Report"><i class="glyphicon glyphicon-list-alt"></i> <span>Cat PO Summary Report</span></a></li>
                                <li class="active"><a href="?page=catposalesperson&v=category_po_salesperson_filter&s=Category PO Salesperson Report"><i class="glyphicon glyphicon-user"></i> <span>Cat PO Salesperson Report</span></a></li>
                                <li class="active"><a href="?page=catpodetail&v=category_po_detail_filter&s=Category PO Detail Report"><i class="glyphicon glyphicon-th-list"></i> <span>Cat PO Detail Report</span></a></li>
                           </ul>
                        </li>
			<?php //if($_GET['role'] == 'master'){ ?>
                       <li class="treeview">
                            <a href="#"><i class="fa fa-link"></i> <span>Master</span> <i class="fa fa-angle-left pull-right"></i></a>
                            <ul class="treeview-menu">
                                <li class="active"><a href="?page=master_user_list"><i class="glyphicon glyphicon-list-alt"></i> <span>Master User</span></a></li>
                                <li class="active"><a href="?page=master_category_list&v=master_category&s=Master Category"><i class="glyphicon glyphicon-list-alt"></i> <span>Master Category</span></a></li>
                                <li class="active"><a href="?page=master_target_list"><i class="glyphicon glyphicon-list-alt"></i> <span>Master Target</span></a></li>
                           </ul>
                        </li>
			<?php //} ?>
                    </ul><!-- /.sidebar-menu -->
                </section>
            <!-- /.sidebar -->
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Page Header
                        <small>Optional description</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
                        <li class="active">Here</li>
                    </ol>
                </section>
                <!-- Main content -->
                <section class="content">
                    <!-- Your Page Content Here -->
                  <?php
                  if(isset($_GET['page'])){
                      if($_GET['page'] == 'import'){
                        //include "page/import_data.php";
                      }elseif($_GET['page'] == 'inv'){
                        include "page/inventory.php";
					  }elseif($_GET['page'] == 'inv_cons'){
                        include "page/inventory_cons.php";
                      }elseif($_GET['page'] == 'catdetail'){
                        include "page/category_detail.php";
                      }elseif($_GET['page'] == 'catsummary' || $_GET['page'] == 'catsalesperson'  || $_GET['page'] == 'catsosalesperson' || $_GET['page'] == 'cat_so_summary' || $_GET['page'] == 'catsodetail'){
                        include "page/choose_db.php";
                      }elseif($_GET['page'] == 'cat_po_summary' || $_GET['page'] == 'catposalesperson' || $_GET['page'] == 'catpodetail'  ){
                        include "page/choose_po_db.php";
                      }elseif($_GET['page'] == 'category_cons'){
                        include "page/consolidate.php";
					  }elseif($_GET['page'] == 'category_cons_dev'){
						include "page/consolidate_dev.php";
					  }elseif($_GET['page'] == 'category_cons_det'){
						include "page/consolidate_det.php";
                      }elseif($_GET['page'] == 'master_user'){
                        include "page/master_user.php";
                      }elseif($_GET['page'] == 'master_user_list'){
                        include "page/master_user_list.php";
                      }elseif($_GET['page'] == 'master_user_view'){
                        include "page/master_user_view.php";
                      }elseif($_GET['page'] == 'master_user_edit'){
                        include "page/master_user_edit.php";
                      }elseif($_GET['page'] == 'master_category'){
                        include "page/master_category.php";
                      }elseif($_GET['page'] == 'master_category_edit'){
                        include "page/master_category_edit.php";
                      }elseif($_GET['page'] == 'master_category_view'){
                        include "page/master_category_view.php";
                      }elseif($_GET['page'] == 'master_category_list'){
                        include "page/master_category_list.php";
                      }elseif($_GET['page'] == 'master_target'){
                        include "page/master_target.php";
                      }elseif($_GET['page'] == 'master_target_view'){
                        include "page/master_target_view.php";
                      }elseif($_GET['page'] == 'master_target_edit'){
                        include "page/master_target_edit.php";
                      }elseif($_GET['page'] == 'master_target_list'){
                        include "page/master_target_list.php";
                      }else{
                        include "page/dashboard.php";
                      }
                }
                  ?>
                </section><!-- /.content -->
            </div><!-- /.content-wrapper -->

            <!-- Main Footer -->
            <footer class="main-footer">
                <div class="alignright">
                    <strong>&copy; <?php echo date('Y');?> <a href="#">Willertindo Innovation Solution</a>.</strong> All rights reserved.
                </div>
            </footer>

            <!-- Control Sidebar -->
            <aside class="control-sidebar control-sidebar-dark" id="control-sidebar">
                <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
                    <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
                    <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="control-sidebar-home-tab">
                        <h3 class="control-sidebar-heading">On develope feature!</h3>
                    </div>
                    <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
                    <div class="tab-pane" id="control-sidebar-settings-tab">
                        <label>Change password</label>
                        <div class="input-group">
                            <input type="password" class="form-control" placeholder="Old password" id="passlama" aria-describedby="basic-addon1">
                            <input type="password" class="form-control" placeholder="New password" id="passbaru" aria-describedby="basic-addon1">
                            <input type="password" class="form-control" placeholder="Password confirmation" id="passbaru2" aria-describedby="basic-addon1">
                        </div>
                        <p class="alertcpass"></p>
                        <button class="btn btn-success" type="submit" onclick="changepassword()">
                            <span class="fa fa-edit"></span> Change password
                        </button>
                    </div>
                </div>
            </aside>
        </div>
        <!-- REQUIRED JS SCRIPTS -->
        <script src="assets/js/jquery.js"></script>
        <script src="assets/bootstrap.min.js"></script>
        <script src="assets/js/app.min.js"></script>
        <script type="text/javascript" src="assets/datatables/js/jquery.dataTables.js"></script>
        <script type="text/javascript" src="assets/datatables/js/dataTables.bootstrap.min.js"></script>
    	<!-- jQuery autocomplete -->
	      <script src="assets/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
        <script src="assets/script.js"></script>


        <script type="text/javascript">
 /* $(function(){
    jQuery(".fancybox").fancybox();
  });
*/

function isNumberKey(evt,val)
{
    var charCode = (evt.which) ? evt.which : event.keyCode
    //counts(val);
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
        return true;
}
$("#XForm").submit(function(event) {
event.preventDefault();
  var formData = new FormData(this);

      $.ajax({
        type:'POST',
        url: $(this).attr('action'),
        data:formData,
        cache:false,
        contentType: false,
        processData: false,
          success:function(data){
            //console.log("success");
           // console.log(data);
            alert("Simpan Data telah berhasil");
            //$("#isi").load("themes/master_product.php");
             // $("#isi").css({'display':''});
            $('.form-control').val('');
            $('.listcheck').attr('checked', false);
           // location.reload();
          },
          error: function(data){
            //console.log("error");
            //console.log(data);
          }
          });

});

$("#EditForm").submit(function(event) {
event.preventDefault();
  var formData = new FormData(this);

      $.ajax({
        type:'POST',
        url: $(this).attr('action'),
        data:formData,
        cache:false,
        contentType: false,
        processData: false,
          success:function(data){
            //console.log("success");
           // console.log(data);
            alert("Edit Data telah berhasil");
            window.history.back();
            //$("#isi").load("themes/master_product.php");
             // $("#isi").css({'display':''});
            //$('.form-control').val('');

          },
          error: function(data){
            //console.log("error");
            //console.log(data);
          }
          });

});

/*$(document).ready( function ()
{
    var date = new Date();
    var d = date.getDate(),
    m = date.getMonth(),
    y = date.getFullYear();
    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        editable: false,
        droppable: false,
        selectable: true,
        selectHelper: true,
        buttonText: {
            today: 'today',
            month: 'month',
            week: 'week',
            day: 'day'
        },
         //events: 'getEvents.php',
        events: "inc/getEvents.php?y="+y+"&m="+m+"&d="+d,

        eventLimit: true,
        viewRender: function (view, element) {
 //           var b = $('#calendar').fullCalendar('getDate');
  //          alert(b.format('L'));

          },
        eventClick:  function(event, jsEvent, view) {
            $('#modalTitle').html(event.title);
            $('#modalBody').html(event.description);
            $('#eventUrl').attr('href',event.url);
            $('#fullCalModal').modal();
            return false;
        },
        dayClick: function(date, event, jsEvent, view) {
            $('#fullCalModalAdd').modal();
            },
        });
                $('.table_id').DataTable
                ({
                    responsive: true
                });
                $('#control-sidebar').css('display', 'none');
            });
            $('#btnconf').click(function()
            {
                var element = document.getElementById('control-sidebar'),
                style = window.getComputedStyle(element),
                display = style.getPropertyValue('display');
                if (display=="none")
                {
                    $('#control-sidebar').css('display', 'inline');
                }
                else
                {
                    $('#control-sidebar').css('display', 'none');
                }
            });
*/




        </script>
    </body>
<?php } ?>
</html>
