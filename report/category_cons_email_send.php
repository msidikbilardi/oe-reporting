<?php
  require '/var/www/html/oe_reporting/assets/mailgun-php-master/vendor/autoload.php';
  use Mailgun\Mailgun;
  
  $mgClient = new Mailgun('key-1fc017e6b69f6b4a177948a2fa1cb8a7');
  $domain = "tasksupport.willert.co.id";

  $to      = $email;
  $subject = 'Daily Achievement '. strtoupper($usrnm) .' - '. date('Ymd');
  $message = $msg;

  $result = $mgClient->sendMessage($domain, array
  (
  	'from'    => 'Auto Report <auto.report@willertindo.com>',
        'to'      => $to,
        'subject' => $subject,
        'html'    => $message
  ));

?>
