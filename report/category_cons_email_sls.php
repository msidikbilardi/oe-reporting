<?php 
//drop and create table
	date_default_timezone_set('Asia/Jakarta');
	$localhostdb = '192.168.1.70';
	$usernamedb  = 'root';
	$passworddb  = 'Qwert!123';
	$databasedb  = 'oereporting';
		
	$con = mysqli_connect($localhostdb,$usernamedb,$passworddb,$databasedb)or die("cannot connect"); 

	$query = "DROP TABLE IF EXISTS `so_sum" . $usr . "`";
	$submit = mysqli_query($con,$query);

	$query = "CREATE TABLE `so_sum" . $usr . "` (`category_id` VARCHAR(10) NOT NULL,`category` VARCHAR(50) NOT NULL,`nominal` DOUBLE NOT NULL,`db` VARCHAR(50) NOT NULL) ENGINE=InnoDB";

	$submit = mysqli_query($con,$query);
//end drop and create table

//set filter
	if ($per == 'Q1'){
		$start = $thn . "-01-01";
		$end = $thn . "-03-31";
	}else if ($per == 'Q2'){
		$start = $thn . "-04-01";
		$end = $thn . "-06-30";
	}else if ($per == 'Q3'){
		$start = $thn . "-07-01";
		$end = $thn . "-09-30";
	}else if ($per == 'Q4'){
		$start = $thn . "-10-01";
		$end = $thn . "-12-31";
	}

	$so = "yes";// $_GET['so'];
	$db = "TOTALINPRO_2016"; //$_GET['db'];
	$type =  "out_invoice"; //$_GET['type'];
	$state =  "all"; //$_GET['state'];
	$type_report=  "so"; //$_GET['type_report'];
//end set filter


//start variable
	$sum = 0;
	$sumnom = 0;
	$filter = '';
	$filter_prn = '';
	$info = 'No Company Category';
	$no = 1;
	$cat = "";
	if($type_report == 'so'){
		$state_dt = "AND B.state != 'cancel' AND B.state != 'draft' AND B.state != 'sent'";
		$state_dt2 = "AND state != 'cancel' AND state != 'draft' AND state != 'sent'";


		$string_state = 'ALL SALES ORDER';
		$type_string = "Document : SO Category Consolidate Report " . $per . " - " . $thn;
	}
//end variable


//start loop query
	//DB TIM
		$conn_string = "host=192.168.1.111 dbname=TOTALINPRO_2016 user=postgres password=postgres";
		$dbconn = pg_connect($conn_string);
		
		$state_list = "b.state != 'cancel'";
		$state_sales = "AND A.state != 'cancel' AND A.state != 'draft' AND A.state != 'sent'";
//		$q2 ="SELECT categ_id, name,SUM(total) as final,SUM(not_diskon) as not_diskon FROM (SELECT A.product_id ,sum(A.product_uom_qty*A.sm_price_unit_after_disc3) as total,SUM(A.product_uom_qty*A.price_unit) as not_diskon FROM sale_order_line A,sale_order B where B.date_order >= '$start' AND B.date_order <= '$end' AND B.date_order IS NOT NULL AND B.currency_id = '13' $state_dt AND A.order_id = B.id group by A.product_id) Z LEFT JOIN (SELECT B.categ_id, A.id,C.name,A.default_code,A.name_template,A.product_tmpl_id FROM product_product A,product_template B,product_category C WHERE B.categ_id = C.id AND A.product_tmpl_id = B.id) Y ON Z.product_id = Y.id GROUP BY categ_id, name";
$q2 = "
select C.id as categ_id,C.name,sum(product_uom_qty* sm_price_unit_after_disc3) as final from(
	select *,sum(product_uom_qty* sm_price_unit_after_disc3) as st from sale_order_line where date_order BETWEEN '$start'  AND '$end' $state_dt2
	GROUP BY id) A 
	LEFT JOIN product_category C ON C.id = A.categ_id
	GROUP BY C.id
       ORDER BY C.name

";
		$d = pg_query($dbconn,$q2);
		
		$values = "";
			
		while($dt = pg_fetch_assoc($d)){
			if($dt['name'] == ''){
				$nom = $dt['not_diskon'];
				$sum += $dt['not_diskon'];
				$nm = 'No Product ID';
				$categid = '0';
			}else{
				$nom = $dt['final'];
				$sum += $dt['final'];
				$nm = $dt['name'];
				$categid = $dt['categ_id'];
			}
			
			$values = $values . "('$categid', '$nm', $nom, 'TIM'),";
		}
		$values = substr($values,0,-1) . ";";
		$query = "INSERT INTO `so_sum" . $usr . "`(`category_id`, `category`, `nominal`, `db`) VALUES " . $values;
		$submit = mysqli_query($con,$query);
	//DB WIS
		$conn_string = "host=192.168.1.111 dbname=WILLERTINDO_2016 user=postgres password=postgres";
		$dbconn = pg_connect($conn_string);
		
		$state_list = "b.state != 'cancel'";
		$state_sales = "AND A.state != 'cancel' AND A.state != 'draft' AND A.state != 'sent'";
//		$q2 ="SELECT categ_id, name,SUM(total) as final,SUM(not_diskon) as not_diskon FROM (SELECT A.product_id ,sum(A.product_uom_qty*A.sm_price_unit_after_disc3) as total,SUM(A.product_uom_qty*A.price_unit) as not_diskon FROM sale_order_line A,sale_order B where B.date_order >= '$start' AND B.date_order <= '$end' AND B.date_order IS NOT NULL AND B.currency_id = '13' $state_dt AND A.order_id = B.id group by A.product_id) Z LEFT JOIN (SELECT B.categ_id, A.id,C.name,A.default_code,A.name_template,A.product_tmpl_id FROM product_product A,product_template B,product_category C WHERE B.categ_id = C.id AND A.product_tmpl_id = B.id) Y ON Z.product_id = Y.id GROUP BY categ_id, name";
$q2 = "
select C.id as categ_id,C.name,sum(product_uom_qty* sm_price_unit_after_disc3) as final from(
	select *,sum(product_uom_qty* sm_price_unit_after_disc3) as st from sale_order_line where date_order BETWEEN '$start'  AND '$end' $state_dt2
	GROUP BY id) A 
	LEFT JOIN product_category C ON C.id = A.categ_id
	GROUP BY C.id
       ORDER BY C.name

";
		$d = pg_query($dbconn,$q2);
		
		$values = "";
			
		while($dt = pg_fetch_assoc($d)){
			if($dt['name'] == ''){
				$nom = $dt['not_diskon'];
				$sum += $dt['not_diskon'];
				$nm = 'No Product ID';
				$categid = '0';
			}else{
				$nom = $dt['final'];
				$sum += $dt['final'];
				$nm = $dt['name'];
				$categid = $dt['categ_id'];
			}
			
			$values = $values . "('$categid', '$nm', $nom, 'WIS'),";
		}
		$values = substr($values,0,-1) . ";";
		$query = "INSERT INTO `so_sum" . $usr . "`(`category_id`, `category`, `nominal`, `db`) VALUES " . $values;
		$submit = mysqli_query($con,$query);
	//DB Ratek
		$conn_string = "host=192.168.1.111 dbname=RAJATEKNOLOGI_2016 user=postgres password=postgres";
		$dbconn = pg_connect($conn_string);
		
		$state_list = "b.state != 'cancel'";
		$state_sales = "AND A.state != 'cancel' AND A.state != 'draft' AND A.state != 'sent'";
//		$q2 ="SELECT categ_id, name,SUM(total) as final,SUM(not_diskon) as not_diskon FROM (SELECT A.product_id ,sum(A.product_uom_qty*A.sm_price_unit_after_disc3) as total,SUM(A.product_uom_qty*A.price_unit) as not_diskon FROM sale_order_line A,sale_order B where B.date_order >= '$start' AND B.date_order <= '$end' AND B.date_order IS NOT NULL AND B.currency_id = '13' $state_dt AND A.order_id = B.id group by A.product_id) Z LEFT JOIN (SELECT B.categ_id, A.id,C.name,A.default_code,A.name_template,A.product_tmpl_id FROM product_product A,product_template B,product_category C WHERE B.categ_id = C.id AND A.product_tmpl_id = B.id) Y ON Z.product_id = Y.id GROUP BY categ_id, name";
$q2 = "
select C.id as categ_id,C.name,sum(product_uom_qty* sm_price_unit_after_disc3) as final from(
	select *,sum(product_uom_qty* sm_price_unit_after_disc3) as st from sale_order_line where date_order BETWEEN '$start'  AND '$end' $state_dt2
	GROUP BY id) A 
	LEFT JOIN product_category C ON C.id = A.categ_id
	GROUP BY C.id
       ORDER BY C.name

";
		$d = pg_query($dbconn,$q2);
		
		$values = "";
			
		while($dt = pg_fetch_assoc($d)){
			if($dt['name'] == ''){
				$nom = $dt['not_diskon'];
				$sum += $dt['not_diskon'];
				$nm = 'No Product ID';
				$categid = '0';
			}else{
				$nom = $dt['final'];
				$sum += $dt['final'];
				$nm = $dt['name'];
				$categid = $dt['categ_id'];
			}
			
			$values = $values . "('$categid', '$nm', $nom, 'RaTek'),";
		}
		$values = substr($values,0,-1) . ";";
		$query = "INSERT INTO `so_sum" . $usr . "`(`category_id`, `category`, `nominal`, `db`) VALUES " . $values;
		$submit = mysqli_query($con,$query);
	//DB GBU
		$conn_string = "host=192.168.1.111 dbname=GBU user=postgres password=postgres";
		$dbconn = pg_connect($conn_string);
		
		$state_list = "b.state != 'cancel'";
		$state_sales = "AND A.state != 'cancel' AND A.state != 'draft' AND A.state != 'sent'";
//		$q2 ="SELECT categ_id, name,SUM(total) as final,SUM(not_diskon) as not_diskon FROM (SELECT A.product_id ,sum(A.product_uom_qty*A.sm_price_unit_after_disc3) as total,SUM(A.product_uom_qty*A.price_unit) as not_diskon FROM sale_order_line A,sale_order B where B.date_order >= '$start' AND B.date_order <= '$end' AND B.date_order IS NOT NULL AND B.currency_id = '13' $state_dt AND A.order_id = B.id group by A.product_id) Z LEFT JOIN (SELECT B.categ_id, A.id,C.name,A.default_code,A.name_template,A.product_tmpl_id FROM product_product A,product_template B,product_category C WHERE B.categ_id = C.id AND A.product_tmpl_id = B.id) Y ON Z.product_id = Y.id GROUP BY categ_id, name";
$q2 = "
select C.id as categ_id,C.name,sum(product_uom_qty* sm_price_unit_after_disc3) as final from(
	select *,sum(product_uom_qty* sm_price_unit_after_disc3) as st from sale_order_line where date_order BETWEEN '$start'  AND '$end' $state_dt2
	GROUP BY id) A 
	LEFT JOIN product_category C ON C.id = A.categ_id
	GROUP BY C.id
       ORDER BY C.name

";
		$d = pg_query($dbconn,$q2);
		
		$values = "";
			
		while($dt = pg_fetch_assoc($d)){
			if($dt['name'] == ''){
				$nom = $dt['not_diskon'];
				$sum += $dt['not_diskon'];
				$nm = 'No Product ID';
				$categid = '0';
			}else{
				$nom = $dt['final'];
				$sum += $dt['final'];
				$nm = $dt['name'];
				$categid = $dt['categ_id'];
			}
			
			$values = $values . "('$categid', '$nm', $nom, 'GBU'),";
		}
		$values = substr($values,0,-1) . ";";
		$query = "INSERT INTO `so_sum" . $usr . "`(`category_id`, `category`, `nominal`, `db`) VALUES " . $values;
		$submit = mysqli_query($con,$query);
	//DB Naik
		$conn_string = "host=192.168.1.111 dbname=NAIKREATIF user=postgres password=postgres";
		$dbconn = pg_connect($conn_string);
		
		$state_list = "b.state != 'cancel'";
		$state_sales = "AND A.state != 'cancel' AND A.state != 'draft' AND A.state != 'sent'";
//		$q2 ="SELECT categ_id, name,SUM(total) as final,SUM(not_diskon) as not_diskon FROM (SELECT A.product_id ,sum(A.product_uom_qty*A.sm_price_unit_after_disc3) as total,SUM(A.product_uom_qty*A.price_unit) as not_diskon FROM sale_order_line A,sale_order B where B.date_order >= '$start' AND B.date_order <= '$end' AND B.date_order IS NOT NULL AND B.currency_id = '13' $state_dt AND A.order_id = B.id group by A.product_id) Z LEFT JOIN (SELECT B.categ_id, A.id,C.name,A.default_code,A.name_template,A.product_tmpl_id FROM product_product A,product_template B,product_category C WHERE B.categ_id = C.id AND A.product_tmpl_id = B.id) Y ON Z.product_id = Y.id GROUP BY categ_id, name";
$q2 = "
select C.id as categ_id,C.name,sum(product_uom_qty* sm_price_unit_after_disc3) as final from(
	select *,sum(product_uom_qty* sm_price_unit_after_disc3) as st from sale_order_line where date_order BETWEEN '$start'  AND '$end' $state_dt2
	GROUP BY id) A 
	LEFT JOIN product_category C ON C.id = A.categ_id
	GROUP BY C.id
       ORDER BY C.name

";
		$d = pg_query($dbconn,$q2);
		
		$values = "";
			
		while($dt = pg_fetch_assoc($d)){
			if($dt['name'] == ''){
				$nom = $dt['not_diskon'];
				$sum += $dt['not_diskon'];
				$nm = 'No Product ID';
				$categid = '0';
			}else{
				$nom = $dt['final'];
				$sum += $dt['final'];
				$nm = $dt['name'];
				$categid = $dt['categ_id'];
			}
			
			$values = $values . "('$categid', '$nm', $nom, 'Naik'),";
		}
		$values = substr($values,0,-1) . ";";
		$query = "INSERT INTO `so_sum" . $usr . "`(`category_id`, `category`, `nominal`, `db`) VALUES " . $values;
		$submit = mysqli_query($con,$query);
//end loop query


//start display
	$msg = '
<!DOCTYPE html>
<html>
    <head>
<style>
.col-xs-12 {
    width: 100%;
}
.col-xs-6 {
    width: 50%;
}
.col-xs-1 {
    width: 8.33333333%;
}
.col-xs-5 {
    width: 41.66666667%;
}
.col-xs-1, .col-xs-10, .col-xs-11, .col-xs-12, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8, .col-xs-9 {
    float: left;
}
.col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-xs-1, .col-xs-10, .col-xs-11, .col-xs-12, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8, .col-xs-9 {
    position: relative;
    min-height: 1px;
    padding-right: 15px;
    padding-left: 15px;
}
body {
    font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
    font-size: 14px;
    line-height: 1.42857143;
    color: #333;
}
</style>
</head>
<body>';

    $msg .= '
	<div class="col-xs-12" style="border: 1px solid black;"><strong> ' . strtoupper($usrnm) . '</strong></div>
	<div class="col-xs-12">
		<div class="col-xs-6">'.$type_string.'</div>
		<div class="col-xs-6"></div>
    </div>  
    <div class="col-xs-12">
		<div class="col-xs-6">'.$string_state.'</div>
		<div class="col-xs-6">SO Date : '.$start.' to '.$end.' </div>
    </div>              
    <div class="col-xs-12"><hr></div>';


    //$sq = "SELECT A.nama usr, tahun, ". $per . ", C.nama, D.db, idoe, sum(nominal) nominal FROM tblmasteruser A INNER JOIN tblmastertarget B ON A.iduser = B.iduser " .
			"INNER JOIN tblmastercategory C ON B.idcat = C.idcat " .
			"INNER JOIN tblmastercategory_detail D ON C.idcat = D.idcat " .
			"INNER JOIN `so_sum" . $usr . "` E ON E.category = D.idoe AND E.db = D.db " .
			"GROUP BY A.nama, tahun, ". $per . ", C.nama, D.db, idoe " .
			"ORDER BY C.nama, D.db";
	/*$sq = "SELECT A.nama usr, ". $per . " target, tahun, C.nama, D.db, idoe, sum(nominal) nominal, (SELECT sum(nominal) nominal FROM tblmasteruser A INNER JOIN tblmastertarget B ON A.iduser = B.iduser 
		INNER JOIN tblmastercategory C ON B.idcat = C.idcat
		INNER JOIN tblmastercategory_detail D ON C.idcat = D.idcat
		INNER JOIN `so_sum" . $usr . "` E ON E.category = D.idoe AND E.db = D.db
		WHERE A.iduser = '" .$usr ."' AND tahun = " .$thn ."
		) sumall FROM tblmasteruser A INNER JOIN tblmastertarget B ON A.iduser = B.iduser 
		INNER JOIN tblmastercategory C ON B.idcat = C.idcat
		INNER JOIN tblmastercategory_detail D ON C.idcat = D.idcat
		INNER JOIN `so_sum" . $usr . "` E ON E.category = D.idoe AND E.db = D.db
		WHERE A.iduser = '" .$usr ."' AND tahun = " .$thn ."
		GROUP BY A.nama, target, tahun, C.nama, D.db, idoe
		ORDER BY nama, db";*/
	$sq = "SELECT A.nama usr, ". $per . " target, tahun, C.nama, D1.second_name db, idoe, sum(nominal) nominal, (SELECT sum(nominal) nominal FROM tblmasteruser A INNER JOIN tblmastertarget B ON A.iduser = B.iduser 
		INNER JOIN tblmastercategory C ON B.idcat = C.idcat
		INNER JOIN tblmastercategory_detail D ON C.idcat = D.idcat
		INNER JOIN tblmasterdatabase D1 ON D.dbid = D1.id
		INNER JOIN `so_sum" . $usr . "` E ON E.category_id = D.idoe AND E.db = D1.second_name
		WHERE A.iduser = '" .$usr ."' AND tahun = " .$thn .") sumall FROM tblmasteruser A INNER JOIN tblmastertarget B ON A.iduser = B.iduser 
		INNER JOIN tblmastercategory C ON B.idcat = C.idcat
		INNER JOIN tblmastercategory_detail D ON C.idcat = D.idcat
		INNER JOIN tblmasterdatabase D1 ON D.dbid = D1.id
		INNER JOIN `so_sum" . $usr . "` E ON E.category_id = D.idoe AND E.db = D1.second_name
		WHERE A.iduser = '" .$usr ."' AND tahun = " .$thn ." GROUP BY A.nama, target, tahun, C.nama, D1.second_name, idoe
		ORDER BY nama, second_name";
 
	$rs = mysqli_query($con,$sq);
		//echo $q2;
		while($res = mysqli_fetch_assoc($rs)){			
			if ($no == 1){
				$cat = $res['nama'];
				$sum = 0;
			}
			if ($cat !=  $res['nama']){
				$msg .= '<div class="col-xs-12" style="border: 1px solid black;">
					<div class="col-xs-1"><strong>TOTAL '. $cat .'</strong></div>
					<div class="col-xs-1"><strong>' . number_format($sumnom) .' </strong></div>
					<div class="col-xs-1"><strong>TARGET</strong></div>
					<div class="col-xs-1"><strong>' . number_format($target) . ' </strong></div>
					<div class="col-xs-1"><strong>SELISIH ' . $cat . '</strong></div>
					<div class="col-xs-1" '; 
						if ($target-$sumnom < 0){
							$msg .= "style='color:limegreen;'";
						}else{
							$msg .= "style='color:red;'"; 
						}

					$msg .= '><strong>'; 
						if ($target-$sumnom < 0){
							$msg .= number_format($sumnom-$target);
						}else{
							$msg .= number_format($sumnom-$target); 
						}
					$msg .= '</strong></div>
					<div class="col-xs-1"><strong>PENCAPAIAN $cat</strong></div>
					<div class="col-xs-1"><strong>' . number_format($sumnom / $target * 100) . ' % </strong></div>
					<div class="col-xs-1"><strong>KONTRIBUSI ' . $cat . '</strong></div>
					<div class="col-xs-1"><strong>' . number_format($sumnom / $sumall * 100) . ' % </strong></div>
				</div>';

				$sumnom = 0;
				$cat = $res['nama'];
			}

	$msg .= '<div class="col-xs-12" style="border-bottom: 1px dashed orange;">
		<div class="col-xs-1">'. $no .'</div>
		<div class="col-xs-6">'. $res['nama'] . ' - ' . $res['db'] .'</div>
		<div class="col-xs-5" style="text-align:right;">'. number_format($res['nominal']) .'</div>
	</div>';
	
			$sum = $sum + $res['nominal'];
			$sumnom = $sumnom + $res['nominal'];
			$target = $res['target'];
			$sumall = $res['sumall'];
			$no++;
		}
	if ($sumall == 0){
		$kontribusi = 0;
	}else{
		$kontribusi = $sumnom / $sumall * 100;
	}
	$msg .= '<div class="col-xs-12" style="border: 1px solid black;">
		<div class="col-xs-1"><strong>TOTAL '. $cat .'</strong></div>
					<div class="col-xs-1"><strong>'. number_format($sumnom) .' </strong></div>
					<div class="col-xs-1"><strong>TARGET</strong></div>
					<div class="col-xs-1"><strong>'. number_format($target) .' </strong></div>
					<div class="col-xs-1"><strong>SELISIH '. $cat .'</strong></div>
					<div class="col-xs-1" '; 
						if ($target-$sumnom < 0){
							$msg .= "style='color:limegreen;'";
						}else{
							$msg .= "style='color:red;'"; 
						}
					$msg .= '><strong>'; 
						if ($target-$sumnom < 0){
							$msg .= number_format($sumnom-$target);
						}else{
							$msg .= number_format($sumnom-$target); 
						}
					$msg .= '</strong></div>
					<div class="col-xs-1"><strong>PENCAPAIAN '. $cat .'</strong></div>
					<div class="col-xs-1"><strong>'. number_format($sumnom / $target * 100) .' % </strong></div>
					<div class="col-xs-1"><strong>KONTRIBUSI '. $cat .'</strong></div>
					<div class="col-xs-1"><strong>'. number_format($kontribusi) .' % </strong></div>
	</div>
    <div class="col-xs-12"><hr></div> 
    <div class="col-xs-12">
		<div class="col-xs-7"><strong>GRAND TOTAL</strong></div>
		<div class="col-xs-5"><strong>'. number_format($sum) .' </strong></div>
    </div>'; 
?>      
<?php
//drop and create table
	date_default_timezone_set('Asia/Jakarta');
	$localhostdb = '192.168.1.70';
	$usernamedb  = 'root';
	$passworddb  = 'Qwert!123';
	$databasedb  = 'oereporting';
		
	$con = mysqli_connect($localhostdb,$usernamedb,$passworddb,$databasedb)or die("cannot connect"); 

	$query = "DROP TABLE IF EXISTS `so_sls" . $usr ."`";
	$submit = mysqli_query($con,$query);

	$query = "CREATE TABLE `so_sls" . $usr . "` (`category` VARCHAR(50) NOT NULL,`nominal` DOUBLE NOT NULL,`db` VARCHAR(50) NOT NULL) ENGINE=InnoDB";

	$submit = mysqli_query($con,$query);
//end drop and create table


//set filter
	if ($per == 'Q1'){
		$start = $thn . "-01-01";
		$end = $thn . "-03-31";
	}else if ($per == 'Q2'){
		$start = $thn . "-04-01";
		$end = $thn . "-06-30";
	}else if ($per == 'Q3'){
		$start = $thn . "-07-01";
		$end = $thn . "-09-30";
	}else if ($per == 'Q4'){
		$start = $thn . "-10-01";
		$end = $thn . "-12-31";
	}

	$so = "yes";// $_GET['so'];
	$db = "TOTALINPRO_2016"; //$_GET['db'];
	$type =  "out_invoice"; //$_GET['type'];
	$state =  "all"; //$_GET['state'];
	$state_so =  "all"; //$_GET['state'];
	$type_report=  "so"; //$_GET['type_report'];
//end set filter


//start variable
	$sum = 0;
	$sumall = 0;
	$filter = '';
	$filter_prn = '';
	$info = 'No Company Category';
	$no = 1;
	$cat = "";
	if($type_report == 'so'){
		$state_sales = "AND A.state != 'cancel' AND A.state != 'draft' AND A.state != 'sent'";
	        $state_dt = "AND E.state != 'cancel' AND E.state != 'draft' AND E.state != 'sent'";
		$string_state = 'ALL SALES ORDER';
		$type_string = "Document : SO Category Consolidate Sales Report " . $per . " - " . $thn;
	}
//end variable

//start loop query
	//DB TIM
		$sq1 = "SELECT idoe FROM tblmasteruser_detail A INNER JOIN tblmasterdatabase B ON A.dbid = B.id WHERE iduser = '".$usr."' AND second_name = 'TIM'";
		$rs1 = mysqli_query($con,$sq1);
		$res1 = mysqli_fetch_assoc($rs1);
		if ($res1['idoe']=='') {
			$idoe = '-1';
		}else{
			$idoe = $res1['idoe'];
		}

		$conn_string = "host=192.168.1.111 dbname=TOTALINPRO_2016 user=postgres password=postgres";
		$dbconn = pg_connect($conn_string);
		
		$q2 ="SELECT COALESCE(sum(A.sm_price_unit_after_disc3 * product_uom_qty),'0') as final,D.name 
	                FROM res_partner F,sale_order E,sale_order_line A, product_product B,product_template C,product_category D
        		WHERE E.partner_id = F.id $state_dt AND C.categ_id = D.id AND A.product_id = B.id AND B.product_tmpl_id = C.id 
			AND E.user_id='".$idoe."' AND A.order_id = E.id AND E.currency_id = '13' AND E.date_order >= '$start' AND E.date_order <= '$end'  
			GROUP BY D.name ORDER BY D.name";

		$d = pg_query($dbconn,$q2);
		
		$values = "";
			
		while($dt = pg_fetch_assoc($d)){
			$nom = $dt['final'];
			$nm = $dt['name'];
			
			$values = $values . "('$nm', $nom, 'TIM'),";
		}
		$values = substr($values,0,-1) . ";";
		$query = "INSERT INTO `so_sls" . $usr . "`(`category`, `nominal`, `db`) VALUES " . $values;
		$submit = mysqli_query($con,$query);
	//DB WIS
		$sq1 = "SELECT idoe FROM tblmasteruser_detail A INNER JOIN tblmasterdatabase B ON A.dbid = B.id WHERE iduser = '".$usr."' AND second_name =  'WIS'";
		$rs1 = mysqli_query($con,$sq1);
		$res1 = mysqli_fetch_assoc($rs1);
		if ($res1['idoe']=='') {
			$idoe = '-1';
		}else{
			$idoe = $res1['idoe'];
		}

		$conn_string = "host=192.168.1.111 dbname=WILLERTINDO_2016 user=postgres password=postgres";
		$dbconn = pg_connect($conn_string);
		
		$q2 ="SELECT COALESCE(sum(A.sm_price_unit_after_disc3 * product_uom_qty),'0') as final,D.name 
	                FROM res_partner F,sale_order E,sale_order_line A, product_product B,product_template C,product_category D
        		WHERE E.partner_id = F.id $state_dt AND C.categ_id = D.id AND A.product_id = B.id AND B.product_tmpl_id = C.id 
			AND E.user_id='".$idoe."' AND A.order_id = E.id AND E.currency_id = '13' AND E.date_order >= '$start' AND E.date_order <= '$end'  
			GROUP BY D.name ORDER BY D.name";

		$d = pg_query($dbconn,$q2);
		
		$values = "";
			
		while($dt = pg_fetch_assoc($d)){
			$nom = $dt['final'];
			$nm = $dt['name'];
			
			$values = $values . "('$nm', $nom, 'WIS'),";
		}
		$values = substr($values,0,-1) . ";";
		$query = "INSERT INTO `so_sls" . $usr . "`(`category`, `nominal`, `db`) VALUES " . $values;
		$submit = mysqli_query($con,$query);
	//DB Ratek
		$sq1 = "SELECT idoe FROM tblmasteruser_detail A INNER JOIN tblmasterdatabase B ON A.dbid = B.id WHERE iduser = '".$usr."' AND second_name =  'RaTek'";
		$rs1 = mysqli_query($con,$sq1);
		$res1 = mysqli_fetch_assoc($rs1);
		if ($res1['idoe']=='') {
			$idoe = '-1';
		}else{
			$idoe = $res1['idoe'];
		}

		$conn_string = "host=192.168.1.111 dbname=RAJATEKNOLOGI_2016 user=postgres password=postgres";
		$dbconn = pg_connect($conn_string);
		
		$q2 ="SELECT COALESCE(sum(A.sm_price_unit_after_disc3 * product_uom_qty),'0') as final,D.name 
	                FROM res_partner F,sale_order E,sale_order_line A, product_product B,product_template C,product_category D
        		WHERE E.partner_id = F.id $state_dt AND C.categ_id = D.id AND A.product_id = B.id AND B.product_tmpl_id = C.id 
			AND E.user_id='".$idoe."' AND A.order_id = E.id AND E.currency_id = '13' AND E.date_order >= '$start' AND E.date_order <= '$end'  
			GROUP BY D.name ORDER BY D.name";

		$d = pg_query($dbconn,$q2);
		
		$values = "";
			
		while($dt = pg_fetch_assoc($d)){
			$nom = $dt['final'];
			$nm = $dt['name'];
			
			$values = $values . "('$nm', $nom, 'RaTek'),";
		}
		$values = substr($values,0,-1) . ";";
		$query = "INSERT INTO `so_sls" . $usr . "`(`category`, `nominal`, `db`) VALUES " . $values;
		$submit = mysqli_query($con,$query);
	//DB GBU
		$sq1 = "SELECT idoe FROM tblmasteruser_detail A INNER JOIN tblmasterdatabase B ON A.dbid = B.id WHERE iduser = '".$usr."' AND second_name =  'GBU'";
		$rs1 = mysqli_query($con,$sq1);
		$res1 = mysqli_fetch_assoc($rs1);
		if ($res1['idoe']=='') {
			$idoe = '-1';
		}else{
			$idoe = $res1['idoe'];
		}

		$conn_string = "host=192.168.1.111 dbname=GBU user=postgres password=postgres";
		$dbconn = pg_connect($conn_string);
		
		$q2 ="SELECT COALESCE(sum(A.sm_price_unit_after_disc3 * product_uom_qty),'0') as final,D.name 
	                FROM res_partner F,sale_order E,sale_order_line A, product_product B,product_template C,product_category D
        		WHERE E.partner_id = F.id $state_dt AND C.categ_id = D.id AND A.product_id = B.id AND B.product_tmpl_id = C.id 
			AND E.user_id='".$idoe."' AND A.order_id = E.id AND E.currency_id = '13' AND E.date_order >= '$start' AND E.date_order <= '$end'  
			GROUP BY D.name ORDER BY D.name";

		$d = pg_query($dbconn,$q2);
		
		$values = "";
			
		while($dt = pg_fetch_assoc($d)){
			$nom = $dt['final'];
			$nm = $dt['name'];
			
			$values = $values . "('$nm', $nom, 'GBU'),";
		}
		$values = substr($values,0,-1) . ";";
		$query = "INSERT INTO `so_sls" . $usr . "`(`category`, `nominal`, `db`) VALUES " . $values;
		$submit = mysqli_query($con,$query);
	//DB Naik
		$sq1 = "SELECT idoe FROM tblmasteruser_detail A INNER JOIN tblmasterdatabase B ON A.dbid = B.id WHERE iduser = '".$usr."' AND second_name =  'Naik'";
		$rs1 = mysqli_query($con,$sq1);
		$res1 = mysqli_fetch_assoc($rs1);
		if ($res1['idoe']=='') {
			$idoe = '-1';
		}else{
			$idoe = $res1['idoe'];
		}

		$conn_string = "host=192.168.1.111 dbname=NAIKREATIF user=postgres password=postgres";
		$dbconn = pg_connect($conn_string);
		
		$q2 ="SELECT COALESCE(sum(A.sm_price_unit_after_disc3 * product_uom_qty),'0') as final,D.name 
	                FROM res_partner F,sale_order E,sale_order_line A, product_product B,product_template C,product_category D
        		WHERE E.partner_id = F.id $state_dt AND C.categ_id = D.id AND A.product_id = B.id AND B.product_tmpl_id = C.id 
			AND E.user_id='".$idoe."' AND A.order_id = E.id AND E.currency_id = '13' AND E.date_order >= '$start' AND E.date_order <= '$end'  
			GROUP BY D.name ORDER BY D.name";

		$d = pg_query($dbconn,$q2);
		
		$values = "";
			
		while($dt = pg_fetch_assoc($d)){
			$nom = $dt['final'];
			$nm = $dt['name'];
			
			$values = $values . "('$nm', $nom, 'Naik'),";
		}
		$values = substr($values,0,-1) . ";";
		$query = "INSERT INTO `so_sls" . $usr . "`(`category`, `nominal`, `db`) VALUES " . $values;
		$submit = mysqli_query($con,$query);
//end loop query


//start display
	$sq = "SELECT nama usr, tahun, ". $per . " target, category, nominal, db,
		(SELECT SUM(nominal) nominal FROM `so_sls" . $usr . "` A CROSS JOIN 
		tblmasteruser B INNER JOIN tblmastertarget C ON B.iduser = C.iduser
		WHERE B.iduser = '".$usr."' AND C.tahun = '".$thn."' AND C.idcat = '') sumall
		FROM `so_sls" . $usr . "` A CROSS JOIN 
		tblmasteruser B INNER JOIN tblmastertarget C ON B.iduser = C.iduser
		WHERE B.iduser = '".$usr."' AND C.tahun = '".$thn."' AND C.idcat = ''";
 
	$rs = mysqli_query($con,$sq);
		//echo $q2;
		while($res = mysqli_fetch_assoc($rs)){
			if ($no == 1){
				$cat = $res['category'];
				$sum = 0;
				$msg .= '<div class="col-xs-12" style="border: 1px solid black;"><strong>'. strtoupper($res['usr']) .'</strong></div>';

			}
	$msg .= '<div class="col-xs-12" style="border-bottom: 1px dashed orange;">
		<div class="col-xs-1">'. $no .'</div>
		<div class="col-xs-6">'. $res['category'] . ' - ' . $res['db'] .'</div>
		<div class="col-xs-5" style="text-align:right;">'. number_format($res['nominal']) .'</div>
	</div>';

			$sum = $sum + $res['nominal'];
			$no++;
			$target = $res['target'];
		}

    $msg .= '<div class="col-xs-12"><hr></div> 
    <div class="col-xs-12">
		<div class="col-xs-1"><strong>GRAND TOTAL</strong></div>
		<div class="col-xs-1"><strong>'. number_format($sum) .' </strong></div>
		<div class="col-xs-1"><strong>TARGET</strong></div>
		<div class="col-xs-1"><strong>'. number_format($target) .' </strong></div>
		<div class="col-xs-1"><strong>SELISIH</strong></div>
		<div class="col-xs-1"'; 
			if ($target-$sum < 0){
				$msg .= "style='color:limegreen;'";
			}else{
				$msg .= "style='color:red;'"; 
			}
		$msg .= '><strong>'; 
			if ($target-$sumnom < 0){
				$msg .= number_format($sum-$target);
			}else{
				$msg .= number_format($sum-$target); 
			}
		$msg .= '</strong></div>
		<div class="col-xs-1"><strong>PENCAPAIAN</strong></div>
		<div class="col-xs-1"><strong>'. number_format($sum / $target * 100) .' % </strong></div>
    </div>';
$msg .= '</body>
</html>';
$msgceo .= $msg . '<br><br><hr><br><br>';
?>