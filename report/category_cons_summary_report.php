<link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">

<?php 
session_start();

//drop and create table
	date_default_timezone_set('Asia/Jakarta');
	$localhostdb = '192.168.1.70';
	$usernamedb  = 'root';
	$passworddb  = 'Qwert!123';
	$databasedb  = 'oereporting';
		
	$con = mysqli_connect($localhostdb,$usernamedb,$passworddb,$databasedb)or die("cannot connect"); 

	$query = "DROP TABLE IF EXISTS `so_sum".$_SERVER['REMOTE_ADDR']."`";
	$submit = mysqli_query($con,$query);

	$query = "CREATE TABLE `so_sum".$_SERVER['REMOTE_ADDR']."` (`category_id` VARCHAR(10) NOT NULL,`category` VARCHAR(50) NOT NULL,`nominal` DOUBLE NOT NULL,`db` VARCHAR(50) NOT NULL) ENGINE=InnoDB";

	$submit = mysqli_query($con,$query);
//end drop and create table


//set filter
	if ($_GET['per'] == 'Q1'){
		$start = $_GET['thn'] . "-01-01";
		$end = $_GET['thn'] . "-03-31";
	}else if ($_GET['per'] == 'Q2'){
		$start = $_GET['thn'] . "-04-01";
		$end = $_GET['thn'] . "-06-30";
	}else if ($_GET['per'] == 'Q3'){
		$start = $_GET['thn'] . "-07-01";
		$end = $_GET['thn'] . "-09-30";
	}else if ($_GET['per'] == 'Q4'){
		$start = $_GET['thn'] . "-10-01";
		$end = $_GET['thn'] . "-12-31";
	}

	$so = "yes";// $_GET['so'];
	$db = "TOTALINPRO_2016"; //$_GET['db'];
	$type =  "out_invoice"; //$_GET['type'];
	$state =  "all"; //$_GET['state'];
	$type_report=  "so"; //$_GET['type_report'];
//end set filter
    if($type_report == 'so'){
      $filter = "AND B.state != 'cancel' AND B.state != 'draft' AND B.state != 'sent'";
    }else{
      $filter = "AND B.state != 'cancel' AND (B.state = 'draft' OR B.state = 'sent' ) ";
    }
	$string_state = 'ALL SALES ORDER';
	$type_string = "Document : SO Category Consolidate Report " . $_GET['per'] . " - " . $_GET['thn'];

	$values = '';
	$q_main = mysqli_query($con,"SELECT * FROM( SELECT * FROM `tblmasteruser_detail` where iduser = '".$_GET['usr']."' ) A LEFT JOIN tblmasterdatabase B ON A.dbid = B.id");
	while($main = mysqli_fetch_assoc($q_main)){
		if($main['nama'] == 'GBU'){
			$ip = $_SESSION['ip_vps'];
		}else{
			$ip = $_SESSION['ip_local'];
		}
		$conn_string = "host=".$ip." dbname=".$main['nama']." user=".$_SESSION['user_pg']." password=";
		$dbconn = pg_connect($conn_string);	
		$query = pg_query($dbconn,"SELECT W.id,W.name,sum(Z.sm_price_unit_after_disc3 * Z.product_uom_qty) as after3,sum(Z.sm_price_unit_before_disc * Z.product_uom_qty) as no_categ FROM(
            SELECT A.*,B.name,B.date_order FROM sale_order_line A,sale_order B where A.order_id = B.id $filter AND B.date_order BETWEEN '$start 00:00:00' AND '$end 23:59:59' AND B.user_id = '".$main['idoe']."'
            ) Z 
            LEFT JOIN (select id,default_code,product_tmpl_id from product_product) Y ON Z.product_id = Y.id
            LEFT JOIN (SELECT id,categ_id FROM product_template) X ON Y.product_tmpl_id = X.id
            LEFT JOIN product_category W ON W.id = X.categ_id
            GROUP BY W.id ORDER BY W.name");
        while($data = pg_fetch_assoc($query)){
	        if($data['id']){
	          $nm = $data['name'];
	          $nom = $data['after3'];
	        }else{
	          $nm = 'No Category';
	          $nom = $data['no_categ'];
	        }    
	        $categid = $data['id'];
	        $values = $values . "('$categid', '$nm', $nom, '".$main['second_name']."'),";    	
        	//echo $data['name'];
        }
		
		//echo $main['nama'].' - '.$values;
	}

	$values = substr($values,0,-1) . ";";
	$query = "INSERT INTO `so_sum".$_SERVER['REMOTE_ADDR']."`(`category_id`, `category`, `nominal`, `db`) VALUES " . $values;
	$submit = mysqli_query($con,$query);


//start display
?>
    <div class="col-xs-12">
		<div class="col-xs-6"><?php echo $type_string; ?></div>
		<div class="col-xs-6"></div>
    </div>  
    <div class="col-xs-12">
		<div class="col-xs-6"><?php echo $string_state; ?></div>
		<div class="col-xs-6">SO Date : <?php echo $start; ?> to  <?php echo $end;?> </div>
    </div>              
    <div class="col-xs-12"><hr></div> 

<?php
	$sumnom = 0;
	$no = 1;
	$sum = 0;
	$sq = "SELECT A.nama usr, ". $_GET['per'] . " target, tahun, C.nama, D1.second_name db, idoe, sum(nominal) nominal, (SELECT sum(nominal) nominal FROM tblmasteruser A INNER JOIN tblmastertarget B ON A.iduser = B.iduser 
		INNER JOIN tblmastercategory C ON B.idcat = C.idcat
		INNER JOIN tblmastercategory_detail D ON C.idcat = D.idcat
		INNER JOIN tblmasterdatabase D1 ON D.dbid = D1.id
		INNER JOIN `so_sum::1` E ON E.category_id = D.idoe AND E.db = D1.second_name
		WHERE A.iduser = '" .$_GET['usr'] ."' AND tahun = " .$_GET['thn'] .") sumall FROM tblmasteruser A INNER JOIN tblmastertarget B ON A.iduser = B.iduser 
		INNER JOIN tblmastercategory C ON B.idcat = C.idcat
		INNER JOIN tblmastercategory_detail D ON C.idcat = D.idcat
		INNER JOIN tblmasterdatabase D1 ON D.dbid = D1.id
		INNER JOIN `so_sum::1` E ON E.category_id = D.idoe AND E.db = D1.second_name
		WHERE A.iduser = '" .$_GET['usr'] ."' AND tahun = " .$_GET['thn'] ." GROUP BY A.nama, target, tahun, C.nama, D1.second_name, idoe
		ORDER BY nama, second_name";
 
	$rs = mysqli_query($con,$sq);
		//echo $q2;
		while($res = mysqli_fetch_assoc($rs)){			
			if ($no == 1){
				$cat = $res['nama'];
				$sum = 0;
		?>
		<div class="col-xs-12" style="border: 1px solid black;"><strong><?= strtoupper($res['usr']) ?></strong></div>
		<?php
			}
			if ($cat !=  $res['nama']){
		?>
				<div class="col-xs-12" style="border: 1px solid black;">
					<div class="col-xs-6" style="border-bottom: 1px dashed orange;"><strong>TOTAL <?= $cat ?></strong></div>
					<div class="col-xs-6" style="border-bottom: 1px dashed orange;"><strong><?php echo number_format($sumnom); ?> </strong></div>
					<div class="col-xs-6" style="border-bottom: 1px dashed orange;"><strong>TARGET</strong></div>
					<div class="col-xs-6" style="border-bottom: 1px dashed orange;"><strong><?php echo number_format($target); ?> </strong></div>
					<div class="col-xs-6" style="border-bottom: 1px dashed orange;"><strong>SELISIH <?= $cat ?></strong></div>
					<div class="col-xs-6" style="border-bottom: 1px dashed orange; <?php 
						if ($target-$sumnom < 0){
							echo "color:limegreen;";
						}else{
							echo "color:red;"; 
						}
					?>
					"><strong><?php 
						if ($target-$sumnom < 0){
							echo number_format($sumnom-$target);
						}else{
							echo number_format($sumnom-$target); 
						}
					?> </strong></div>
					<div class="col-xs-6" style="border-bottom: 1px dashed orange;"><strong>PENCAPAIAN <?= $cat ?></strong></div>
					<div class="col-xs-6" style="border-bottom: 1px dashed orange;"><strong><?php echo number_format($sumnom / $target * 100); ?> % </strong></div>
					<div class="col-xs-6" style="border-bottom: 1px dashed orange;"><strong>KONTRIBUSI <?= $cat ?></strong></div>
					<div class="col-xs-6" style="border-bottom: 1px dashed orange;"><strong><?php echo number_format($sumnom / $sumall * 100); ?> % </strong></div>
				</div>
		<?php
				$sumnom = 0;
				$cat = $res['nama'];
			}
		?>
	<div class="col-xs-12" style="border-bottom: 1px dashed orange;">
		<div class="col-xs-1"><?php echo $no; ?></div>
		<div class="col-xs-6"><?php echo $res['nama'] . " - " . $res['db']; ?></div>
		<div class="col-xs-5" style="text-align:right;"><?php echo number_format($res['nominal']); ?></div>
	</div>
		<?php
			$sum = $sum + $res['nominal'];
			$sumnom = $sumnom + $res['nominal'];
			$target = $res['target'];
			$sumall = $res['sumall'];
			$no++;
		} ?>
	<div class="col-xs-12" style="border: 1px solid black;">
		<div class="col-xs-6" style="border-bottom: 1px dashed orange;"><strong>TOTAL <?= $cat ?></strong></div>
					<div class="col-xs-6" style="border-bottom: 1px dashed orange;"><strong><?php echo number_format($sumnom); ?> </strong></div>
					<div class="col-xs-6" style="border-bottom: 1px dashed orange;"><strong>TARGET</strong></div>
					<div class="col-xs-6" style="border-bottom: 1px dashed orange;"><strong><?php echo number_format($target); ?> </strong></div>
					<div class="col-xs-6" style="border-bottom: 1px dashed orange;"><strong>SELISIH <?= $cat ?></strong></div>
					<div class="col-xs-6" style="border-bottom: 1px dashed orange; <?php 
						if ($target-$sumnom < 0){
							echo "color:limegreen;";
						}else{
							echo "color:red;"; 
						}
					?>
					"><strong><?php 
						if ($target-$sumnom < 0){
							echo number_format($sumnom-$target);
						}else{
							echo number_format($sumnom-$target); 
						}
					?> </strong></div>
					<div class="col-xs-6" style="border-bottom: 1px dashed orange;"><strong>PENCAPAIAN <?= $cat ?></strong></div>
					<div class="col-xs-6" style="border-bottom: 1px dashed orange;"><strong><?php echo number_format($sumnom / $target * 100); ?> % </strong></div>
					<div class="col-xs-6" style="border-bottom: 1px dashed orange;"><strong>KONTRIBUSI <?= $cat ?></strong></div>
					<div class="col-xs-6" style="border-bottom: 1px dashed orange;"><strong><?php echo number_format($sumnom / $sumall * 100); ?> % </strong></div>
	</div>
    <div class="col-xs-12"><hr></div> 
    <div class="col-xs-12">
		<div class="col-xs-7"><strong>GRAND TOTAL</strong></div>
		<div class="col-xs-5"><strong><?php echo number_format($sum); ?> </strong></div>
    </div>      
      

<script>

/*  var printContent = document.getElementById('reports').innerHTML;
    var original = document.body.innerHTML;

    document.body.innerHTML = printContent;
    //alert("elelel");
    window.print();
    document.body.innerHTML = original;  
*/
    window.print();

</script>