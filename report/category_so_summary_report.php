
    <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">

<?php 
session_start();
$conn_string = "host=".$_SESSION['host']." dbname=".$_GET['db']." user=".$_SESSION['user_db']." password=".$_SESSION['pass_db']."";
$dbconn = pg_connect($conn_string);

$so = $_GET['so'];
$db = $_GET['db'];
$start = $_GET['start'];
$end =  $_GET['end'];
$type =  $_GET['type'];
$state =  $_GET['state'];
$type_report=  $_GET['type_report'];
$filter = '';

$no = 1;
$c = pg_query($dbconn,"SELECT * FROM res_company");
$comp = pg_fetch_array($c);

$type_string = "Document : Category Summary Report";
if($_GET['type_report'] == 'so'){
  $string_state = 'SALES ORDER';
}else{
  $string_state = 'DRAFT AND SENT QUOTATION';
}
if($type_report == 'so'){
  $filter = "AND B.state != 'cancel' AND B.state != 'draft' AND B.state != 'sent'";
}else{
  $filter = "AND B.state != 'cancel' AND (B.state = 'draft' or B.state = 'sent' )";
}
?>
  
  <div class="col-xs-12"><strong><?php echo $comp['name']; ?></strong></div>
  <div class="col-xs-12"><?php echo $comp['msb_po_address_1']; ?></div> 
  <div class="col-xs-12"><?php echo $comp['msb_po_address_2']; ?></div> 
  <div class="col-xs-12"><?php echo $comp['msb_po_phone']; ?></div>
  <div class="col-xs-12"><?php echo $comp['msb_po_city']; ?></div>  
  <div class="col-xs-12"><?php echo $comp['msb_po_email']; ?></div>
  <div class="col-xs-12">______________________________________________________________________________________________</div> 
  <div class="col-xs-12">
    <div class="col-xs-6"><?php echo $type_string; ?></div>
    <div class="col-xs-6"></div>
  </div>  
  <div class="col-xs-12">
    <div class="col-xs-6"><?php echo $string_state; ?></div>
    <div class="col-xs-6">Date : <?php echo $start; ?> to  <?php echo $end;?> </div>
  </div>              
  <div class="col-xs-12">______________________________________________________________________________________________</div> 
<?php
  $q = "SELECT W.id,W.name,sum(Z.sm_price_unit_after_disc3 * Z.product_uom_qty) as after3,sum(Z.sm_price_unit_before_disc * Z.product_uom_qty) as no_categ FROM(
        SELECT A.*,B.name,B.date_order FROM sale_order_line A,sale_order B where A.order_id = B.id $filter AND B.date_order BETWEEN '$start 00:00:00' AND '$end 23:59:59'
        ) Z 
        LEFT JOIN (select id,default_code,product_tmpl_id from product_product) Y ON Z.product_id = Y.id
        LEFT JOIN (SELECT id,categ_id FROM product_template) X ON Y.product_tmpl_id = X.id
        LEFT JOIN product_category W ON W.id = X.categ_id
        GROUP BY W.id ORDER BY W.name";
  $m = pg_query($dbconn,$q);
  //echo $q;
  $no = 1;
  $sum = 0;
  while($main = pg_fetch_assoc($m)){
    if($main['id']){
      $nm = $main['name'];
      $nom = $main['after3'];
    }else{
      $nm = 'No Category';
      $nom = $main['no_categ'];
    }
    
    ?>
      <div class="col-xs-12">
        <div class="col-xs-1"><?php echo $no; ?></div>
        <div class="col-xs-6"><?php echo $nm; ?></div>
        <div class="col-xs-5"><?php echo number_format($nom); ?></div>
      </div>
    <?php
    $no++;
    $sum += $nom;
  }
?>  

  <div class="col-xs-12">______________________________________________________________________________________________</div> 
  <div class="col-xs-12">
    <div class="col-xs-7"><strong>Summary</strong></div>
    <div class="col-xs-5"><strong><?php echo number_format($sum); ?> </strong></div>
  </div>      
       

<script>

/*  var printContent = document.getElementById('reports').innerHTML;
    var original = document.body.innerHTML;

    document.body.innerHTML = printContent;
    //alert("elelel");
    window.print();
    document.body.innerHTML = original;  
*/
    window.print();

</script>