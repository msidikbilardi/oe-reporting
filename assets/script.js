$('.list_sls').autocomplete
({
    serviceUrl: 'assets/ajax/list_sls.php',
    onSelect: function (suggestion)
    {
      $('#idusr').val(suggestion.data);
      $('#namausr').val(suggestion.nama);
    }
});

var db = $("#db_choose").val();
$('.list_categoe').autocomplete
({
    serviceUrl: 'assets/ajax/list_categoe.php?db='+db,
    onSelect: function (suggestion)
    {
      $('#user_id').val(suggestion.data);
      $('#user_name').val(suggestion.value);
    }
});

$('.list_useroe').autocomplete
({
    serviceUrl: 'assets/ajax/list_useroe.php?db='+db,
    onSelect: function (suggestion)
    {
      $('#user_id').val(suggestion.data);
      $('#user_name').val(suggestion.value);
    }
});


//var $datatable = $('#tbl_user');

$('#tbl_user').dataTable({
  'keys': true,
  'order': [[ 1, 'asc' ]],
  'columns': [
              { data: 'idusr' },
              { data: 'db' },
              { data: 'idoe' },
            ],
  'columnDefs': [
    { orderable: false, targets: [0] }
  ]
});

function addRow_usr(){
  var nama = $("#nama").val();
  var db = $("#db").val();
  var idoe = $("#idoe").val();

  if(nama == '' || db == '' || idoe == '' ){
    alert('Data belum lengkap');
  }else{
    var table = $('#tbl_user').DataTable();
    table.row.add( {
            "idusr": nama,
            "db": db,
            "idoe": idoe,
        } ).draw();
    $("#nama").val('');
    $("#db").val('');
    $("#idoe").val('');
  }
}



var $datatable = $('#tbl_target');

$datatable.dataTable({
  'keys': true,
  'order': [[ 1, 'asc' ]],
  'columns': [
              { data: 'idcat' },
              { data: 'cat' },
              { data: 'tahun' },
              { data: 'q1' },
              { data: 'q2' },
              { data: 'q3' },
              { data: 'q4' },
            ],
  'columnDefs': [
    {
                "targets": [ 0 ],
                "visible": false
    }
  ]
});
function CatView(id){
	window.location.replace("?role=master&page=master_category_view&bkt="+id);
	//alert(id);

}

function UserView(id){
	window.location.replace("?role=master&page=master_user_view&bkt="+id);
	//alert(id);

}


function TargetView(id){
	window.location.replace("?role=master&page=master_target_view&bkt="+id);
	//alert(id);

}

function addCommas(nStr)
{
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}
