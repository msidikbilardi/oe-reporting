<?php
$data = array();

//DB TIM
  $conn_string = "host=192.168.1.111 dbname=TOTALINPRO_2016 user=postgres password=postgres";
  $dbconn = pg_connect($conn_string);

  $q2 ="SELECT A.*, B.*, A.date_order as dtorder, C.login FROM ( 
	select * from (
		SELECT a.msb_category, b.user_id, b.date_order, a.name,COALESCE(sum(amount_untaxed),'0') as total, a.id
		FROM res_partner a, sale_order b
		WHERE b.currency_id = '13' AND b.state != 'cancel' AND b.state != 'draft' AND b.state != 'sent' AND b.partner_id = a.id
		GROUP BY a.name,a.id, b.user_id, b.date_order
	) Z
	LEFT JOIN (
		select id as cat_id,name as name_cat,parent_id from res_partner_category
	) Y ON Z.msb_category = Y.cat_id
) A
LEFT JOIN (
	SELECT Z.id, user_id,date_order, name as cat,SUM(total) as final, SUM(not_diskon) as not_diskon
	FROM (
		SELECT F.id,A.product_id, b.user_id , B.date_order, sum(A.product_uom_qty*A.sm_price_unit_after_disc3) as total,SUM(A.product_uom_qty*A.price_unit) as not_diskon
		FROM sale_order_line A,sale_order B,res_partner F
		where B.partner_id = F.id AND B.date_order IS NOT NULL AND B.currency_id = '13' AND b.state != 'cancel' AND b.state != 'draft' AND b.state != 'sent' AND A.order_id = B.id
		group by A.product_id,F.id, b.user_id, B.date_order
	) Z
	LEFT JOIN (
		SELECT A.id,C.name,A.default_code,A.name_template,A.product_tmpl_id
		FROM product_product A,product_template B,product_category C
		WHERE B.categ_id = C.id AND A.product_tmpl_id = B.id
	) Y ON Z.product_id = Y.id
	GROUP BY name,Z.id, user_id, date_order
) B ON A.id = B.id AND A.user_id = B.user_id AND a.date_order = b.date_order
INNER JOIN res_users C ON A.user_id = C.id";

  $d = pg_query($dbconn,$q2);


  while($dt = pg_fetch_assoc($d)){
    $rows = array();
    $rows[] = $dt['login'];
	$rows[] = $dt['dtorder'];
    $rows[] = $dt['name'];
    $rows[] = $dt['cat'];
    $rows[] = number_format($dt['not_diskon']);
    $data[] = $rows;
	}
//

//proses datatables
    $json_data = array
     (
       "data"            => $data
     );
     print json_encode($json_data);
?>
