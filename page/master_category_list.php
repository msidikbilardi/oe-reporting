<style>
hr {
    display: block;
    height: 1px;
    border: 0;
    border-top: 1px solid #ccc;
    margin: 1em 0;
    padding: 0;
}
</style>

<form class="form-horizontal" action="#" method="POST" enctype="multipart/form-data">

<div class="nav-tabs-custom" >
	<!-- Tabs within a box -->
	<ul class="nav nav-tabs pull-right ui-sortable-handle">
		<li class="active" onClick="tab_content('new');"><a href="#revenue-chart" data-toggle="tab">Generate</a></li>
		<li class="pull-left header"><i class="fa fa-inbox"></i>Master Category</li>
	</ul>
	<div class="tab-content no-padding" >
	<!-- Morris chart - Sales -->
		<div class="chart tab-pane active" id="revenue-chart" style="height: 100%; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
			<div class="box">
				<div class="box-body">
					<table id="tbl_target_list" class="table table-bordered table-hover">
	                    <thead>
	                        <tr>
	                            <th class="col-sm-1">No</th>
	                            <th class="col-sm-9">Name</th>
	                            <th class="col-sm-2"></th>
	                        </tr>
	                    </thead>
	                    <tbody>
				<?php 
				$no = 1;
				$q = mysqli_query($con,"SELECT * FROM tblmastercategory where status = 1"); ?>
				<?php while($qq = mysqli_fetch_assoc($q)){ ?>
	                        <tr>
					<td><?php echo $no; ?></td>
					<td><?php echo $qq['nama']; ?></td>
					<td><div class="btn btn-flat btn-success" onClick="CatView('<?php echo $qq['idcat']; ?>');">View</div></td>
				   </tr>
				<?php $no++; } ?>
	                    </tbody>     
                    </table>		
					

				</div>
			</div>			
		</div>
	</div>

</div>
<div class="box-footer">   
      <a href="?role=master&page=master_category&v=master_category&s=Master Category"><div class="btn btn-flat btn-success pull-right" class="btn btn-success btn-flat pull-right"><i class="glyphicon glyphicon-download"></i>New !</div> </a>
    </div>
  </form>
<script src="assets/js/jquery.js"></script>
<script type="text/javascript" src="assets/datatables/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="assets/datatables/js/dataTables.bootstrap.min.js"></script>
<script>

$(document).ready(function(){


    var table = $('#tbl_target_list').DataTable();
 
   
} );

</script>
