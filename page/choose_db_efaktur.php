<div class="col-md-6">
	<div class="nav-tabs-custom" >
	                <!-- Tabs within a box -->
	<ul class="nav nav-tabs pull-right ui-sortable-handle">
	  <li class="active" onClick="tab_content('new');"><a href="#revenue-chart" data-toggle="tab">Generate</a></li>
	 
	  <li class="pull-left header"><i class="fa fa-inbox"></i> e-Faktur Export</li>
	</ul>
		<div class="tab-content no-padding" >
		<!-- Morris chart - Sales -->
			<div class="chart tab-pane active" id="revenue-chart" style="height: 100%; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
				<div class="box">
				    <div class="box-header">
				      <h3 class="box-title">Form Database</h3>
				    </div><!-- /.box-header -->
				    <div class="box-body">
					<form class="form-horizontal"  enctype="multipart/form-data">
						    <div class="form-group">
						      <label for="inputEmail3" class="col-sm-2 control-label">Database </label>
						      <div class="col-sm-10">
								<select class="form-control" id="db" onChange="changeDB();" name="db">
								<option value="">---- Pilih Database ----</option>
								<option value="TOTALINPRO_2016">TOTALINPRO</option>
								<option value="WILLERTINDO_2016">WILLERTINDO</option>
								<option value="RAJATEKNOLOGI_2016">RAJA TEKNOLOGI</option>
								<option value="GBU">GBU</option>							
								</select>
						        <!--input type="file" class="form-control" id="date_start" required name="date_start" -->
						      </div>
						    </div>
							
					</form>		

					<div id="opsiFilter"></div>

				  	</div>
		
				 </div>			
			</div>
		</div>
	</div>
</div>
<div class="col-md-6">
	<div id="invoice_detail"></div>
</div>
<script src="assets/js/jquery.js"></script>
<script type="text/javascript" src="assets/datatables/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="assets/datatables/js/dataTables.bootstrap.min.js"></script>
<script>

 
function changeDB(){

	var db = $("#db").val();
	$("#opsiFilter").load("serverside/efaktur_filter.php?db="+db);
	//$("#opsiFilter").load("serverside/category_summary_filter.php?db=");

	//alert(db);
}

</script>