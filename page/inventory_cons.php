
<div class="nav-tabs-custom" >
                <!-- Tabs within a box -->
<ul class="nav nav-tabs pull-right ui-sortable-handle">
  <li class="active" onClick="tab_content('new');"><a href="#revenue-chart" data-toggle="tab">Generate</a></li>

  <li class="pull-left header"><i class="fa fa-inbox"></i> Generate Quantity on Hand</li>
</ul>
	<div class="tab-content no-padding" >
	<!-- Morris chart - Sales -->
		<div class="chart tab-pane active" id="revenue-chart" style="height: 100%; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
			<div class="box">
			    <div class="box-header">
			      <h3 class="box-title">Form Report</h3>
			    </div><!-- /.box-header -->
			    <div class="box-body">
            <form class="form-horizontal" enctype="multipart/form-data">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label"></label>
                  <div class="col-sm-12">
                    <div class="checkbox">
                      <div class="col-sm-4">
                          <label>
                            <input type="checkbox" checked id="nullfield" name="nullfield">
                            Hidden zero stock
                          </label>
                      </div>
                      <div class="col-sm-4">
                          <label>
                            <input type="checkbox" onChange="bestSearch();" id="best" name="best">
                            Best Performance Item
                          </label>
                      </div>
                      <div class="col-sm-4" onClick="openModal();">
                          <label>
                            <input type="checkbox"  disabled="disabled" id="cost" name="cost">
                            Show Cost Price
                          </label>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-sm-5">
                    <div class="form-group groupdate">
                      <label for="inputEmail3" class="control-label">Date Start</label>

                        <input type="date" class="form-control" id="date_start" name="date_start" >

                        <!--input type="file" class="form-control" id="date_start" required name="date_start" -->

                    </div>
                </div>
                <div class="col-sm-2"></div>
                <div class="col-sm-5">
                    <div class="form-group groupdate" >
                      <label for="inputEmail3" class="control-label">Date End</label>

                        <input type="date" class="form-control" id="date_end" name="date_end" >

                        <!--input type="file" class="form-control" id="date_start" required name="date_start" -->

                    </div>
                </div>
            <div class="box-footer">
                <div onclick="preview();" class="btn btn-flat btn-success pull-right" type="submit" class="btn btn-success btn-flat pull-right"><i class="glyphicon glyphicon-download"></i> Go!</div>
            </div><!-- /.box-footer -->
            <div id="preview"></div>
            </form>
			  	</div>
			 </div>
		</div>
	</div>
</div>
                  <div class="modal fade bs-example-modal-lg" id="modalAuthorized" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-md">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Authorized </h4>
                          </div>
                          <div class="modal-body">
                            <div class="col-xs-12">
                                <form class="form-horizontal">

                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                            <input type="text" id="user" class="form-control" placeholder="Username">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                            <input type="password" id="pass" class="form-control" placeholder="Password">
                                        </div>
                                    </div>
                                </form>
                            </div>


                          </div>
                          <div class="modal-footer">
                            <div  class="btn btn-default pull-left" data-dismiss="modal">Close</div>
                            <div  class="btn btn-primary" onClick="doAuthotized();">Authorized Login</div>
                          </div>
                        </div>
                    </div>
                  </div>
<script src="assets/js/jquery.js"></script>


<!--script type="text/javascript" src="assets/datatables/js/jquery.dataTables.js"></script-->
<!--script type="text/javascript" src="assets/datatables/js/dataTables.bootstrap.min.js"></script-->
<script>
$(document).ready(function() {
    $('.groupdate').css({'display':'none'});
    $('#groupref').css({'display':'none'});

});
function bestSearch(){
	var best = $('#best:checked').val();
	if(best == 'on'){
		$('#groupref').css({'display':'none'});
		$('.groupdate').css({'display':''});
	}else{
		$('#groupref').css({'display':'none'});
		$('.groupdate').css({'display':'none'});
	}
}

function preview(){
	var best = $('#best:checked').val();
	var categ = $("#categ").val();
	var categ2 = $("#categ2").val();
	var categ3 = $("#categ3").val();
	var ref = $("#ref").val();
	var nullfield = $('#nullfield:checked').val();
	var start = $("#date_start").val();
	var end = $("#date_end").val();
	var gudang = $("#gudang").val();
	var pl = $("#pl").val();
	var cost = $('#cost:checked').val();


//		alert("Data is still Processing");
//	    $("#preview").html('<div class="col-xs-5"></div><div class="col-xs-2"><img src="img/ajax-loader1.gif"/></div><div class="col-xs-5"></div>');
	    $("#preview").html('<center><img src="assets/img/ajax-loader1.gif"/></center>');

		$("#preview").load("serverside/inventory_list_cons.php?cat="+categ+"&ref="+ref+"&nullfield="+nullfield+"&start="+start+"&end="+end+"&best="+best+"&gudang="+gudang+"&categ2="+categ2+"&categ3="+categ3+"&pl="+pl+"&cost="+cost);

}

function openModal(){
  if ($('#cost').is(":checked"))
  {
    // it is checked
    // alert('asd');
	$("#cost").prop("checked", false);
  }else{
    $('#modalAuthorized').modal('show');
  }
   console.log('');
}
function doAuthotized(){
    var user = $('#user').val();
    var pass = $('#pass').val();
	if(user || pass){
    $.ajax({
      type: "POST",
      url: "inc/class.func.php?action=doAuthorized",
      data: "user="+user+"&pass="+pass,
      cache: false,
      success: function(msg){
      //location.reload();
      console.log(msg);
      if(msg > 0){
        $('#modalAuthorized').modal('toggle');
     $("#cost").prop("checked", true);
      }else{
        alert("You dont have permissions to access this");
      }
    }});

	}else{
		alert('Ada data yang kosong');
	}

}

</script>
