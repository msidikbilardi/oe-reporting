<style>
hr {
    display: block;
    height: 1px;
    border: 0;
    border-top: 1px solid #ccc;
    margin: 1em 0;
    padding: 0;
}
</style>

<?php 

$val = $_GET['v'];
$s = $_GET['s'];
$so = $_GET['so']; 
?>
<form class="form-horizontal" action="#" method="POST" enctype="multipart/form-data">

<div class="nav-tabs-custom" >
	<!-- Tabs within a box -->
	<ul class="nav nav-tabs pull-right ui-sortable-handle">
		<li class="active" onClick="tab_content('new');"><a href="#revenue-chart" data-toggle="tab">Generate</a></li>
		<li class="pull-left header"><i class="fa fa-inbox"></i>Master User - NEW</li>
	</ul>
	<div class="tab-content no-padding" >
	<!-- Morris chart - Sales -->
		<div class="chart tab-pane active" id="revenue-chart" style="height: 100%; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
			<div class="box">
				<div class="box-body">

					<div class="form-group">
						<label class="control-label col-md-2 col-sm-2 col-xs-12" for="first-name">Nama *</label>
						<div class="col-md-10 col-sm-10 col-xs-12">
							<input type="text" id="nama" name="nama" class="form-control col-md-7 col-xs-12">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2 col-sm-2 col-xs-12" for="first-name">Email</label>
						<div class="col-md-10 col-sm-10 col-xs-12">
							<input type="text" id="ket" name="ket" class="form-control col-md-7 col-xs-12">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2 col-sm-2 col-xs-12" for="first-name">Database</label>
						<div class="col-md-10 col-sm-10 col-xs-12">
							<select class="form-control" id="db" name="db" onChange="chgDB();">
								<option value="">---- Pilih Database ----</option>
								<?php $d = mysqli_query($con,"SELECT * FROM tblmasterdatabase where status = 1 ORDER BY nama"); ?>
								<?php while($db = mysqli_fetch_assoc($d)) { ?>
									<option value="<?php echo $db['id'].','.$db['nama'].','.$db['second_name']; ?>"><?php echo $db['nama']; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div id="list_user"></div>	
					<input type="hidden" id="db2" name="db2"><input type="hidden" id="db_choose" name="db_choose">
					<input type="hidden" id="user_id" name="user_id"><input type="hidden" id="user_name" name="user_name">
					<hr />					
					<div class="col-xs-12">	
						<div class="col-xs-6"><div class="btn btn-danger btn-flat del_btn" id="del_btn">Hapus Item</div></div>			
						<div class="col-xs-6">
							<div onclick="AddtblUser();" class="btn btn-flat btn-success pull-right"> <i class="glyphicon glyphicon-download"></i>Add</div>	
						</div>			
      								
      				</div>
					<div class="col-xs-12"><br/></div>
					<div class="col-xs-12">	
					<table id="tblListDB" class="table table-bordered table-hover">
	                    <thead>
	                        <tr>
	                            <th>User</th>
	                            <th>DB ID</th>
	                            <th>DB Name</th>
	                            <th>DB Other Name </th>
	                            <th>OE ID</th>
	                            <th>OE Name</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                        
	                    </tbody>     
                    </table>
      				</div>      				
					

				</div>
			</div>			
		</div>
	</div>

</div>
<div class="box-footer">   
      <div onclick="SaveUser();" class="btn btn-flat btn-success pull-right" class="btn btn-success btn-flat pull-right"><i class="glyphicon glyphicon-download"></i>Save !</div>
    </div>
  </form>
<!-- <script src="assets/js/jquery.js"></script>
<script type="text/javascript" src="assets/datatables/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="assets/datatables/js/dataTables.bootstrap.min.js"></script> -->
<script>
$(document).ready(function(){
	var $datatable = $('#tblListDB');
	$datatable.dataTable({
	  'keys': true,
	  'order': [[ 1, 'asc' ]],
	  'columns': [
	              { data: 'User' },
	              { data: 'DB_ID' },
	              { data: 'DB_Name' },
	              { data: 'DB_Name2' },
	              { data: 'OE_ID' },
	              { data: 'OE_Name' },
	            ],
	  'columnDefs': [
            {
                "targets": [ 1,3,4 ],
                "visible": false,
                
            }
	  ]
	});

    var table = $('#tblListDB').DataTable();
 
    $('#tblListDB tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    } );
 
    $('.del_btn').click( function () {
        table.row('.selected').remove().draw( false );
    } );

} );

function userChg(value){
	var str = value.split(",")
	//alert(str[0]);
	$("#user_id").val(str[0]);
	$("#user_name").val(str[1]);
}

function AddtblUser(){
	//alert(tbl);
	var nama = $("#nama").val();
	var db_list = $("#db").val();
	var db = $("#db_id").val();
	var db_name = $("#db_name").val();
	var db_name2 = $("#db2").val();
	var user = $("#user_id").val();
	var user_name = $("#user_name").val();
	var usr = $("#usr").val();
	if(nama == '' || db_list == '' || user == '' || usr == ''){
		alert('Masih ada data yang kosong');
	}else{
	$("#nama").attr("disabled", "disabled");

	var table = $('#tblListDB').DataTable();    
    
	var flg = 0;
	var data = table.rows().data();
		data.each(function (value, index) {
			if (db_name == value.DB_Name && user == value.OE_ID) {
				flg = 1;
			}
		});

	if (flg == 0){
		table.row.add( {
	    	"User"			:   nama,
		    "DB_ID"		:   db,
		    "DB_Name"		:   db_name,
		    "DB_Name2"		:   db_name2,
	    	"OE_ID"	:   user,
		    "OE_Name"	:   user_name,
    	} ).draw();
	}else{
		alert ("Database dan nama sudah ada");
	}
    $("#db").val('');
    $("#user").val(''); 
    $("#list_user").hide();
	}

}

function AddtblUserORI(){
	//alert(tbl);
	var nama = $("#nama").val();
	var db_list = $("#db").val();
	var db = $("#db_id").val();
	var db_name = $("#db_name").val();
	var db_name2 = $("#db2").val();
	var user = $("#user_id").val();
	var user_name = $("#user_name").val();
	var usr = $("#usr").val();
	if(nama == '' || db_list == '' || user == '' || usr == ''){
		alert('Masih ada data yang kosong');
	}else{
	$("#nama").attr("disabled", "disabled");

	var table = $('#tblListDB').DataTable();    
    table.row.add( {
	    "User"			:   nama,
	    "DB_ID"		:   db,
	    "DB_Name"		:   db_name,
	    "DB_Name2"		:   db_name2,
	    "OE_ID"	:   user,
	    "OE_Name"	:   user_name,
    } ).draw();
    $("#db").val('');
    $("#user").val(''); 
    $("#list_user").hide();
	}

}

function chgDB(){
	var db = $("#db").val();
	var str = db.split(",")
	$("#db2").val(str[2]);
	$("#db_choose").val(str[1]);
	$("#list_user").load("serverside/list_user_oe.php?db="+db);
	$("#list_user").show();	
	//alert(db);
}
function SaveUser(){
	var konfirmasi=confirm("Yakin ingin save ? ");
	if (konfirmasi==true)
	{		
		var name = $("#nama").val();
		var ket = $("#ket").val();
		var table = $('#tblListDB').DataTable();    
	    var rows = table.rows().data();
	    var text = '';
	    var nom = 0;
	    for (i = 0; i < rows.length; i++) { 
	    	text += name+','+rows[i].DB_ID+','+rows[i].DB_Name+','+rows[i].OE_ID+','+rows[i].OE_Name+','+rows[i].DB_Name2+'|';
	    } 
	    //alert(text);
		if(text == ''){
			alert('Anda belum memilih data');
		}else{
		      $.ajax({
		        type: "POST",
		        url: "inc/proses_user.php",
		        data: "act=AddUser&name="+name+"&ket="+ket+"&data="+text,
		        cache: false,
		        success: function(msg){
		        console.log(msg);
		        //location.reload();
  	        	 window.location.replace("?role=master&page=master_user_view&bkt="+msg); 		         
		      }});
		}
    }
}


</script>
