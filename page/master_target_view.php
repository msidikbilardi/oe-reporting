<style>
hr {
    display: block;
    height: 1px;
    border: 0;
    border-top: 1px solid #ccc;
    margin: 1em 0;
    padding: 0;
}
</style>

<?php 
$val = $_GET['v'];
$s = $_GET['s'];
$bkt = $_GET['bkt']; 

 
$d = mysqli_query($con,"SELECT *,B.nama as namauser,A.id as idtarget,C.nama as namacat FROM(SELECT * FROM tblmastertarget where iduser = '$bkt' AND status = '1') A LEFT JOIN tblmasteruser B ON A.iduser = B.iduser LEFT JOIN tblmastercategory C ON C.idcat = A.idcat"); 
while($user = mysqli_fetch_assoc($d)) { 
	$usr = $user['iduser']; 
	$nm = $user['namauser']; 
	$data .= $user['idtarget'].','.$user['idcat'].','.$user['namacat'].','.$user['tahun'].','.$user['q1'].','.$user['q2'].','.$user['q3'].','.$user['q4'].'|';
}
//echo $data;
?>
								
<form class="form-horizontal" action="#" method="POST" enctype="multipart/form-data">

<div class="nav-tabs-custom" >
	<!-- Tabs within a box -->
	<ul class="nav nav-tabs pull-right ui-sortable-handle">
		<li class="active" onClick="tab_content('new');"><a href="#revenue-chart" data-toggle="tab">Generate</a></li>
		<li class="pull-left header"><i class="fa fa-inbox"> </i> Master Target <?php echo $nm; ?></li>
	</ul>
	<div class="tab-content no-padding" >
	<!-- Morris chart - Sales -->
		<div class="chart tab-pane active" id="revenue-chart" style="height: 100%; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
			<div class="box">
				<div class="box-body">
					<div class="form-group">
						<label class="control-label col-md-2 col-sm-2 col-xs-12" for="first-name">Nama *</label>
						<div class="col-md-10 col-sm-10 col-xs-12">
							<input type="text" disabled="disabled" id="nm_brg" value='<?php echo $nm; ?>' name="nm_brg" class="form-control col-md-7 col-xs-12 list_sls">
             						<input type="hidden" id="idusr" value='<?php echo $usr; ?>' name="idusr" class="form-control">
					              <input type="hidden" id="namausr" name="namausr" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2 col-sm-2 col-xs-12" for="first-name">Category</label>
						<div class="col-md-10 col-sm-10 col-xs-12">
							<select class="form-control" disabled="disabled" id="cat" name="cat" onChange="chgDB();">
								<option value="">---- Pilih Category ----</option>
								<?php $d = mysqli_query($con,"SELECT * FROM tblmastercategory where status = 1 ORDER BY nama"); ?>
								<?php while($db = mysqli_fetch_assoc($d)) { ?>
									<option value="<?php echo $db['id'].','.$db['idcat'].','.$db['nama']; ?>"><?php echo $db['nama']; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-2 col-sm-2 col-xs-12" for="first-name">Year</label>
						<div class="col-md-10 col-sm-10 col-xs-12">
							<input type="number" id="year" name="year" disabled="disabled" value=<?= date("Y") ?> class="form-control col-md-7 col-xs-12">
             					</div>
					</div>
					<div class="col-sm-6 col-xs-12">
					<div class="form-group">
						<label class="control-label col-md-2 col-sm-2 col-xs-12" for="first-name">Q1 *</label>
						<div class="col-md-10 col-sm-10 col-xs-12">
							<input type="number" id="q1" disabled="disabled" name="q1" class="form-control col-md-7 col-xs-12">
             					</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-2 col-sm-2 col-xs-12" for="first-name">Q2 *</label>
						<div class="col-md-10 col-sm-10 col-xs-12">
							<input type="number" id="q2" disabled="disabled" name="q2" class="form-control col-md-7 col-xs-12">
             					</div>
					</div>
					</div>
					<div class="col-sm-6 col-xs-12">

					<div class="form-group">
						<label class="control-label col-md-2 col-sm-2 col-xs-12" for="first-name">Q3 *</label>
						<div class="col-md-10 col-sm-10 col-xs-12">
							<input type="number" id="q3" disabled="disabled" name="q3" class="form-control col-md-7 col-xs-12">
             					</div>
					</div>
					
					<div class="form-group">
						<label class="control-label col-md-2 col-sm-2 col-xs-12" for="first-name">Q4 *</label>
						<div class="col-md-10 col-sm-10 col-xs-12">
							<input type="number" id="q4"  disabled="disabled" name="q4" class="form-control col-md-7 col-xs-12">
             					</div>
					</div>
					</div>
					<input type="hidden" id="db2" name="db2"><input type="hidden" id="db_choose" name="db_choose">
					<input type="hidden" id="user_id" name="user_id"><input type="hidden" id="user_name" name="user_name">
					<hr />					
					<div class="col-xs-12">	
						<div class="col-xs-6">
						          <div class="btn btn-danger btn-flat disabled " >Hapus Item</div>                             
						</div>			
						<div class="col-xs-6">
							<div class="btn btn-flat btn-success pull-right disabled"> <i class="glyphicon glyphicon-download"></i>Add</div>	
						</div>			
      								
      					</div>
					<div class="col-xs-12"><br/></div>
					<div class="col-xs-12">	
					<table id="tbl_target" class="table table-bordered table-hover">
	                    <thead>
	                        <tr>
	                            <th>idcat</th>
	                            <th>Category</th>
	                            <th>Tahun</th>
	                            <th>Q1</th>
	                            <th>Q2 </th>
	                            <th>Q3</th>
	                            <th>Q4</th>
	                        </tr>
	                    </thead>
	                    <tbody>
				<?php
						$item = explode("|",$data);
						foreach ($item as $value) {

						$dt = explode(",",$value);
						if($dt[0] != ''){ ?>
					<tr>
						<td><?php echo $dt[1];?></td>
						<td><?php echo $dt[2];?></td>
						<td><?php echo $dt[3];?></td>
						<td><?php echo number_format($dt[4]);?></td>
						<td><?php echo number_format($dt[5]);?></td>
						<td><?php echo number_format($dt[6]);?></td>
						<td><?php echo number_format($dt[7]);?></td>
						
					</tr>
					<?php }
					}

 ?>

	                    </tbody>     
                    </table>
      				</div>      				
					

				</div>
			</div>			
		</div>
	</div>

</div>
<div class="box-footer">   
      <div onclick="EditTarget('<?php echo $usr; ?>');" class="btn btn-flat btn-success pull-right" class="btn btn-success btn-flat pull-right"><i class="glyphicon glyphicon-download"></i>Edit !</div>
      <div onclick="BackTarget('<?php echo $usr; ?>');"  class="btn btn-warning btn-flat pull-right"><i class="glyphicon glyphicon-download"></i>Back !</div>
    </div>
  </form>
<script src="assets/js/jquery.js"></script>
<script type="text/javascript" src="assets/datatables/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="assets/datatables/js/dataTables.bootstrap.min.js"></script>
<script>

$(document).ready(function(){


    var table = $('#tbl_target').DataTable();
 
    $('#tbl_target tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    } );
 
    $('.del_btn').click( function () {
        table.row('.selected').remove().draw( false );
    } );

} );

function userChg(value){
	var str = value.split(",")
	//alert(str[0]);
	$("#user_id").val(str[0]);
	$("#user_name").val(str[1]);
}
function BackTarget(id){
	window.location.replace("?role=master&page=master_target_list");  

}
function AddtblUser(){
	//alert(tbl);
	var value = $("#cat").val();
	var cat = value.split(",")
	var tahun = $("#year").val();
	var usr = $("#idusr").val();
	var q1 = $("#q1").val();
	var q2 = $("#q2").val();
	var q3 = $("#q3").val();
	var q4 = $("#q4").val();
	var idcat = '';
	var namacat = '';
	if(usr == '' || q1 == '' || q2 == '' || q3 == '' || q4 == ''){
		alert("Masih ada data yang kosong");
	}else{
		if(cat == ''){ 
			idcat = '';
			namacat = '';
		}else{
			idcat = cat[1];
			namacat = cat[2];
		}
		$("#nm_brg").attr("disabled", "disabled");
		var table = $('#tbl_target').DataTable();    
		table.row.add( {
		    "idcat"			:   idcat,
		    "cat"			:   namacat,
		    "tahun"		:   tahun,
		    "q1"		:   addCommas(q1),
		    "q2"		:   addCommas(q2),
		    "q3"	:   addCommas(q3),
		    "q4"	:   addCommas(q4),
		} ).draw();
	$("#q1").val('');
	$("#q2").val('');
	$("#q3").val('');
	$("#q4").val('');	
	}
}
function EditTarget(id){
	window.location.replace("?role=master&page=master_target_edit&bkt="+id);  

}
function SaveTarget(){
	var usr = $("#idusr").val();
	
	var konfirmasi=confirm("Yakin ingin save ? ");
	if (konfirmasi==true)
	{		

		var table = $('#tbl_target').DataTable();    
	    var rows = table.rows().data();
	    var text = '';
	    var nom = 0;
	    for (i = 0; i < rows.length; i++) { 
	    	text += usr+':'+rows[i].idcat+':'+rows[i].tahun+':'+rows[i].q1+':'+rows[i].q2+':'+rows[i].q3+':'+rows[i].q4+'|';
	    } 
	    //alert(text);
	      $.ajax({
	        type: "POST",
	        url: "inc/proses_target.php",
	        data: "act=AddTarget&usr="+usr+"&data="+text,
	        cache: false,
	        success: function(msg){
	        console.log(msg);
	        location.reload();
	         
	      }});

    }
}


</script>
