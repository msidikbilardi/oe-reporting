<style>
hr {
    display: block;
    height: 1px;
    border: 0;
    border-top: 1px solid #ccc;
    margin: 1em 0;
    padding: 0;
}
</style>

<?php 

$val = $_GET['v'];
$s = $_GET['s'];
$so = $_GET['so']; 
$bkt = $_GET['bkt'];
$d = mysqli_query($con,"SELECT *,B.nama as namacat,C.nama as dbname,B.keterangan as ket FROM (
    SELECT * FROM tblmastercategory_detail where status = 1 AND idcat = '$bkt') A
    LEFT JOIN tblmastercategory B ON A.idcat = B.idcat
    LEFT JOIN tblmasterdatabase C on C.id = A.dbid"); 
while($user = mysqli_fetch_assoc($d)) { 
	$usr = $user['idcat']; 
	$nm = $user['namacat']; 
	$ket = $user['ket']; 
	$data .= $user['idcat'].','.$user['namacat'].','.$user['dbid'].','.$user['dbname'].','.$user['second_name'].','.$user['idoe'].','.$user['catname'].'|';
}
//echo $data;
?>
<form class="form-horizontal" action="#" method="POST" enctype="multipart/form-data">

<div class="nav-tabs-custom" >
	<!-- Tabs within a box -->
	<ul class="nav nav-tabs pull-right ui-sortable-handle">
		<li class="active" onClick="tab_content('new');"><a href="#revenue-chart" data-toggle="tab">Generate</a></li>
		<li class="pull-left header"><i class="fa fa-inbox"></i>Master Category <?php echo $nm; ?></li>
	</ul>
	<div class="tab-content no-padding" >
	<!-- Morris chart - Sales -->
		<div class="chart tab-pane active" id="revenue-chart" style="height: 100%; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
			<div class="box">
				<div class="box-body">

					<div class="form-group">
						<label class="control-label col-md-2 col-sm-2 col-xs-12" for="first-name">Nama *</label>
						<div class="col-md-10 col-sm-10 col-xs-12">
							<input type="text" id="nama" name="nama" disabled="disabled" value="<?php echo $nm; ?>" class="form-control col-md-7 col-xs-12">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2 col-sm-2 col-xs-12" for="first-name">Keterangan</label>
						<div class="col-md-10 col-sm-10 col-xs-12">
							<input type="text" id="ket" name="ket" disabled="disabled" value="<?php echo $ket; ?>" class="form-control col-md-7 col-xs-12">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2 col-sm-2 col-xs-12" for="first-name">Database</label>
						<div class="col-md-10 col-sm-10 col-xs-12">
							<select class="form-control" id="db" name="db" disabled="disabled" onChange="chgDB();">
								<option value="">---- Pilih Database ----</option>
								<?php $d = mysqli_query($con,"SELECT * FROM tblmasterdatabase where status = 1 ORDER BY nama"); ?>
								<?php while($db = mysqli_fetch_assoc($d)) { ?>
									<option value="<?php echo $db['id'].','.$db['nama'].','.$db['second_name']; ?>"><?php echo $db['nama']; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div id="list_user"></div>	
					<input type="hidden" id="db2" name="db2"><input type="hidden" id="db_choose" name="db_choose">
					<input type="hidden" id="user_id" name="user_id"><input type="hidden" id="user_name" name="user_name">
					<hr />					
					<div class="col-xs-12">	
						<div class="col-xs-6"><div class="btn btn-danger btn-flat disabled" >Hapus Item</div></div>			
						<div class="col-xs-6">
							<div  class="btn btn-flat btn-success disabled pull-right"> <i class="glyphicon glyphicon-download"></i>Add</div>	
						</div>			
      								
      				</div><div class="col-xs-12"><br /></div>
					<div class="col-xs-12">	
					<table id="tbl_categ" class="table table-bordered table-hover">
	                    <thead>
	                        <tr>
	                            <th>Name</th>
	                            <th>DB ID</th>
	                            <th>DB Name</th>
	                            <th>DB Name2</th>
	                            <th>OE Cat ID</th>
	                            <th>OE Cat Name</th>
	                        </tr>
	                    </thead>
	                    <tbody><?php
						$item = explode("|",$data);
						foreach ($item as $value) {

						$dt = explode(",",$value);
						if($dt[1] != ''){ ?>
					<tr>
						<td><?php echo $nm;?></td>
						<td><?php echo $dt[2];?></td>
						<td><?php echo $dt[3];?></td>
						<td><?php echo $dt[4];?></td>
						<td><?php echo $dt[5];?></td>
						<td><?php echo $dt[6];?></td>						
					</tr>
					<?php }
					}	 ?>  
	                        
	                    </tbody>     
                    </table>
      				</div>      				
					

				</div>
			</div>			
		</div>
	</div>

</div>
<div class="box-footer">   
      <div onclick="EditCat('<?php echo $usr; ?>');" class="btn btn-flat btn-success pull-right" class="btn btn-success btn-flat pull-right"><i class="glyphicon glyphicon-download"></i>Edit !</div>
      <div onclick="BackTarget('<?php echo $usr; ?>');"  class="btn btn-warning btn-flat pull-right"><i class="glyphicon glyphicon-download"></i>Back !</div>
    </div>
  </form>
<script src="assets/js/jquery.js"></script>
<script type="text/javascript" src="assets/datatables/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="assets/datatables/js/dataTables.bootstrap.min.js"></script>
<script>
function BackTarget(id){
	window.location.replace("?role=master&page=master_category_list");  

}
$(document).ready(function(){

var $datatable = $('#tbl_categ');

$datatable.dataTable({
  'keys': true,
  'order': [[ 1, 'asc' ]],
  'columns': [
              { data: 'nama' },
              { data: 'dbid' },
              { data: 'dbname' },
              { data: 'dbname2' },
              { data: 'catid' },
              { data: 'catname' },
            ],
  'columnDefs': [
    { 
                "targets": [ 1,3,4 ],
                "visible": false,

    }
  ]
});


    var table = $('#tbl_categ').DataTable();
 
    $('#tbl_categ tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    } );
 
    $('.del_btn').click( function () {
        table.row('.selected').remove().draw( false );
    } );

} );


function userChg(value){
	var str = value.split(",")
	//alert(str[0]);
	$("#user_id").val(str[0]);
	$("#user_name").val(str[1]);
}

function AddtblUser(){
	//alert(tbl);
	var nama = $("#nama").val();
	var db_list = $("#db").val();
	var db = $("#db_id").val();
	var db_name = $("#db_name").val();
	var db_name2 = $("#db2").val();
	var user = $("#user_id").val();
	var user_name = $("#user_name").val();
	//var user = $("#user").val();
	var table = $('#tbl_categ').DataTable(); 
	if(nama =='' || db_list == '' || user_name == ''){
		alert('Masih ada data yang kosong');
	}else{   
		$("#nama").attr("disabled", "disabled");
	    table.row.add( {
		    "nama"			:   nama,
		    "dbid"		:   db,
		    "dbname"		:   db_name,
		    "dbname2"		:   db_name2,
		    "catid"		:   user,
		    "catname"	:   user_name,
    	    } ).draw();
	    $("#db").val('');
    		$("#user").val(''); 
  	  $("#list_user").hide();
	}
}
function chgDB(){
	var db = $("#db").val();
	var str = db.split(",")
	$("#db2").val(str[2]);
	$("#db_choose").val(str[1]);
	$("#list_user").load("serverside/list_categ_oe.php?db="+db);
	$("#list_user").show();	
	//alert(db);
}
function EditCat(id){
	window.location.replace("?role=master&page=master_category_edit&bkt="+id); 

}

</script>
