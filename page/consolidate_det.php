<?php
	$s = $_GET['s'];
?>
<form class="form-horizontal" action="#" method="POST" enctype="multipart/form-data">
<div class="nav-tabs-custom" >
	<!-- Tabs within a box -->
	<ul class="nav nav-tabs pull-right ui-sortable-handle">
		<li class="active" onClick="tab_content('new');"><a href="#revenue-chart" data-toggle="tab">Generate</a></li>
		<li class="pull-left header"><i class="fa fa-inbox"></i> <?php echo $s; ?></li>
	</ul>
	<div class="tab-content no-padding" >
	<!-- Morris chart - Sales -->
		<div class="chart tab-pane active" id="revenue-chart" style="height: 100%; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
			<div class="box">
				<div class="box-body">
					<div class="form-group">
						<div class="col-md-6">
							<label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">From date :</label>
							<div class="col-md-8 col-sm-8 col-xs-12">
								<input type="date" id="dtfrom" name="dtfrom" class="form-control col-md-7 col-xs-12">
							</div>
						</div>
						<div class="col-md-6">
							<label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">To date :</label>
							<div class="col-md-8 col-sm-8 col-xs-12">
								<input type="date" id="dtto" name="dtto" class="form-control col-md-7 col-xs-12">
							</div>
						</div>
					</div>
					<?php
					$data = array();

					//DB TIM
					  $conn_string = "host=192.168.1.111 dbname=TOTALINPRO_2016 user=postgres password=postgres";
					  $dbconn = pg_connect($conn_string);

					  $q2 ="SELECT A.*, B.*, A.date_order as dtorder, C.login FROM (
						select * from (
							SELECT a.msb_category, b.user_id, b.date_order, a.name,COALESCE(sum(amount_untaxed),'0') as total, a.id
							FROM res_partner a, sale_order b
							WHERE b.currency_id = '13' AND b.state != 'cancel' AND b.state != 'draft' AND b.state != 'sent' AND b.partner_id = a.id
							GROUP BY a.name,a.id, b.user_id, b.date_order
						) Z
						LEFT JOIN (
							select id as cat_id,name as name_cat,parent_id from res_partner_category
						) Y ON Z.msb_category = Y.cat_id
					) A
					LEFT JOIN (
						SELECT Z.id, user_id,date_order, name as cat,SUM(total) as final, SUM(not_diskon) as not_diskon
						FROM (
							SELECT F.id,A.product_id, b.user_id , B.date_order, sum(A.product_uom_qty*A.sm_price_unit_after_disc3) as total,SUM(A.product_uom_qty*A.price_unit) as not_diskon
							FROM sale_order_line A,sale_order B,res_partner F
							where B.partner_id = F.id AND B.date_order IS NOT NULL AND B.currency_id = '13' AND b.state != 'cancel' AND b.state != 'draft' AND b.state != 'sent' AND A.order_id = B.id
							group by A.product_id,F.id, b.user_id, B.date_order
						) Z
						LEFT JOIN (
							SELECT A.id,C.name,A.default_code,A.name_template,A.product_tmpl_id
							FROM product_product A,product_template B,product_category C
							WHERE B.categ_id = C.id AND A.product_tmpl_id = B.id
						) Y ON Z.product_id = Y.id
						GROUP BY name,Z.id, user_id, date_order
					) B ON A.id = B.id AND A.user_id = B.user_id AND a.date_order = b.date_order
					INNER JOIN res_users C ON A.user_id = C.id";

					  $d = pg_query($dbconn,$q2);
						$login = [];
						$custname = [];
						$cat = [];

					  while($dt = pg_fetch_assoc($d)){
					    $rows = array();
					    $rows[] = $dt['login'];
							$rows[] = $dt['dtorder'];
					    $rows[] = $dt['name'];
					    $rows[] = $dt['cat'];
					    $rows[] = number_format($dt['not_diskon']);
					    $data[] = $rows;
							//login
							$flag = 0;
							for($i=0;$i<=count($login);$i++){
								if($login[$i] == $dt['login']){
									$flag = 1;
								}
							}
							if ($flag == 0){
								array_push($login,$dt['login']);
							}
							//custname
							$flag = 0;
							for($i=0;$i<=count($custname);$i++){
								if($custname[$i] == $dt['name']){
									$flag = 1;
								}
							}
							if ($flag == 0){
								array_push($custname,$dt['name']);
							}//login
							$flag = 0;
							for($i=0;$i<=count($cat);$i++){
								if($cat[$i] == $dt['cat']){
									$flag = 1;
								}
							}
							if ($flag == 0){
								array_push($cat,$dt['cat']);
							}
						}
					//

					//proses datatables
					    $json_data = array
					     (
					       "data"            => $data
					     );
					     $ajaxData = json_encode($data);
					?>

					<div class="form-group">
						<div class="col-md-4">
							<label class="control-label col-md-5 col-sm-5 col-xs-12" for="first-name">Sales Name :</label>
							<!-- <input type="checkbox" class="filter" value="IDPAC" />IDPAC
							<input type="checkbox" class="filter" value="IDSAF" />IDSAF
							<input type="checkbox" class="filter" value="IDHMI" />IDHMI -->
							<div class="col-md-7 col-sm-7 col-xs-12">
								<select name="slsOpt[]" multiple id="slsOpt">
									<?php
										sort($login);
										for($i=0;$i<count($login);$i++){
											echo '<option value="'.$login[$i].'">'.$login[$i].'</option>';
										}
									?>
								</select>
							</div>
						</div>
						<div class="col-md-4">
							<label class="control-label col-md-5 col-sm-5 col-xs-12" for="first-name">Customer Name :</label>
							<div class="col-md-7 col-sm-7 col-xs-12">
								<select name="custOpt[]" multiple id="custOpt">
									<?php
										sort($custname);
										for($i=0;$i<count($custname);$i++){
											echo '<option value="'.$custname[$i].'">'.$custname[$i].'</option>';
										}
									?>
								</select>
							</div>
						</div>
						<div class="col-md-4">
							<label class="control-label col-md-5 col-sm-5 col-xs-12" for="first-name">Product Category :</label>
							<div class="col-md-7 col-sm-7 col-xs-12">
								<select name="catOpt[]" multiple id="catOpt">
									<?php
										sort($cat);
										for($i=0;$i<count($cat);$i++){
											echo '<option value="'.$cat[$i].'">'.$cat[$i].'</option>';
										}
									?>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<table class="table_id display responsive no-wrap" width="100%">
            <thead>
                <tr>
            			<th>Sales Name</th>
            			<th>Order Date</th>
            			<th>Customer Name</th>
            			<th>Product Group</th>
                  <th>Value per product</th>
                </tr>
								<tr>
            			<th class="inptxt">Sales Name</th>
            			<th class="inptxt">Order Date</th>
            			<th class="inptxt">Customer Name</th>
            			<th class="inptxt">Product Group</th>
                  <th class="inptxt">Value per product</th>
                </tr>
            </thead>
						<tfoot>
      				<tr>
      					<th colspan="4" style="text-align:right">Total:</th>
      					<th></th>
      				</tr>
      			</tfoot>
        	</table>
				</div>
			</div>
		</div>
	</div>
</div>
</form>
<script src="assets/js/jquery.js"></script>
<script type="text/javascript" src="assets/datatables/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="assets/datatables/js/dataTables.bootstrap.min.js"></script>

<script src="assets/js/jquery-multiselect.js"></script>
<script type="text/javascript">
var optSls
var optCust
var optCat

function convertToRupiah(angka){
    var rupiah = '';
    var angkarev = angka.toString().split('').reverse().join('');
    for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
    return rupiah.split('',rupiah.length-1).reverse().join('');
}
    $(document).ready( function ()
    {
			var table = $('.table_id').DataTable(
					{
						// "sAjaxSource":'assets/ajax/list_cons_so_det.php' ,
						"data": <?php print $ajaxData; ?> ,
						"deferRender": true,
						"deferLoading": 5,
						orderCellsTop: true,
						responsive: false,
						"order": [[ 1, "asc" ]],
						"scrollX": true,
						"iDisplayLength": 10,
						"lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100,"All"]],
						"oLanguage": {
							"oPaginate": {
							"sPrevious": "<<",
							"sNext": ">>",
							}
						},
						"footerCallback": function ( row, data, start, end, display ) {
							var api = this.api(), data;

							// Remove the formatting to get integer data for summation
							var intVal = function ( i ) {
									return typeof i === 'string' ?
											i.replace(/[\$,]/g, '')*1 :
											typeof i === 'number' ?
													i : 0;
							};

							// Total over all pages
							total = api
									.column( 4 )
									.data()
									.reduce( function (a, b) {
											return intVal(a) + intVal(b);
									}, 0 );

							// Total over this page

							pageTotal = api
									.column( 4, { page: 'current'} )
									.data()
									.reduce( function (a, b) {
											var skrg = intVal(a) + intVal(b);
											return skrg;
									}, 0 );

							// Update footer
							total = convertToRupiah(total);
							var nom = convertToRupiah(pageTotal);
							$( api.column( 4 ).footer() ).html(
									''+nom+' ( '+ total +' total)'
							);
						},
						// "initComplete": function(settings, json) {
						// 		table
						// 		.column(0)
				    //     .sort()     //***How do we get this to sort according to columnDefs?***
				    //     .data()
				    //     .unique()
				    //     .each( function ( d ) {
				    //         $('#langOpt').append(('<option value="'+d+'">'+d+'</option>'));
						// 				$('.dropdown-menu').append('<li class=""><a tabindex="0"><label class="checkbox" title="'+d+'"><input value="'+d+'" type="checkbox"> '+d+'</label></a></li>');
				    //     } );
					  // }
					});

			$('.inptxt').each( function (i) {
				var title = $(this).text();
				if (title != ''){
					$(this).html( '<input type="text" size="12" placeholder="Search '+title+'" />' );
					}
						$( 'input', this ).on( 'keyup change', function () {
							if ( table.column(i).search() !== this.value ) {
								table
									.column(i)
									.search( this.value )
									.draw();
							}
						} );
				} );

			$('#dtfrom, #dtto').change( function() {
	        table.draw();
	    } );

	});

	$.fn.dataTable.ext.search.push(
    function( settings, data, dataIndex ) {
        var min = new Date($('#dtfrom').val());
        var max = new Date($('#dtto').val());
        var age = new Date(data[1]);

        if ( ( isNaN( min ) && isNaN( max ) ) ||
             ( isNaN( min ) && age <= max ) ||
             ( min <= age   && isNaN( max ) ) ||
             ( min <= age   && age <= max ) )
        {
            return true;
        }
        return false;
    }
	);

	$('#slsOpt').multiselect({
		enableCaseInsensitiveFiltering: true,
		includeSelectAllOption: true,
    enableClickableOptGroups: true,
    onChange: function(element, checked) {
        var brands = $('#slsOpt option:selected');
        var selected = [];
        $(brands).each(function(index, brand){
            selected.push([$(this).val()]);
        });

        optSls = selected.join("|");
        $('.table_id').DataTable().column(0).search(optSls,true, false).draw();
    },
    onSelectAll: function(element, checked) {
        var brands = $('#slsOpt option:selected');
        var selected = [];
        $(brands).each(function(index, brand){
            selected.push([$(this).val()]);
        });

        optSls = selected.join("|");
        $('.table_id').DataTable().column(0).search(optSls,true, false).draw();
    },
	});

	$('#custOpt').multiselect({
		enableCaseInsensitiveFiltering: true,
		includeSelectAllOption: true,
    enableClickableOptGroups: true,
    onChange: function(element, checked) {
        var brands = $('#custOpt option:selected');
        var selected = [];
        $(brands).each(function(index, brand){
            selected.push([$(this).val()]);
        });

        optCust = selected.join("|");
        $('.table_id').DataTable().column(2).search(optCust,true, false).draw();
    },
    onSelectAll: function(element, checked) {
        var brands = $('#custOpt option:selected');
        var selected = [];
        $(brands).each(function(index, brand){
            selected.push([$(this).val()]);
        });

        optCust = selected.join("|");
        $('.table_id').DataTable().column(2).search(optCust,true, false).draw();
    }
	});

	$('#catOpt').multiselect({
		enableCaseInsensitiveFiltering: true,
		includeSelectAllOption: true,
    enableClickableOptGroups: true,
    onChange: function(element, checked) {
        var brands = $('#catOpt option:selected');
        var selected = [];
        $(brands).each(function(index, brand){
            selected.push([$(this).val()]);
        });

        optCat = selected.join("|");
        $('.table_id').DataTable().column(3).search(optCat,true, false).draw();
    },
    onSelectAll: function(element, checked) {
        var brands = $('#catOpt option:selected');
        var selected = [];
        $(brands).each(function(index, brand){
            selected.push([$(this).val()]);
        });

        optCat = selected.join("|");
        $('.table_id').DataTable().column(3).search(optCat,true, false).draw();
    }
	});
</script>
