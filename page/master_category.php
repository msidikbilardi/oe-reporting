<style>
hr {
    display: block;
    height: 1px;
    border: 0;
    border-top: 1px solid #ccc;
    margin: 1em 0;
    padding: 0;
}
</style>

<?php 

$val = $_GET['v'];
$s = $_GET['s'];
$so = $_GET['so']; 
?>
<form class="form-horizontal" action="#" method="POST" enctype="multipart/form-data">

<div class="nav-tabs-custom" >
	<!-- Tabs within a box -->
	<ul class="nav nav-tabs pull-right ui-sortable-handle">
		<li class="active" onClick="tab_content('new');"><a href="#revenue-chart" data-toggle="tab">Generate</a></li>
		<li class="pull-left header"><i class="fa fa-inbox"></i> <?php echo $s; ?></li>
	</ul>
	<div class="tab-content no-padding" >
	<!-- Morris chart - Sales -->
		<div class="chart tab-pane active" id="revenue-chart" style="height: 100%; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
			<div class="box">
				<div class="box-body">

					<div class="form-group">
						<label class="control-label col-md-2 col-sm-2 col-xs-12" for="first-name">Nama *</label>
						<div class="col-md-10 col-sm-10 col-xs-12">
							<input type="text" id="nama" name="nama" class="form-control col-md-7 col-xs-12">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2 col-sm-2 col-xs-12" for="first-name">Keterangan</label>
						<div class="col-md-10 col-sm-10 col-xs-12">
							<input type="text" id="ket" name="ket" class="form-control col-md-7 col-xs-12">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2 col-sm-2 col-xs-12" for="first-name">Database</label>
						<div class="col-md-10 col-sm-10 col-xs-12">
							<select class="form-control" id="db" name="db" onChange="chgDB();">
								<option value="">---- Pilih Database ----</option>
								<?php $d = mysqli_query($con,"SELECT * FROM tblmasterdatabase where status = 1 ORDER BY nama"); ?>
								<?php while($db = mysqli_fetch_assoc($d)) { ?>
									<option value="<?php echo $db['id'].','.$db['nama'].','.$db['second_name']; ?>"><?php echo $db['nama']; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div id="list_user"></div>	
					<input type="hidden" id="db2" name="db2"><input type="hidden" id="db_choose" name="db_choose">
					<input type="hidden" id="user_id" name="user_id"><input type="hidden" id="user_name" name="user_name">
					<hr />					
					<div class="col-xs-12">	
						<div class="col-xs-6"><div class="btn btn-danger btn-flat del_btn" id="del_btn">Hapus Item</div></div>			
						<div class="col-xs-6">
							<div onclick="AddtblUser();" class="btn btn-flat btn-success pull-right"> <i class="glyphicon glyphicon-download"></i>Add</div>	
						</div>			
      								
      				</div><div class="col-xs-12"><br /></div>
					<div class="col-xs-12">	
					<table id="tbl_categ" class="table table-bordered table-hover">
	                    <thead>
	                        <tr>
	                            <th>Name</th>
	                            <th>DB ID</th>
	                            <th>DB Name</th>
	                            <th>DB Name2</th>
	                            <th>OE Cat ID</th>
	                            <th>OE Cat Name</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                        
	                    </tbody>     
                    </table>
      				</div>      				
					

				</div>
			</div>			
		</div>
	</div>

</div>
<div class="box-footer">   
      <div onclick="SaveUser();" class="btn btn-flat btn-success pull-right" class="btn btn-success btn-flat pull-right"><i class="glyphicon glyphicon-download"></i>Save !</div>
    </div>
  </form>
<script src="assets/js/jquery.js"></script>
<script type="text/javascript" src="assets/datatables/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="assets/datatables/js/dataTables.bootstrap.min.js"></script>
<script>

$(document).ready(function(){

var $datatable = $('#tbl_categ');

$datatable.dataTable({
  'keys': true,
  'order': [[ 1, 'asc' ]],
  'columns': [
              { data: 'nama' },
              { data: 'dbid' },
              { data: 'dbname' },
              { data: 'dbname2' },
              { data: 'catid' },
              { data: 'catname' },
            ],
  'columnDefs': [
    { 
                "targets": [ 1,3,4 ],
                "visible": false,

    }
  ]
});


    var table = $('#tbl_categ').DataTable();
 
    $('#tbl_categ tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    } );
 
    $('.del_btn').click( function () {
        table.row('.selected').remove().draw( false );
    } );

} );


function userChg(value){
	var str = value.split(",")
	//alert(str[0]);
	$("#user_id").val(str[0]);
	$("#user_name").val(str[1]);
}

function AddtblUser(){
	//alert(tbl);
	var nama = $("#nama").val();
	var db_list = $("#db").val();
	var db = $("#db_id").val();
	var db_name = $("#db_name").val();
	var db_name2 = $("#db2").val();
	var user = $("#user_id").val();
	var user_name = $("#user_name").val();
	var usr = $("#usr").val();
	var table = $('#tbl_categ').DataTable(); 

	if(nama =='' || db_list == '' || user_name == '' || user == ''){
		alert('Masih ada data yang kosong');
	}else{   
		$("#nama").attr("disabled", "disabled");
	    
		var flg = 0;
		var data = table.rows().data();
		data.each(function (value, index) {

			if (db == value.dbid && user == value.catid) {
				flg = 1;
			}
		});

		if (flg == 0){
			table.row.add( {
			    "nama"			:   nama,
			    "dbid"		:   db,
			    "dbname"		:   db_name,
			    "dbname2"		:   db_name2,
			    "catid"		:   user,
			    "catname"	:   user_name,
			} ).draw();
		}else{
			alert ("Database dan category sudah ada");
		}
	    $("#db").val('');
    		$("#user").val(''); 
  	  $("#list_user").hide();
	}
}

function chgDB(){
	var db = $("#db").val();
	var str = db.split(",")
	$("#db2").val(str[2]);
	$("#db_choose").val(str[1]);
	$("#list_user").load("serverside/list_categ_oe.php?db="+db);
	$("#list_user").show();	
	//alert(db);
}
function SaveUser(){
	var konfirmasi=confirm("Yakin ingin save ? ");
	if (konfirmasi==true)
	{		
		var name = $("#nama").val();
		var ket = $("#ket").val();
		var table = $('#tbl_categ').DataTable();    
	    var rows = table.rows().data();
	    var text = '';
	    var nom = 0;
	    for (i = 0; i < rows.length; i++) { 
	    	text += name+','+rows[i].dbid+','+rows[i].dbname+','+rows[i].catid+','+rows[i].catname+','+rows[i].dbname2+'|';
	    } 
		if(text == ''){
			alert('Anda belum memilih data');
		}else{
	   // alert(text);
		      $.ajax({
		        type: "POST",
		        url: "inc/proses_category.php",
		        data: "act=AddCateg&name="+name+"&ket="+ket+"&data="+text,
		        cache: false,
		        success: function(msg){
		        console.log(msg);
		        //location.reload();
	 		 window.location.replace("?role=master&page=master_category_view&bkt="+msg); 
		      }});
		}
		
    }
}


</script>
