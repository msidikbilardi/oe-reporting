<?php

?>
<div class="nav-tabs-custom" >
                <!-- Tabs within a box -->
<ul class="nav nav-tabs pull-right ui-sortable-handle">
  <li class="active" onClick="tab_content('new');"><a href="#revenue-chart" data-toggle="tab">Reporting</a></li>
 
  <li class="pull-left header"><i class="fa fa-inbox"></i> Generate Reporting</li>
</ul>
	<div class="tab-content no-padding" >
	<!-- Morris chart - Sales -->
		<div class="chart tab-pane active" id="revenue-chart" style="height: 100%; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
			<div class="box">
			    <div class="box-header">
			      <h3 class="box-title">Form Report</h3>
			    </div><!-- /.box-header -->
			    <div class="box-body">
			    	<form class="form-horizontal" action="page/preview_file.php" method="POST" enctype="multipart/form-data">
					    <div class="form-group">
					      <label for="inputEmail3" class="col-sm-2 control-label">File</label>
					      <div class="col-sm-10">
					        <input type="file" class="form-control" id="date_start" required name="date_start" >
					      </div>
					    </div>	

				    				    				    	
					<div class="box-footer">    
					    <button type="submit" class="btn btn-success btn-flat pull-right"><i class="glyphicon glyphicon-download"></i> Go!</button>
					</div><!-- /.box-footer -->					    
				</form>
			  	</div>
	
			 </div>			
		</div>
	</div>
</div>

<script>
function preview(){
	var file_import = $('#file_import').val(); 
	alert(file_import);
	$("#preview_file").load("page/preview_file.php");
}
</script>