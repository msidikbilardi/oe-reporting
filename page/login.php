<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Willert Ticket - Manage</title>
    <link href="assets/img/willert/favicon.png" rel="shortcut icon">
    <link href="assets/css/vinzlee.css" rel="stylesheet">
    <link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
</head>
<body class="htmllogin">
      <?php 
      if(isset($_GET['msg'])){
      if($_GET['msg'] == '1'){ ?>
        <div class="callout callout-warning">
          <p><?php if(isset($_SESSION['error_msg'])){ echo $_SESSION['error_msg']; }?></p>
        </div>
      <?php }else if($_GET['msg'] == '2'){ ?> 
        <div class="callout callout-danger">
          <p><?php echo $_SESSION['error_msg'];?></p>
        </div>
      <?php }
      } ?>
 
    <div class="logologin"></div>
    <form action="?action=doLogin" method="POST">
        <div align="center">
            <div class="input-group inputglogin">
                <span class="input-group-addon" id="basic-addon1"><span class="fa fa-user"></span></span>
                <input type="text" class="form-control inputlogin"  placeholder="Username" name="user" aria-describedby="basic-addon1">
            </div>
            <div class="input-group inputglogin">
                <span class="input-group-addon" id="basic-addon1"><span class="fa fa-lock"></span></span>
                <input type="password" class="form-control inputlogin" placeholder="Password" name="pass" aria-describedby="basic-addon1">
            </div>
            <button type="submit" class="btn btn-primary btnlogin">Login</button>
        </div>
    </form>
    <p class="credit">
        <a target="_blank" href="https://www.willertindo.com">Willertindo Innovation Solution</a>
        - your technology solution
    </p>
    <script src="assets/js/jquery.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>