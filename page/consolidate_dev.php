<?php 

$val = $_GET['v'];
$s = $_GET['s'];
$so = $_GET['so']; 

if ($val == "category_cons_summary_filter"){
?>
	<form class="form-horizontal" action="inc/excel_so_category_cons_summary.php" method="POST" enctype="multipart/form-data">
<?php
}else{
?>
	<form class="form-horizontal" action="inc/excel_so_category_cons_sales.php" method="POST" enctype="multipart/form-data">
<?php
}
?>
<div class="nav-tabs-custom" >
	<!-- Tabs within a box -->
	<ul class="nav nav-tabs pull-right ui-sortable-handle">
		<li class="active" onClick="tab_content('new');"><a href="#revenue-chart" data-toggle="tab">Generate</a></li>
		<li class="pull-left header"><i class="fa fa-inbox"></i> <?php echo $s; ?></li>
	</ul>
	<div class="tab-content no-padding" >
	<!-- Morris chart - Sales -->
		<div class="chart tab-pane active" id="revenue-chart" style="height: 100%; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
			<div class="box">
				<div class="box-body">
						<div class="form-group">
							<label class="control-label col-md-2 col-sm-2 col-xs-12" for="first-name">Nama</label>
							<div class="col-md-10 col-sm-10 col-xs-12">
								<input type="text" id="nm_brg" name="nm_brg" class="form-control col-md-7 col-xs-12 list_sls">
        	      						<input type="hidden" id="idusr" name="idusr" class="form-control">
						                <input type="hidden" id="namausr" name="namausr" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label">Periode </label>
							<div class="col-sm-5">
								<input type="number" id="tahun" name="tahun" class="form-control " value=<?= date("Y") ?>>
							</div>
							<div class="col-sm-5">	
								<select class="form-control" id="periode" name="periode">
									<option value="">---- Pilih Periode ----</option>
									<option value="Q1">Quarter 1</option>
									<option value="Q2">Quarter 2</option>
									<option value="Q3">Quarter 3</option>
									<option value="Q4">Quarter 4</option>
								</select>
							</div>
						</div>

				</div>
			</div>			
		</div>
	</div>
</div>
<div class="box-footer">   
	  <div onclick="print_test();" class="btn btn-flat btn-success pull-right" class="btn btn-success btn-flat pull-right"><i class="glyphicon glyphicon-download"></i>Print !</div>
      <button type="submit" class="btn btn-success btn-flat pull-right"><i class="glyphicon glyphicon-download"></i>EXCEL !</button> 
      <div onclick="viewtable();" class="btn btn-flat btn-success pull-right" class="btn btn-success btn-flat pull-right"><i class="glyphicon glyphicon-download"></i>Preview !</div>
    </div>
  </form>
<div id="viewtable"></div>
<script src="assets/js/jquery.js"></script>
<script type="text/javascript" src="assets/datatables/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="assets/datatables/js/dataTables.bootstrap.min.js"></script>
<script>

function print_test(){
  var state = $('#state').val();
  var db = $('#db').val(); 
  var usr = $('#idusr').val(); 
  var tahun = $('#tahun').val(); 
  var periode = $('#periode').val();
  var type = $('#type').val();   
  var type_report = $('#type_report').val();   
  //var parent = $('#parent').val();  
  if (usr == ''){
    alert("Nama belum diisi");    
  }else if (tahun == '' || periode == ''){ 
    alert("Periode belum diisi");
  }else{   
<?php
if ($val == "category_cons_summary_filter"){
?>
	var url="report/category_cons_summary_report.php?usr="+usr+"&thn="+tahun+"&per="+periode;
<?php
}else{
?>
	var url="report/category_cons_sales_report.php?usr="+usr+"&thn="+tahun+"&per="+periode;
<?php
}
?>
      var win = window.open(url, '_blank');
      win.focus();    
  } 

}

function print_email(){
  var state = $('#state').val();
  var db = $('#db').val(); 
  var usr = $('#idusr').val(); 
  var tahun = $('#tahun').val(); 
  var periode = $('#periode').val();
  var type = $('#type').val();   
  var type_report = $('#type_report').val();   
  //var parent = $('#parent').val();  
  if (usr == ''){
    alert("Nama belum diisi");    
  }else if (tahun == '' || periode == ''){ 
    alert("Periode belum diisi");
  }else{   
<?php
if ($val == "category_cons_summary_filter"){
?>
	var url="report/category_cons_email_sls.php?usr="+usr+"&thn="+tahun+"&per="+periode;
<?php
}else{
?>
	var url="report/category_cons_email_sls.php?usr="+usr+"&thn="+tahun+"&per="+periode;
<?php
}
?>
      var win = window.open(url, '_blank');
      win.focus();    
  } 

}

function viewtable(){
	var state = $('#state').val();
  	var db = $('#db').val(); 
	var usr = $('#idusr').val(); 
  	var tahun = $('#tahun').val(); 
  	var periode = $('#periode').val();
  	var type = $('#type').val();   
  	var type_report = $('#type_report').val();

	if (tahun == '' || periode == ''){ 
    	alert("Periode belum diisi");
	}else{	
		$("#viewtable").load("serverside/viewtable_cons_summary.php?usr="+usr+"&thn="+tahun+"&per="+periode);
	}
}

</script>
