<?php
session_start();

//drop and create table
	date_default_timezone_set('Asia/Jakarta');
	$localhostdb = '192.168.1.70';
	$usernamedb  = 'root';
	$passworddb  = 'Qwert!123';
	$databasedb  = 'oereporting';

	$con = mysqli_connect($localhostdb,$usernamedb,$passworddb,$databasedb)or die("cannot connect");

	$query = "DROP TABLE IF EXISTS `invcons".$_SERVER['REMOTE_ADDR']."`";
	$submit = mysqli_query($con,$query);

	$query = "CREATE TABLE `invcons".$_SERVER['REMOTE_ADDR']."` (`db` VARCHAR(50) NOT NULL, `intnum` VARCHAR(50) NOT NULL, `name` VARCHAR(50) NOT NULL, `category` VARCHAR(50) NOT NULL, `price` DOUBLE NOT NULL, `stock` DOUBLE NOT NULL, `in` DOUBLE NOT NULL, `out` DOUBLE NOT NULL, `incoming` DOUBLE NOT NULL, `outgoing` DOUBLE NOT NULL, `forecast` DOUBLE NOT NULL) ENGINE=MyISAM";

	$submit = mysqli_query($con,$query);
//end drop and create table

$cost = $_GET['cost'];
$nullfield = $_GET['nullfield'];
$start = $_GET['start'];
$end = $_GET['end'];
$best = $_GET['best'];
//echo $start.'|'.$end.'|'.$best;
 ?>
<div id="" style="overflow-x: scroll; width:100%;">
<table id="example5" class=" display responsive no-wrap" width="100%">
    <thead>
        <th>Internal Number</th>
        <th>Name</th>
        <th class="hidden-xs">Category</th>
        <?php if($best == 'on'){ ?>
            <th class="hidden-xs">TOTAL IN</th>
            <th class="hidden-xs">TOTAL OUT</th>
        <?php }else{ ?>
		<?php if($cost == 'on'){ ?>
            <th class="hidden-xs">Cost Price</th>
		<?php } ?>
        <?php } ?>
        <th>Stock</th>
        <th class="">IN</th>
        <th class="">OUT</th>
        <!--th>Forcasted</th-->
    </thead>
    <tbody>
        <?php
        $filter = "AND A.default_code LIKE '%%' ";
        if($best == 'on'){
            $date = "AND date >= '$start' AND date <= '$end' ";
        }else{
            $date = '';
        }
        //TIM
        $conn_string = "host=192.168.1.111 dbname=TOTALINPRO_2016 user=postgres password=postgres";
        $dbconn = pg_connect($conn_string);

        $num = 0;
        $loc_id_IN = '';
        $loc_dest_id_IN = '';

        $loc_id_OUT = '';
        $loc_dest_id_OUT = '';

        $i = pg_query($dbconn,"SELECT * FROM stock_warehouse");
        while($inv = pg_fetch_array($i)){
            if($num == 0){
                $loc_id_IN .= "A.location_id != '".$inv['lot_input_id']."' ";
                $loc_dest_id_IN .= "A.location_dest_id = '".$inv['lot_input_id']."'  ";

                $loc_id_OUT .= "A.location_id = '".$inv['lot_input_id']."' ";
                $loc_dest_id_OUT .= "A.location_dest_id != '".$inv['lot_input_id']."'  ";
            }else{
                $loc_id_IN .= "OR A.location_id != '".$inv['lot_input_id']."' ";
                $loc_dest_id_IN .= "OR A.location_dest_id = '".$inv['lot_input_id']."'  ";

                $loc_id_OUT .= "OR A.location_id = '".$inv['lot_input_id']."' ";
                $loc_dest_id_OUT .= "OR A.location_dest_id != '".$inv['lot_input_id']."'  ";
            }

            $num++;
        }

        $q = "select 'TIM' as db, * from (select C.parent_id,C.name as cat_name,A.default_code,A.name_template as name_prd,A.id,B.standard_price from product_product A, product_template B, product_category C where C.id = B.categ_id AND A.product_tmpl_id = B.id  $filter ORDER BY A.default_code) Z
        LEFT JOIN ( select sum(A.product_qty) as masuk, A.product_id from stock_move A, product_product B where ($loc_id_IN) and ($loc_dest_id_IN)  AND A.product_id = B.id and A.state = 'done' $date group by A.product_id,B.name_template)
        Y ON Z.id = Y.product_id
        LEFT JOIN ( select B.name_template,sum(A.product_qty) as keluar, A.product_id from stock_move A, product_product B where ($loc_id_OUT) and ($loc_dest_id_OUT)  AND A.product_id = B.id and A.state = 'done' $date group by A.product_id,B.name_template)
        X ON X.product_id = Z.id
        LEFT JOIN ( select B.name_template,sum(A.product_qty) as outgoing, A.product_id from stock_move A, product_product B where ($loc_id_OUT) and ($loc_dest_id_OUT)  AND A.product_id = B.id and A.state IN ('confirmed','waiting','assigned') $date group by A.product_id,B.name_template)
        W ON W.product_id = Z.id
        LEFT JOIN ( select sum(A.product_qty) as incoming, A.product_id from stock_move A, product_product B where ($loc_id_IN) and ($loc_dest_id_IN)  AND A.product_id = B.id and A.state IN ('confirmed','waiting','assigned') $date group by A.product_id,B.name_template)
        V ON Z.id = V.product_id";
        $rs =  pg_query($dbconn,$q);
        $values = "";

  			while($dt = pg_fetch_assoc($rs)){
          $stock = 0;
          $forcasted = 0;

  				$db = $dt['db'];
  				$intnum = mysqli_real_escape_string($con,stripslashes($dt['default_code']));
          $nm = mysqli_real_escape_string($con,stripslashes($dt['name_prd']));
          $cat = mysqli_real_escape_string($con,stripslashes($dt['cat_name']));
          $price = $dt['standard_price'];

          if($dt['masuk'] != ""){
            $masuk = $dt['masuk'];
          }else{
            $masuk = 0;
          }
          if($dt['keluar'] != ""){
            $keluar = $dt['keluar'];
          }else{
            $keluar = 0;
          }
          if($dt['incoming'] != ""){
            $incoming = $dt['incoming'];
          }else{
            $incoming = 0;
          }
          if($dt['outgoing'] != ""){
            $outgoing = $dt['outgoing'];
          }else{
            $outgoing = 0;
          }

          $stock = $masuk - $keluar;
          $forecasted = $stock + $incoming - $outgoing;
          $in = $masuk;
          $out = $keluar;
          $incoming = $incoming;
          $outgoing = $outgoing;

  				$values = $values . "('$db', '$intnum', '$nm', '$cat', $price, $stock, $in, $out, $incoming, $outgoing, $forecasted),";
  			}
  			$values = substr($values,0,-1) . ";";
  			$query = "INSERT INTO `invcons".$_SERVER['REMOTE_ADDR']."`(`db`, `intnum`, `name`, `category`, `price`, `stock`, `in`, `out`, `incoming`, `outgoing`, `forecast`) VALUES " . $values;
  			$submit = mysqli_query($con,$query);

        //WIS
        $conn_string = "host=192.168.1.111 dbname=WILLERTINDO_2016 user=postgres password=postgres";
        $dbconn = pg_connect($conn_string);

        $num = 0;
        $loc_id_IN = '';
        $loc_dest_id_IN = '';

        $loc_id_OUT = '';
        $loc_dest_id_OUT = '';

        $i = pg_query($dbconn,"SELECT * FROM stock_warehouse");
        while($inv = pg_fetch_array($i)){
            if($num == 0){
                $loc_id_IN .= "A.location_id != '".$inv['lot_input_id']."' ";
                $loc_dest_id_IN .= "A.location_dest_id = '".$inv['lot_input_id']."'  ";

                $loc_id_OUT .= "A.location_id = '".$inv['lot_input_id']."' ";
                $loc_dest_id_OUT .= "A.location_dest_id != '".$inv['lot_input_id']."'  ";
            }else{
                $loc_id_IN .= "OR A.location_id != '".$inv['lot_input_id']."' ";
                $loc_dest_id_IN .= "OR A.location_dest_id = '".$inv['lot_input_id']."'  ";

                $loc_id_OUT .= "OR A.location_id = '".$inv['lot_input_id']."' ";
                $loc_dest_id_OUT .= "OR A.location_dest_id != '".$inv['lot_input_id']."'  ";
            }

            $num++;
        }

        $q = "select 'WIS' as db, * from (select C.parent_id,C.name as cat_name,A.default_code,A.name_template as name_prd,A.id,B.standard_price from product_product A, product_template B, product_category C where C.id = B.categ_id AND A.product_tmpl_id = B.id  $filter ORDER BY A.default_code) Z
        LEFT JOIN ( select sum(A.product_qty) as masuk, A.product_id from stock_move A, product_product B where ($loc_id_IN) and ($loc_dest_id_IN)  AND A.product_id = B.id and A.state = 'done' $date group by A.product_id,B.name_template)
        Y ON Z.id = Y.product_id
        LEFT JOIN ( select B.name_template,sum(A.product_qty) as keluar, A.product_id from stock_move A, product_product B where ($loc_id_OUT) and ($loc_dest_id_OUT)  AND A.product_id = B.id and A.state = 'done' $date group by A.product_id,B.name_template)
        X ON X.product_id = Z.id
        LEFT JOIN ( select B.name_template,sum(A.product_qty) as outgoing, A.product_id from stock_move A, product_product B where ($loc_id_OUT) and ($loc_dest_id_OUT)  AND A.product_id = B.id and A.state IN ('confirmed','waiting','assigned') $date group by A.product_id,B.name_template)
        W ON W.product_id = Z.id
        LEFT JOIN ( select sum(A.product_qty) as incoming, A.product_id from stock_move A, product_product B where ($loc_id_IN) and ($loc_dest_id_IN)  AND A.product_id = B.id and A.state IN ('confirmed','waiting','assigned') $date group by A.product_id,B.name_template)
        V ON Z.id = V.product_id";
        $rs =  pg_query($dbconn,$q);
        $values = "";

  			while($dt = pg_fetch_assoc($rs)){
          $stock = 0;
          $forcasted = 0;

  				$db = $dt['db'];
  				$intnum = mysqli_real_escape_string($con,stripslashes($dt['default_code']));
          $nm = mysqli_real_escape_string($con,stripslashes($dt['name_prd']));
          $cat = mysqli_real_escape_string($con,stripslashes($dt['cat_name']));
          $price = $dt['standard_price'];

          if($dt['masuk'] != ""){
            $masuk = $dt['masuk'];
          }else{
            $masuk = 0;
          }
          if($dt['keluar'] != ""){
            $keluar = $dt['keluar'];
          }else{
            $keluar = 0;
          }
          if($dt['incoming'] != ""){
            $incoming = $dt['incoming'];
          }else{
            $incoming = 0;
          }
          if($dt['outgoing'] != ""){
            $outgoing = $dt['outgoing'];
          }else{
            $outgoing = 0;
          }

          $stock = $masuk - $keluar;
          $forecasted = $stock + $incoming - $outgoing;
          $in = $masuk;
          $out = $keluar;
          $incoming = $incoming;
          $outgoing = $outgoing;

  				$values = $values . "('$db', '$intnum', '$nm', '$cat', $price, $stock, $in, $out, $incoming, $outgoing, $forecasted),";
  			}
  			$values = substr($values,0,-1) . ";";
  			$query = "INSERT INTO `invcons".$_SERVER['REMOTE_ADDR']."`(`db`, `intnum`, `name`, `category`, `price`, `stock`, `in`, `out`, `incoming`, `outgoing`, `forecast`) VALUES " . $values;
  			$submit = mysqli_query($con,$query);

        //GBU
        $conn_string = "host=192.168.1.111 dbname=GBU user=postgres password=postgres";
        $dbconn = pg_connect($conn_string);

        $num = 0;
        $loc_id_IN = '';
        $loc_dest_id_IN = '';

        $loc_id_OUT = '';
        $loc_dest_id_OUT = '';

        $i = pg_query($dbconn,"SELECT * FROM stock_warehouse");
        while($inv = pg_fetch_array($i)){
            if($num == 0){
                $loc_id_IN .= "A.location_id != '".$inv['lot_input_id']."' ";
                $loc_dest_id_IN .= "A.location_dest_id = '".$inv['lot_input_id']."'  ";

                $loc_id_OUT .= "A.location_id = '".$inv['lot_input_id']."' ";
                $loc_dest_id_OUT .= "A.location_dest_id != '".$inv['lot_input_id']."'  ";
            }else{
                $loc_id_IN .= "OR A.location_id != '".$inv['lot_input_id']."' ";
                $loc_dest_id_IN .= "OR A.location_dest_id = '".$inv['lot_input_id']."'  ";

                $loc_id_OUT .= "OR A.location_id = '".$inv['lot_input_id']."' ";
                $loc_dest_id_OUT .= "OR A.location_dest_id != '".$inv['lot_input_id']."'  ";
            }

            $num++;
        }

        $q = "select 'GBU' as db, * from (select C.parent_id,C.name as cat_name,A.default_code,A.name_template as name_prd,A.id,B.standard_price from product_product A, product_template B, product_category C where C.id = B.categ_id AND A.product_tmpl_id = B.id  $filter ORDER BY A.default_code) Z
        LEFT JOIN ( select sum(A.product_qty) as masuk, A.product_id from stock_move A, product_product B where ($loc_id_IN) and ($loc_dest_id_IN)  AND A.product_id = B.id and A.state = 'done' $date group by A.product_id,B.name_template)
        Y ON Z.id = Y.product_id
        LEFT JOIN ( select B.name_template,sum(A.product_qty) as keluar, A.product_id from stock_move A, product_product B where ($loc_id_OUT) and ($loc_dest_id_OUT)  AND A.product_id = B.id and A.state = 'done' $date group by A.product_id,B.name_template)
        X ON X.product_id = Z.id
        LEFT JOIN ( select B.name_template,sum(A.product_qty) as outgoing, A.product_id from stock_move A, product_product B where ($loc_id_OUT) and ($loc_dest_id_OUT)  AND A.product_id = B.id and A.state IN ('confirmed','waiting','assigned') $date group by A.product_id,B.name_template)
        W ON W.product_id = Z.id
        LEFT JOIN ( select sum(A.product_qty) as incoming, A.product_id from stock_move A, product_product B where ($loc_id_IN) and ($loc_dest_id_IN)  AND A.product_id = B.id and A.state IN ('confirmed','waiting','assigned') $date group by A.product_id,B.name_template)
        V ON Z.id = V.product_id";
        $rs =  pg_query($dbconn,$q);
        $values = "";

  			while($dt = pg_fetch_assoc($rs)){
          $stock = 0;
          $forcasted = 0;

  				$db = $dt['db'];
  				$intnum = mysqli_real_escape_string($con,stripslashes($dt['default_code']));
          $nm = mysqli_real_escape_string($con,stripslashes($dt['name_prd']));
          $cat = mysqli_real_escape_string($con,stripslashes($dt['cat_name']));
          $price = $dt['standard_price'];

          if($dt['masuk'] != ""){
            $masuk = $dt['masuk'];
          }else{
            $masuk = 0;
          }
          if($dt['keluar'] != ""){
            $keluar = $dt['keluar'];
          }else{
            $keluar = 0;
          }
          if($dt['incoming'] != ""){
            $incoming = $dt['incoming'];
          }else{
            $incoming = 0;
          }
          if($dt['outgoing'] != ""){
            $outgoing = $dt['outgoing'];
          }else{
            $outgoing = 0;
          }

          $stock = $masuk - $keluar;
          $forecasted = $stock + $incoming - $outgoing;
          $in = $masuk;
          $out = $keluar;
          $incoming = $incoming;
          $outgoing = $outgoing;

  				$values = $values . "('$db', '$intnum', '$nm', '$cat', $price, $stock, $in, $out, $incoming, $outgoing, $forecasted),";
  			}
  			$values = substr($values,0,-1) . ";";
  			$query = "INSERT INTO `invcons".$_SERVER['REMOTE_ADDR']."`(`db`, `intnum`, `name`, `category`, `price`, `stock`, `in`, `out`, `incoming`, `outgoing`, `forecast`) VALUES " . $values;
  			$submit = mysqli_query($con,$query);

        //RTK
        $conn_string = "host=192.168.1.111 dbname=RAJATEKNOLOGI_2016 user=postgres password=postgres";
        $dbconn = pg_connect($conn_string);

        $num = 0;
        $loc_id_IN = '';
        $loc_dest_id_IN = '';

        $loc_id_OUT = '';
        $loc_dest_id_OUT = '';

        $i = pg_query($dbconn,"SELECT * FROM stock_warehouse");
        while($inv = pg_fetch_array($i)){
            if($num == 0){
                $loc_id_IN .= "A.location_id != '".$inv['lot_input_id']."' ";
                $loc_dest_id_IN .= "A.location_dest_id = '".$inv['lot_input_id']."'  ";

                $loc_id_OUT .= "A.location_id = '".$inv['lot_input_id']."' ";
                $loc_dest_id_OUT .= "A.location_dest_id != '".$inv['lot_input_id']."'  ";
            }else{
                $loc_id_IN .= "OR A.location_id != '".$inv['lot_input_id']."' ";
                $loc_dest_id_IN .= "OR A.location_dest_id = '".$inv['lot_input_id']."'  ";

                $loc_id_OUT .= "OR A.location_id = '".$inv['lot_input_id']."' ";
                $loc_dest_id_OUT .= "OR A.location_dest_id != '".$inv['lot_input_id']."'  ";
            }

            $num++;
        }

        $q = "select 'RTK' as db, * from (select C.parent_id,C.name as cat_name,A.default_code,A.name_template as name_prd,A.id,B.standard_price from product_product A, product_template B, product_category C where C.id = B.categ_id AND A.product_tmpl_id = B.id  $filter ORDER BY A.default_code) Z
        LEFT JOIN ( select sum(A.product_qty) as masuk, A.product_id from stock_move A, product_product B where ($loc_id_IN) and ($loc_dest_id_IN)  AND A.product_id = B.id and A.state = 'done' $date group by A.product_id,B.name_template)
        Y ON Z.id = Y.product_id
        LEFT JOIN ( select B.name_template,sum(A.product_qty) as keluar, A.product_id from stock_move A, product_product B where ($loc_id_OUT) and ($loc_dest_id_OUT)  AND A.product_id = B.id and A.state = 'done' $date group by A.product_id,B.name_template)
        X ON X.product_id = Z.id
        LEFT JOIN ( select B.name_template,sum(A.product_qty) as outgoing, A.product_id from stock_move A, product_product B where ($loc_id_OUT) and ($loc_dest_id_OUT)  AND A.product_id = B.id and A.state IN ('confirmed','waiting','assigned') $date group by A.product_id,B.name_template)
        W ON W.product_id = Z.id
        LEFT JOIN ( select sum(A.product_qty) as incoming, A.product_id from stock_move A, product_product B where ($loc_id_IN) and ($loc_dest_id_IN)  AND A.product_id = B.id and A.state IN ('confirmed','waiting','assigned') $date group by A.product_id,B.name_template)
        V ON Z.id = V.product_id";
        $rs =  pg_query($dbconn,$q);
        $values = "";

  			while($dt = pg_fetch_assoc($rs)){
          $stock = 0;
          $forcasted = 0;

  				$db = $dt['db'];
  				$intnum = mysqli_real_escape_string($con,stripslashes($dt['default_code']));
          $nm = mysqli_real_escape_string($con,stripslashes($dt['name_prd']));
          $cat = mysqli_real_escape_string($con,stripslashes($dt['cat_name']));
          $price = $dt['standard_price'];

          if($dt['masuk'] != ""){
            $masuk = $dt['masuk'];
          }else{
            $masuk = 0;
          }
          if($dt['keluar'] != ""){
            $keluar = $dt['keluar'];
          }else{
            $keluar = 0;
          }
          if($dt['incoming'] != ""){
            $incoming = $dt['incoming'];
          }else{
            $incoming = 0;
          }
          if($dt['outgoing'] != ""){
            $outgoing = $dt['outgoing'];
          }else{
            $outgoing = 0;
          }

          $stock = $masuk - $keluar;
          $forecasted = $stock + $incoming - $outgoing;
          $in = $masuk;
          $out = $keluar;
          $incoming = $incoming;
          $outgoing = $outgoing;

  				$values = $values . "('$db', '$intnum', '$nm', '$cat', $price, $stock, $in, $out, $incoming, $outgoing, $forecasted),";
  			}
  			$values = substr($values,0,-1) . ";";
  			$query = "INSERT INTO `invcons".$_SERVER['REMOTE_ADDR']."`(`db`, `intnum`, `name`, `category`, `price`, `stock`, `in`, `out`, `incoming`, `outgoing`, `forecast`) VALUES " . $values;
  			$submit = mysqli_query($con,$query);

        //NAIK
        $conn_string = "host=192.168.1.111 dbname=NAIKREATIF user=postgres password=postgres";
        $dbconn = pg_connect($conn_string);

        $num = 0;
        $loc_id_IN = '';
        $loc_dest_id_IN = '';

        $loc_id_OUT = '';
        $loc_dest_id_OUT = '';

        $i = pg_query($dbconn,"SELECT * FROM stock_warehouse");
        while($inv = pg_fetch_array($i)){
            if($num == 0){
                $loc_id_IN .= "A.location_id != '".$inv['lot_input_id']."' ";
                $loc_dest_id_IN .= "A.location_dest_id = '".$inv['lot_input_id']."'  ";

                $loc_id_OUT .= "A.location_id = '".$inv['lot_input_id']."' ";
                $loc_dest_id_OUT .= "A.location_dest_id != '".$inv['lot_input_id']."'  ";
            }else{
                $loc_id_IN .= "OR A.location_id != '".$inv['lot_input_id']."' ";
                $loc_dest_id_IN .= "OR A.location_dest_id = '".$inv['lot_input_id']."'  ";

                $loc_id_OUT .= "OR A.location_id = '".$inv['lot_input_id']."' ";
                $loc_dest_id_OUT .= "OR A.location_dest_id != '".$inv['lot_input_id']."'  ";
            }

            $num++;
        }

        $q = "select 'NAIK' as db, * from (select C.parent_id,C.name as cat_name,A.default_code,A.name_template as name_prd,A.id,B.standard_price from product_product A, product_template B, product_category C where C.id = B.categ_id AND A.product_tmpl_id = B.id  $filter ORDER BY A.default_code) Z
        LEFT JOIN ( select sum(A.product_qty) as masuk, A.product_id from stock_move A, product_product B where ($loc_id_IN) and ($loc_dest_id_IN)  AND A.product_id = B.id and A.state = 'done' $date group by A.product_id,B.name_template)
        Y ON Z.id = Y.product_id
        LEFT JOIN ( select B.name_template,sum(A.product_qty) as keluar, A.product_id from stock_move A, product_product B where ($loc_id_OUT) and ($loc_dest_id_OUT)  AND A.product_id = B.id and A.state = 'done' $date group by A.product_id,B.name_template)
        X ON X.product_id = Z.id
        LEFT JOIN ( select B.name_template,sum(A.product_qty) as outgoing, A.product_id from stock_move A, product_product B where ($loc_id_OUT) and ($loc_dest_id_OUT)  AND A.product_id = B.id and A.state IN ('confirmed','waiting','assigned') $date group by A.product_id,B.name_template)
        W ON W.product_id = Z.id
        LEFT JOIN ( select sum(A.product_qty) as incoming, A.product_id from stock_move A, product_product B where ($loc_id_IN) and ($loc_dest_id_IN)  AND A.product_id = B.id and A.state IN ('confirmed','waiting','assigned') $date group by A.product_id,B.name_template)
        V ON Z.id = V.product_id";
        $rs =  pg_query($dbconn,$q);
        $values = "";

  			while($dt = pg_fetch_assoc($rs)){
          $stock = 0;
          $forcasted = 0;

  				$db = $dt['db'];
  				$intnum = mysqli_real_escape_string($con,stripslashes($dt['default_code']));
          $nm = mysqli_real_escape_string($con,stripslashes($dt['name_prd']));
          $cat = mysqli_real_escape_string($con,stripslashes($dt['cat_name']));
          $price = $dt['standard_price'];

          if($dt['masuk'] != ""){
            $masuk = $dt['masuk'];
          }else{
            $masuk = 0;
          }
          if($dt['keluar'] != ""){
            $keluar = $dt['keluar'];
          }else{
            $keluar = 0;
          }
          if($dt['incoming'] != ""){
            $incoming = $dt['incoming'];
          }else{
            $incoming = 0;
          }
          if($dt['outgoing'] != ""){
            $outgoing = $dt['outgoing'];
          }else{
            $outgoing = 0;
          }

          $stock = $masuk - $keluar;
          $forecasted = $stock + $incoming - $outgoing;
          $in = $masuk;
          $out = $keluar;
          $incoming = $incoming;
          $outgoing = $outgoing;

  				$values = $values . "('$db', '$intnum', '$nm', '$cat', $price, $stock, $in, $out, $incoming, $outgoing, $forecasted),";
  			}
  			$values = substr($values,0,-1) . ";";
  			$query = "INSERT INTO `invcons".$_SERVER['REMOTE_ADDR']."`(`db`, `intnum`, `name`, `category`, `price`, `stock`, `in`, `out`, `incoming`, `outgoing`, `forecast`) VALUES " . $values;
  			$submit = mysqli_query($con,$query);

        //start
        //$q = "SELECT * FROM `invcons".$_SERVER['REMOTE_ADDR']."` ";
        $q = "SELECT intnum, name, category, sum(`in`) `in`, sum(`out`)`out`, sum(stock) stock, sum(incoming) incoming, sum(outgoing) outgoing, sum(forecast) forecast
              FROM `invcons".$_SERVER['REMOTE_ADDR']."`
              GROUP BY intnum, name, category";

        $rs = mysqli_query($con,$q);
      		//echo $q2;
      	while($res = mysqli_fetch_assoc($rs)){

            //if($stock != '' || $stock != 0){
            if($nullfield == 'on'){
                if($res['stock'] != 0){

            ?>
            <tr>
                    <!-- <td><?php echo $res['db'];?></td> -->
                    <td><?php echo $res['intnum'];?></td>
                    <td><?php echo $res['name'];?></td>
                    <td class="hidden-xs"><?php echo $res['category'];?></td>
                    <?php if($best == 'on'){ ?>
                        <td class="hidden-xs"><?php echo round($res['in']);?></td>
                        <td class="hidden-xs"><?php echo round($res['out']);?></td>
                    <?php }else{ ?>
			<?php if($cost == 'on'){ ?>
                        <!-- <td class="hidden-xs"><?php echo number_format($res['price']);?></td> -->
			<?php } ?>
                    <?php } ?>
                    <td><strong><?php echo $res['stock'];?></strong></td>
                    <td><?php echo round($res['incoming']);?></td>
                    <td><?php echo round($res['outgoing']);?></td>
                    <!--td><strong><?php echo round($res['forecast']);?></strong></td-->
                </tr>
            <?php
                }
            }else{
            ?>
                <tr>
                    <!-- <td><?php echo $res['db'];?></td> -->
                    <td><?php echo $res['intnum'];?></td>
                    <td><?php echo $res['name'];?></td>
                    <td class="hidden-xs"><?php echo $res['category'];?></td>
                    <?php if($best == 'on'){ ?>
                        <td class="hidden-xs"><?php echo round($res['in']);?></td>
                        <td class="hidden-xs"><?php echo round($res['out']);?></td>
                    <?php }else{ ?>

                    <?php } ?>
                    <td><strong><?php echo $res['stock'];?></strong></td>
                    <td><?php echo round($res['incoming']);?></td>
                    <td><?php echo round($res['outgoing']);?></td>
                    <!--td><strong><?php echo $res['forecast'];?></strong></td-->
                </tr>
            <?php
            }
        }   ?>

    </tbody>
</table>

<div style="display:none">
    <table id="detailsTable">
        <thead>
            <tr>
                <th>DB</th>
                <th>Internal Number</th>
                <th>Name</th>
                <th class="hidden-xs">Category</th>
                <?php if($best == 'on'){ ?>
                    <th class="hidden-xs">TOTAL IN</th>
                    <th class="hidden-xs">TOTAL OUT</th>
                <?php }else{ ?>
        		<?php if($cost == 'on'){ ?>
                    <th class="hidden-xs">Cost Price</th>
        		<?php } ?>
                <?php } ?>
                <th>Stock</th>
                <th class="">IN</th>
                <th class="">OUT</th>
                <!--th>Forcasted</th-->
            </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>

</div>
    <script src="assets/datatables.js" type="text/javascript"></script>

<script>
function fnFormatDetails(table_id, html) {
    var sOut = "<table id=\"example5_" + table_id + "\" class=\" display responsive no-wrap\" width=\"100%\">";
    sOut += html;
    sOut += "</table>";
    return sOut;
}

var iTableCounter = 1;
var oTable;
var oInnerTable;
var detailsTableHtml;

//Run On HTML Build
$(document).ready(function () {

	// you would probably be using templates here
	detailsTableHtml = $("#detailsTable").html();

	//Insert a 'details' column to the table
	var nCloneTh = document.createElement('th');
	var nCloneTd = document.createElement('td');
	nCloneTd.innerHTML = '<img src="http://i.imgur.com/SD7Dz.png">';
	nCloneTd.className = "center";

	$('#example5 thead tr').each(function () {
		this.insertBefore(nCloneTh, this.childNodes[0]);
	});

	$('#example5 tbody tr').each(function () {
		this.insertBefore(nCloneTd.cloneNode(true), this.childNodes[0]);
	});


	//Initialse DataTables, with no sorting on the 'details' column
	var oTable = $('#example5').dataTable({
    orderCellsTop: true,
    responsive: false,
		"bJQueryUI": true,
    "bLengthChange": false,
    "iDisplayLength": 10,
    "scrollX": true,
    "oLanguage": {
      "oPaginate": {
      "sPrevious": "<<",
      "sNext": ">>",
      }
    },
		"aoColumns": [
			{
			   "mDataProp": null,
			   "sClass": "control center",
			   "sDefaultContent": '<img src="http://i.imgur.com/SD7Dz.png">'
			},
			{ "mDataProp": "intnum" },
			{ "mDataProp": "name" },
			{ "mDataProp": "category" },
      <?php if($best == 'on'){ ?>{ "mDataProp": "in" }, <?php } ?>
			<?php if($best == 'on'){ ?>{ "mDataProp": "out" }, <?php } ?>
			{ "mDataProp": "stock" },
			{ "mDataProp": "incoming" },
			{ "mDataProp": "outgoing" }
		],
		"aaSorting": [[1, 'asc']],
    });

        /* Add event listener for opening and closing details
        * Note that the indicator for showing which row is open is not controlled by DataTables,
        * rather it is done here
        */

		var table = $('#example5').DataTable();

		$('#example5 tbody td').on('click', 'img', function () {
            var nTr = $(this).parents('tr')[0];
            var nTds = this;

			var coldata = table.row( nTr ).data();

            if (oTable.fnIsOpen(nTr)) {
                /* This row is already open - close it */
                this.src = "http://i.imgur.com/SD7Dz.png";
                oTable.fnClose(nTr);
            }
            else {
                /* Open this row */
                var rowIndex = oTable.fnGetPosition( $(nTds).closest('tr')[0] );

                this.src = "http://i.imgur.com/d4ICC.png";
                oTable.fnOpen(nTr, fnFormatDetails(iTableCounter, detailsTableHtml), 'details');
                oInnerTable = $("#example5_" + iTableCounter).dataTable({
                    orderCellsTop: true,
                    responsive: false,
                    "bJQueryUI": true,
                    "bSort" : true, // disables sorting
                    "bLengthChange": false,
              			"iDisplayLength": 10,
              			"scrollX": true,
              			"oLanguage": {
              			  "oPaginate": {
              				"sPrevious": "<<",
              				"sNext": ">>",
              			  }
              			},
					"ajax": {
						"url": 'serverside/inventory_list_cons_detail.php?code=' + coldata.intnum ,
						"dataSrc": ""
					},
					"columns": [
						{ "data": "db" },
						{ "data": "intnum" },
						{ "data": "name" },
            { "data": "category" },
            <?php if($best == 'on'){ ?>{ "mDataProp": "in" }, <?php } ?>
            <?php if($best == 'on'){ ?>{ "mDataProp": "out" }, <?php } ?>
            { "data": "stock" },
            { "data": "incoming" },
            { "data": "outgoing" }
					],
                });
                iTableCounter = iTableCounter + 1;
            }
        });


    });
</script>
