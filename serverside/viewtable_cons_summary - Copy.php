<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">

<?php 
include "../web_conf.php";


//drop and create table
	date_default_timezone_set('Asia/Jakarta');
	$localhostdb = '192.168.1.70';
	$usernamedb  = 'root';
	$passworddb  = 'Qwert!123';
	$databasedb  = 'oereporting';
		
	$con = mysqli_connect($localhostdb,$usernamedb,$passworddb,$databasedb)or die("cannot connect"); 

	$query = "DROP TABLE IF EXISTS `so_view".$_SERVER['REMOTE_ADDR']."`";
	$submit = mysqli_query($con,$query);

	$query = "CREATE TABLE `so_view".$_SERVER['REMOTE_ADDR']."` (`category_id` VARCHAR(10) NOT NULL,`category` VARCHAR(50) NOT NULL,`nominal` DOUBLE NOT NULL,`db` VARCHAR(50) NOT NULL) ENGINE=InnoDB";

	$submit = mysqli_query($con,$query);
//end drop and create table


//set filter
	if ($_GET['per'] == 'Q1'){
		$start = $_GET['thn'] . "-01-01";
		$end = $_GET['thn'] . "-03-31";
	}else if ($_GET['per'] == 'Q2'){
		$start = $_GET['thn'] . "-04-01";
		$end = $_GET['thn'] . "-06-30";
	}else if ($_GET['per'] == 'Q3'){
		$start = $_GET['thn'] . "-07-01";
		$end = $_GET['thn'] . "-09-30";
	}else if ($_GET['per'] == 'Q4'){
		$start = $_GET['thn'] . "-10-01";
		$end = $_GET['thn'] . "-12-31";
	}

	$so = "yes";// $_GET['so'];
	$db = "TOTALINPRO_2016"; //$_GET['db'];
	$type =  "out_invoice"; //$_GET['type'];
	$state =  "all"; //$_GET['state'];
	$type_report=  "so"; //$_GET['type_report'];
//end set filter


//start variable
	$sum = 0;
	$sumnom = 0;
	$filter = '';
	$filter_prn = '';
	$info = 'No Company Category';
	$no = 1;
	$cat = "";
	if($type_report == 'so'){
		$state_dt = "AND B.state != 'cancel' AND B.state != 'draft' AND B.state != 'sent'";
		$state_dt2 = "AND state != 'cancel' AND state != 'draft' AND state != 'sent'";

		$string_state = 'ALL SALES ORDER';
		$type_string = "Document : SO Category Consolidate Report " . $_GET['per'] . " - " . $_GET['thn'];
	}
//end variable


//start loop query
	//DB TIM
		$conn_string = "host=".$_SESSION['ip_local']." dbname=".$_SESSION['db_tim']." user=".$_SESSION['user_pg']." password=";
		$dbconn = pg_connect($conn_string);
		
		$state_list = "b.state != 'cancel'";
		$state_sales = "AND A.state != 'cancel' AND A.state != 'draft' AND A.state != 'sent'";
//		$q2 ="SELECT categ_id, name,SUM(total) as final,SUM(not_diskon) as not_diskon FROM (SELECT A.product_id ,sum(A.product_uom_qty*A.sm_price_unit_after_disc3) as total,SUM(A.product_uom_qty*A.price_unit) as not_diskon FROM sale_order_line A,sale_order B where B.date_order >= '$start' AND B.date_order <= '$end' AND B.date_order IS NOT NULL AND B.currency_id = '13' $state_dt AND A.order_id = B.id group by A.product_id) Z LEFT JOIN (SELECT B.categ_id, A.id,C.name,A.default_code,A.name_template,A.product_tmpl_id FROM product_product A,product_template B,product_category C WHERE B.categ_id = C.id AND A.product_tmpl_id = B.id) Y ON Z.product_id = Y.id GROUP BY categ_id, name";
$q2 = "
select C.id as categ_id,C.name,sum(product_uom_qty* sm_price_unit_after_disc3) as final from(
	select *,sum(product_uom_qty* sm_price_unit_after_disc3) as st from sale_order_line where date_order BETWEEN '$start'  AND '$end' $state_dt2
	GROUP BY id) A 
	LEFT JOIN product_category C ON C.id = A.categ_id
	GROUP BY C.id
       ORDER BY C.name

";
//echo $q2;

		$d = pg_query($dbconn,$q2);
		
		$values = "";
			
		while($dt = pg_fetch_assoc($d)){
			if($dt['name'] == ''){
				$nom = 0;
				$sum += 0;
				$nm = 'No Product ID';
				$categid = '0';
			}else{
				$nom = $dt['final'];
				$sum += $dt['final'];
				$nm = $dt['name'];
				$categid = $dt['categ_id'];
			}
			
			$values = $values . "('$categid', '$nm', $nom, 'TIM'),";
		}
		$values = substr($values,0,-1) . ";";
		$query = "INSERT INTO `so_view".$_SERVER['REMOTE_ADDR']."`(`category_id`, `category`, `nominal`, `db`) VALUES " . $values;
		//echo $query;
		$submit = mysqli_query($con,$query);
	//DB WIS
		$conn_string = "host=192.168.1.111 dbname=WILLERTINDO_2016 user=openerp password=";
		$dbconn = pg_connect($conn_string);
		
		$state_list = "b.state != 'cancel'";
		$state_sales = "AND A.state != 'cancel' AND A.state != 'draft' AND A.state != 'sent'";
//		$q2 ="SELECT categ_id, name,SUM(total) as final,SUM(not_diskon) as not_diskon FROM (SELECT A.product_id ,sum(A.product_uom_qty*A.sm_price_unit_after_disc3) as total,SUM(A.product_uom_qty*A.price_unit) as not_diskon FROM sale_order_line A,sale_order B where B.date_order >= '$start' AND B.date_order <= '$end' AND B.date_order IS NOT NULL AND B.currency_id = '13' $state_dt AND A.order_id = B.id group by A.product_id) Z LEFT JOIN (SELECT B.categ_id, A.id,C.name,A.default_code,A.name_template,A.product_tmpl_id FROM product_product A,product_template B,product_category C WHERE B.categ_id = C.id AND A.product_tmpl_id = B.id) Y ON Z.product_id = Y.id GROUP BY categ_id, name";
$q2 = "
select C.id as categ_id,C.name,sum(product_uom_qty* sm_price_unit_after_disc3) as final from(
	select *,sum(product_uom_qty* sm_price_unit_after_disc3) as st from sale_order_line where date_order BETWEEN '$start'  AND '$end' $state_dt2
	GROUP BY id) A 
	LEFT JOIN product_category C ON C.id = A.categ_id
	GROUP BY C.id
       ORDER BY C.name

";
		$d = pg_query($dbconn,$q2);
		
		$values = "";
			
		while($dt = pg_fetch_assoc($d)){
			if($dt['name'] == ''){
				$nom = 0;
				$sum += 0;
				$nm = 'No Product ID';
				$categid = '0';
			}else{
				$nom = $dt['final'];
				$sum += $dt['final'];
				$nm = $dt['name'];
				$categid = $dt['categ_id'];
			}
			
			$values = $values . "('$categid', '$nm', $nom, 'WIS'),";
		}
		$values = substr($values,0,-1) . ";";
		$query = "INSERT INTO `so_view".$_SERVER['REMOTE_ADDR']."`(`category_id`, `category`, `nominal`, `db`) VALUES " . $values;
		$submit = mysqli_query($con,$query);
	//DB Ratek
		$conn_string = "host=192.168.1.111 dbname=RAJATEKNOLOGI_2016 user=openerp password=";
		$dbconn = pg_connect($conn_string);
		
		$state_list = "b.state != 'cancel'";
		$state_sales = "AND A.state != 'cancel' AND A.state != 'draft' AND A.state != 'sent'";
//		$q2 ="SELECT categ_id, name,SUM(total) as final,SUM(not_diskon) as not_diskon FROM (SELECT A.product_id ,sum(A.product_uom_qty*A.sm_price_unit_after_disc3) as total,SUM(A.product_uom_qty*A.price_unit) as not_diskon FROM sale_order_line A,sale_order B where B.date_order >= '$start' AND B.date_order <= '$end' AND B.date_order IS NOT NULL AND B.currency_id = '13' $state_dt AND A.order_id = B.id group by A.product_id) Z LEFT JOIN (SELECT B.categ_id, A.id,C.name,A.default_code,A.name_template,A.product_tmpl_id FROM product_product A,product_template B,product_category C WHERE B.categ_id = C.id AND A.product_tmpl_id = B.id) Y ON Z.product_id = Y.id GROUP BY categ_id, name";
$q2 = "
select C.id as categ_id,C.name,sum(product_uom_qty* sm_price_unit_after_disc3) as final from(
	select *,sum(product_uom_qty* sm_price_unit_after_disc3) as st from sale_order_line where date_order BETWEEN '$start'  AND '$end' $state_dt2
	GROUP BY id) A 
	LEFT JOIN product_category C ON C.id = A.categ_id
	GROUP BY C.id
       ORDER BY C.name

";
		$d = pg_query($dbconn,$q2);
		
		$values = "";
			
		while($dt = pg_fetch_assoc($d)){
			if($dt['name'] == ''){
				$nom = 0;
				$sum += 0;
				$nm = 'No Product ID';
				$categid = '0';
			}else{
				$nom = $dt['final'];
				$sum += $dt['final'];
				$nm = $dt['name'];
				$categid = $dt['categ_id'];
			}
			
			$values = $values . "('$categid', '$nm', $nom, 'RaTek'),";
		}
		$values = substr($values,0,-1) . ";";
		$query = "INSERT INTO `so_view".$_SERVER['REMOTE_ADDR']."`(`category_id`, `category`, `nominal`, `db`) VALUES " . $values;
		$submit = mysqli_query($con,$query);
	//DB GBU
		$conn_string = "host=".$_SESSION['ip_vps']." dbname=GBU user=".$_SESSION['user_pg']." password=";
		$dbconn = pg_connect($conn_string);
		
		$state_list = "b.state != 'cancel'";
		$state_sales = "AND A.state != 'cancel' AND A.state != 'draft' AND A.state != 'sent'";
//		$q2 ="SELECT categ_id, name,SUM(total) as final,SUM(not_diskon) as not_diskon FROM (SELECT A.product_id ,sum(A.product_uom_qty*A.sm_price_unit_after_disc3) as total,SUM(A.product_uom_qty*A.price_unit) as not_diskon FROM sale_order_line A,sale_order B where B.date_order >= '$start' AND B.date_order <= '$end' AND B.date_order IS NOT NULL AND B.currency_id = '13' $state_dt AND A.order_id = B.id group by A.product_id) Z LEFT JOIN (SELECT B.categ_id, A.id,C.name,A.default_code,A.name_template,A.product_tmpl_id FROM product_product A,product_template B,product_category C WHERE B.categ_id = C.id AND A.product_tmpl_id = B.id) Y ON Z.product_id = Y.id GROUP BY categ_id, name";
$q2 = "
select C.id as categ_id,C.name,sum(product_uom_qty* sm_price_unit_after_disc3) as final from(
	select *,sum(product_uom_qty* sm_price_unit_after_disc3) as st from sale_order_line where date_order BETWEEN '$start'  AND '$end' $state_dt2
	GROUP BY id) A 
	LEFT JOIN product_category C ON C.id = A.categ_id
	GROUP BY C.id
       ORDER BY C.name

";
		$d = pg_query($dbconn,$q2);
		
		$values = "";
			
		while($dt = pg_fetch_assoc($d)){
			if($dt['name'] == ''){
				$nom = 0;
				$sum += 0;
				$nm = 'No Product ID';
				$categid = '0';
			}else{
				$nom = $dt['final'];
				$sum += $dt['final'];
				$nm = $dt['name'];
				$categid = $dt['categ_id'];
			}
			
			$values = $values . "('$categid', '$nm', $nom, 'GBU'),";
		}
		$values = substr($values,0,-1) . ";";
		$query = "INSERT INTO `so_view".$_SERVER['REMOTE_ADDR']."`(`category_id`, `category`, `nominal`, `db`) VALUES " . $values;
		$submit = mysqli_query($con,$query);
	//DB Naik
		$conn_string = "host=192.168.1.111 dbname=NAIKREATIF user=openerp password=";
		$dbconn = pg_connect($conn_string);
		
		$state_list = "b.state != 'cancel'";
		$state_sales = "AND A.state != 'cancel' AND A.state != 'draft' AND A.state != 'sent'";
//		$q2 ="SELECT categ_id, name,SUM(total) as final,SUM(not_diskon) as not_diskon FROM (SELECT A.product_id ,sum(A.product_uom_qty*A.sm_price_unit_after_disc3) as total,SUM(A.product_uom_qty*A.price_unit) as not_diskon FROM sale_order_line A,sale_order B where B.date_order >= '$start' AND B.date_order <= '$end' AND B.date_order IS NOT NULL AND B.currency_id = '13' $state_dt AND A.order_id = B.id group by A.product_id) Z LEFT JOIN (SELECT B.categ_id, A.id,C.name,A.default_code,A.name_template,A.product_tmpl_id FROM product_product A,product_template B,product_category C WHERE B.categ_id = C.id AND A.product_tmpl_id = B.id) Y ON Z.product_id = Y.id GROUP BY categ_id, name";
$q2 = "
select C.id as categ_id,C.name,sum(product_uom_qty* sm_price_unit_after_disc3) as final from(
	select *,sum(product_uom_qty* sm_price_unit_after_disc3) as st from sale_order_line where date_order BETWEEN '$start'  AND '$end' $state_dt2
	GROUP BY id) A 
	LEFT JOIN product_category C ON C.id = A.categ_id
	GROUP BY C.id
       ORDER BY C.name

";
		$d = pg_query($dbconn,$q2);
		
		$values = "";
			
		while($dt = pg_fetch_assoc($d)){
			if($dt['name'] == ''){
				$nom = 0;
				$sum += 0;
				$nm = 'No Product ID';
				$categid = '0';
			}else{
				$nom = $dt['final'];
				$sum += $dt['final'];
				$nm = $dt['name'];
				$categid = $dt['categ_id'];
			}
			
			$values = $values . "('$categid', '$nm', $nom, 'Naik'),";
		}
		$values = substr($values,0,-1) . ";";
		$query = "INSERT INTO `so_view".$_SERVER['REMOTE_ADDR']."`(`category_id`, `category`, `nominal`, `db`) VALUES " . $values;
		$submit = mysqli_query($con,$query);
//end loop query


//start display
?>
<div class="box box-solid">
	<div class="box-header with-border">
		<h3 class="box-title"><?php echo $string_state.' : '.$info.' ('.$start.' to '.$end.')'; ?></h3>
	</div><!-- /.box-header -->
	<div class="box-body">
		<table id="exampleTable" class="display" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>Name</th>
					<th>IDUser</th>
					<th>Category</th>
					<th>IDCat</th>
					<th>Total</th>
					<th>Target</th>
					<th>Selisih</th>
					<th>Pencapaian</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th colspan="7" style="text-align:right">Total:</th>
					<th></th>
					<th></th>
				</tr>
			</tfoot>
			<tbody> <?php
				$sq = "SELECT idusr, usr, target, tahun, idcat, nama, sum(nominal) nominal FROM (
				SELECT A.iduser idusr, A.nama usr, ". $_GET['per'] . " target, tahun, C.idcat idcat, C.nama, D1.second_name db, idoe, sum(nominal) nominal, (SELECT sum(nominal) nominal FROM tblmasteruser A INNER JOIN tblmastertarget B ON A.iduser = B.iduser 
				INNER JOIN tblmastercategory C ON B.idcat = C.idcat
				INNER JOIN tblmastercategory_detail D ON C.idcat = D.idcat
				INNER JOIN tblmasterdatabase D1 ON D.dbid = D1.id
				INNER JOIN `so_view".$_SERVER['REMOTE_ADDR']."` E ON E.category_id = D.idoe AND E.db = D1.second_name
				WHERE A.iduser = '" .$_GET['usr'] ."' AND tahun = " .$_GET['thn'] .") sumall FROM tblmasteruser A INNER JOIN tblmastertarget B ON A.iduser = B.iduser 
				INNER JOIN tblmastercategory C ON B.idcat = C.idcat
				INNER JOIN tblmastercategory_detail D ON C.idcat = D.idcat
				INNER JOIN tblmasterdatabase D1 ON D.dbid = D1.id
				INNER JOIN `so_view".$_SERVER['REMOTE_ADDR']."` E ON E.category_id = D.idoe AND E.db = D1.second_name
				WHERE A.iduser = '" .$_GET['usr'] ."' AND tahun = " .$_GET['thn'] ." GROUP BY A.nama, target, tahun, C.nama, D1.second_name, idoe) A
				GROUP BY idusr, usr, target, tahun, idcat, nama
				ORDER BY nama";

				$rs = mysqli_query($con,$sq);
				//echo $q2;
				while($res = mysqli_fetch_assoc($rs)){ 
					$nom = $res['nominal'];
					if($nom != 0){
						?>
					<tr data-toggle="tooltip" title="Total <?php echo $res['usr'].' : '.number_format($res['nominal']); ?>">
						<td><?php echo $res['usr']; ?></td>
						<td><?php echo $res['idusr']; ?></td>
						<td><?php echo $res['nama']; ?></td>
						<td><?php echo $res['idcat']; ?></td>
						<td><?php echo number_format($res['nominal']) ?></td>
						<td><?php echo number_format($res['target']) ?></td>
						<td<?php 
								if ($res['target'] - $res['nominal'] < 0){
									echo " style='color:limegreen;'";
								}else{
									echo " style='color:red;'"; 
								}
							?>
						><strong><?php echo number_format($res['nominal'] - $res['target']) ?></strong></td>
						<td><?php echo number_format($res['nominal']/$res['target']*100) . ' %' ?></td>
					</tr>
				<?php 
					}
				}?>
			</tbody>
		</table>
	</div><!-- /.box-body -->
</div>

<div style="display:none">    
    <table id="detailsTable">
        <thead> 
            <tr>
                <th>Category</th>
                <th>Database</th>
                <th>Nominal</th>
            </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>

    <script src="assets/js/jquery.js"></script>
    <script src="assets/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/datatables.min.js" type="text/javascript"></script>

<script>
function convertToRupiah(angka){
    var rupiah = '';
    var angkarev = angka.toString().split('').reverse().join('');
    for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
    return rupiah.split('',rupiah.length-1).reverse().join('');
}

function fnFormatDetails(table_id, html) {
    var sOut = "<table id=\"exampleTable_" + table_id + "\">";
    sOut += html;
    sOut += "</table>";
    return sOut;
}

var iTableCounter = 1;
var oTable;
var oInnerTable;
var detailsTableHtml;

//Run On HTML Build
$(document).ready(function () {
	
	// you would probably be using templates here
	detailsTableHtml = $("#detailsTable").html();

	//Insert a 'details' column to the table
	var nCloneTh = document.createElement('th');
	var nCloneTd = document.createElement('td');
	nCloneTd.innerHTML = '<img src="http://i.imgur.com/SD7Dz.png">';
	nCloneTd.className = "center";

	$('#exampleTable thead tr').each(function () {
		this.insertBefore(nCloneTh, this.childNodes[0]);
	});

	$('#exampleTable tbody tr').each(function () {
		this.insertBefore(nCloneTd.cloneNode(true), this.childNodes[0]);
	});
	
   
	//Initialse DataTables, with no sorting on the 'details' column
	var oTable = $('#exampleTable').dataTable({
		"bJQueryUI": true,
		"bPaginate": false,
		"aoColumns": [
			{
			   "mDataProp": null,
			   "sClass": "control center",
			   "sDefaultContent": '<img src="http://i.imgur.com/SD7Dz.png">'
			},
			{ "mDataProp": "Name" },
			{ "mDataProp": "IDUser" },
			{ "mDataProp": "Category" },
			{ "mDataProp": "IDCat" },
			{ "mDataProp": "Total" },
			{ "mDataProp": "Target" },
			{ "mDataProp": "Selisih" },
			{ "mDataProp": "Pencapaian" }
		],
		"columnDefs": [
            {
                "targets": [ 2 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [ 4 ],
                "visible": false,
				"searchable": false
            }
        ],
		"aaSorting": [[1, 'asc']],
		"footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // Total over all pages
            total = api
                .column( 5 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Total over this page

            pageTotal = api
                .column( 5, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
			var skrg = intVal(a) + intVal(b);
                    return skrg;
                }, 0 );
 		
            // Update footer
		total = convertToRupiah(total);
		var nom = convertToRupiah(pageTotal);
            $( api.column( 7 ).footer() ).html(
                ''+nom+' ( '+ total +' total)'
            );
        }
    });

        /* Add event listener for opening and closing details
        * Note that the indicator for showing which row is open is not controlled by DataTables,
        * rather it is done here
        */
		
		var table = $('#exampleTable').DataTable();
		
		$('#exampleTable tbody td').on('click', 'img', function () {
            var nTr = $(this).parents('tr')[0];
            var nTds = this;
			
			var coldata = table.row( nTr ).data();
            
            if (oTable.fnIsOpen(nTr)) {
                /* This row is already open - close it */
                this.src = "http://i.imgur.com/SD7Dz.png";
                oTable.fnClose(nTr);
            }
            else {
                /* Open this row */
                var rowIndex = oTable.fnGetPosition( $(nTds).closest('tr')[0] ); 
			   
                this.src = "http://i.imgur.com/d4ICC.png";
                oTable.fnOpen(nTr, fnFormatDetails(iTableCounter, detailsTableHtml), 'details');
                oInnerTable = $("#exampleTable_" + iTableCounter).dataTable({
                    "bJQueryUI": true,
                    "bSort" : true, // disables sorting
                    "bPaginate": false,
					"ajax": {
						"url": 'serverside/viewtable_cons_summary_detail.php?idusr=' + coldata.IDUser + '&idcat=' + coldata.IDCat + '&thn=<?= $_GET['thn'] ?>&per=<?= $_GET['per'] ?>',
						"dataSrc": ""
					},
					"columns": [
						{ "data": "nama" },
						{ "data": "db" },
						{ "data": "nominal",
						  "render": $.fn.dataTable.render.number( ',', '.', 0, )						
						}
					],
                });
                iTableCounter = iTableCounter + 1;
            }
        });


    });
</script>