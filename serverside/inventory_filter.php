<?php
session_start();
if($_GET['db'] != ''){

    if($_GET['db'] == 'GBU'){
	$host = $_SESSION['new_host'];
	$user_db = $_SESSION['new_user'];
    }else{
	$host = $_SESSION['host'];
	$user_db = $_SESSION['user_db'];
    }
    $conn_string = "host=".$host." dbname=".$_GET['db']." user=".$user_db." password= ";
    $_SESSION['db'] = $_GET['db'];
    $dbconn = pg_connect($conn_string);

    ?>
    <form class="form-horizontal" enctype="multipart/form-data">
        <div class="form-group" id="groupref">
          <label for="inputEmail3" class="col-sm-2 control-label">Reference Number</label>
          <div class="col-sm-10">
          <input type="hidden" value="<?php echo $_GET['db'];?>" readonly class="form-control" id="db" name="db" placeholder="Reference Number">
            <input type="text" class="form-control" id="ref" name="ref" onkeyup="empty();" placeholder="Reference Number">
          </div>
        </div>  
        <div class="form-group">
          <label for="inputEmail3" class="col-sm-2 control-label">Category</label>
          <div class="col-sm-4">
            <select class="form-control" id="categ" name="categ" onChange="cat_step(this.value);">
            <option value="">--- Pilih Category ---</option>
            <?php 
                $no = 0;
                $l = pg_query($dbconn,"SELECT * FROM product_category where id = '1' ");
                while($list = pg_fetch_assoc($l)){ 
                $no++;?>
                    <option value="<?php echo $list['id'];?>"><?php echo $list['name'];?></option>
                <?php   
                }
            ?>
            </select>
            <!--input type="file" class="form-control" id="date_start" required name="date_start" -->
          </div>
          <div class="col-sm-3" id="category2"></div>
          <div class="col-sm-3" id="category3"></div>
        </div>
        <div class="col-xs-6">
        <div class="form-group">
          <label for="inputEmail3" class="col-sm-4 control-label">Warehouse</label>
          <div class="col-sm-8">
            <select class="form-control" id="gudang" name="gudang">
            <option value="">--- ALL ---</option>
            <?php 
                $no = 0;
                $g = pg_query($dbconn,"SELECT * FROM stock_warehouse ORDER BY name");
                while($gdg = pg_fetch_assoc($g)){ 
                $no++;?>
                    <option value="<?php echo $gdg['id'];?>"><?php echo $gdg['name'];?></option>
                <?php   
                }
            ?>
            </select>
            <!--input type="file" class="form-control" id="date_start" required name="date_start" -->
          </div>
        </div>
        </div>
        <div class="col-xs-6">
        <div class="form-group">
          <label for="inputEmail3" class="col-sm-4 control-label">Pricelist</label>
          <div class="col-sm-8">
            <select class="form-control" id="pl" name="pl">
            <?php 
                $no = 0;
                $p = pg_query($dbconn,"SELECT * FROM (select id as pl_id,name from product_pricelist where active is true AND type != 'purchase') A
LEFT JOIN (select id as version_id,name,pricelist_id from product_pricelist_version) B ON A.pl_id = B.pricelist_id ORDER BY pl_id DESC");
                while($pl = pg_fetch_assoc($p)){ 
                $no++;?>
                    <option value="<?php echo $pl['version_id'];?>"><?php echo $pl['name'];?></option>
                <?php   
                }
            ?>
            </select>
            <!--input type="file" class="form-control" id="date_start" required name="date_start" -->
          </div>
        </div>
        </div>
    
        <div class="col-sm-5">
            <div class="form-group groupdate">
              <label for="inputEmail3" class="control-label">Date Start</label>
              
                <input type="date" class="form-control" id="date_start" name="date_start" >
                
                <!--input type="file" class="form-control" id="date_start" required name="date_start" -->
              
            </div>
        </div>
        <div class="col-sm-2"></div>
        <div class="col-sm-5">
            <div class="form-group groupdate" >
              <label for="inputEmail3" class="control-label">Date End</label>
             
                <input type="date" class="form-control" id="date_end" name="date_end" >
                
                <!--input type="file" class="form-control" id="date_start" required name="date_start" -->
            
            </div>
        </div>  

        <div class="form-group">
          <label for="inputEmail3" class="col-sm-2 control-label"></label>
          <div class="col-sm-10">
            <div class="checkbox">
            <div class="col-sm-3">
                <label>
                  <input type="checkbox" checked id="nullfield" name="nullfield">
                  Hidden zero stock
                </label>        
            </div>
            <div class="col-sm-3">
                <label>
                  <input type="checkbox" onChange="bestSearch();" id="best" name="best">
                  Best Performance Item
                </label>
            </div>
            <div class="col-sm-2" onClick="openModal();">
                <label>
                  <input type="checkbox"  disabled="disabled" id="cost" name="cost">
                  Show Cost Price
                </label>
            </div>

            <div class="col-sm-2"></div>
            <div class="col-sm-2"></div>
            </div>
          </div>
        </div>                          

                                                
    <div class="box-footer">    
        <div onclick="preview();" class="btn btn-flat btn-success pull-right" type="submit" class="btn btn-success btn-flat pull-right"><i class="glyphicon glyphicon-download"></i> Go!</div>
    </div><!-- /.box-footer -->     
    <div id="preview"></div>                
    </form>

<?php }else{ ?> 
    Database belum dipilih
<?php } ?>
<script>
$(document).ready(function() {
    $('.groupdate').css({'display':'none'});
    $('#groupref').css({'display':'none'});

}); 

</script>