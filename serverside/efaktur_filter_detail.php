<?php
session_start();
include"../inc/connect_pg.php";

$id = $_GET['id'];
$query = "SELECT * FROM(SELECT number AS value, id AS data,date_invoice,date_due FROM account_invoice WHERE state ='open' and number = '$id' LIMIT 5) A
	LEFT JOIN smcus_faktur_pajak B ON A.data = B.invoice_id
	LEFT JOIN (SELECT id,name as partner from res_partner) C ON B.partnerid = C.id
	";

$submit = pg_query($query);	
while ($result = pg_fetch_assoc($submit)) 
{
	$inv_number = $result['value'];
	$inv_date = $result['date_invoice'];
	$due_date = $result['date_due'];
	$no_faktur = $result['name'];
	$partner = $result['partner'];
	$faktur_date = $result['date_faktur'];
	$masa = $result['masa_pajak'];
	$tahun = $result['tahun_pajak'];
	$npwp = $result['npwp'];
	$ttd = $result['ttd'];
}
	                 
if(!isset($inv_number)){ ?>
	 <label for="inputEmail3" class="control-label">Invoice tidak ditemukan </label>  <?php
}else{

	?>
	<div class="nav-tabs-custom" >
	                <!-- Tabs within a box -->
	<ul class="nav nav-tabs pull-right ui-sortable-handle">
	  <li class="active" onClick="tab_content('new');"><a href="#revenue-chart" data-toggle="tab">Form</a></li>
	 
	  <li class="pull-left header"><i class="fa fa-inbox"></i> Detail : <strong><?php echo $inv_number;?></strong></li>
	</ul>
		<div class="tab-content no-padding" >
		<!-- Morris chart - Sales -->
			<div class="chart tab-pane active" id="revenue-chart" style="height: 100%; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
				<div class="box">

	                  <div class="box-body">
	                  	<div class="col-md-6"> <!-- LEFT -->
						    <div class="form-group">
						      <label for="inputEmail3" class="control-label">No. Faktur Pajak </label> 
						      <input type="text" name="faktur" id="faktur" readonly class="form-control" value="<?php echo $no_faktur; ?>"/>
						    </div>	

						    <div class="form-group">
						      <label for="inputEmail3" class="control-label">Faktur date</label> 
						      <input type="text" name="faktur_date" id="faktur_date" readonly class="form-control" value="<?php echo $faktur_date; ?>"/>
						    </div>		
						    <div class="form-group">
						      <label for="inputEmail3" class="control-label">Invoice date </label> 
						      <input type="text" name="inv_date" id="inv_date" readonly class="form-control" value="<?php echo $inv_date; ?>"/>
						    </div>	
						    <div class="form-group">
						      <label for="inputEmail3" class="control-label">Tanda tangan</label> 
						      <input type="text" name="ttd" id="ttd" readonly class="form-control" value="<?php echo $ttd; ?>"/>
						    </div>							    					    								    

	                  	</div>
	                  	<div class="col-md-6"><!-- RIGHT -->
						    <div class="form-group">
						      <label for="inputEmail3" class="control-label">NPWP </label>
						      <input type="text" name="npwp" id="npwp" readonly class="form-control" value="<?php echo $npwp; ?>"/>
						    </div>
						    <div class="form-group">
						      <label for="inputEmail3" class="control-label">Partner </label>
						      <input type="text" name="partner" id="partner" readonly class="form-control" value="<?php echo $partner; ?>"/>
						    </div>	
						    <div class="form-group">
						      <label for="inputEmail3" class="control-label">Due date </label>
						      <input type="text" name="due_date" id="due_date" readonly class="form-control" value="<?php echo $due_date; ?>"/>
						    </div>	
						    <div class="form-group">
						      <label for="inputEmail3" class="control-label">Masa dan Tahun pajak </label>
						      <div class="row">
							      <div class="col-xs-6 col-md-6">
							      	<input type="text" name="masa" id="masa" readonly class="form-control" value="<?php echo $masa; ?>"/>
							      </div>
							      <div class="col-xs-6 col-md-6">
							      	<input type="text" name="tahun" id="tahun" readonly class="form-control" value="<?php echo $tahun; ?>"/>
							      </div>
						      </div>
						      
						    </div>	 						                      	
	                  	</div>
	                  	
	                  </div><!-- /.box-body -->				    
	                  <div class="box-footer">
	                  	<div class="btn btn-flat btn-success pull-right" onClick="Excel('<?php echo $inv_number; ?> ');">Generate</div>
	                  </div>
	
				 </div>			
			</div>
		</div>
	</div>	
<?php } ?>
<script>
function Excel(id){
	//alert(id);
	window.location.href = "inc/efaktur_export.php?id="+id;
}
</script>