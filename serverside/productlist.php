<?php
	session_start();
	$conn_string = "host=".$_SESSION['host']." dbname=".$_SESSION['db']." user=".$_SESSION['user_db']." password=".$_SESSION['pass_db']."";
	$dbconn = pg_connect($conn_string);

	$requestData= $_REQUEST;
	$columns = array(
		0=>'default_code',
		1=>'name_template',
		2=>'stock'

	);
	$sql = "select * from (select A.default_code,A.name_template,A.id from product_product A, product_template B where A.product_tmpl_id = B.id AND B.categ_id ='32' ORDER BY A.default_code) Z 
		LEFT JOIN ( SELECT product_id,SUM(product_qty) as masuk FROM stock_move where location_dest_id = '16' and state='done' GROUP BY product_id) 
		Y ON Z.id = Y.product_id 
		LEFT JOIN ( SELECT product_id,SUM(product_qty) as keluar FROM stock_move where location_id = '16' and state='done' GROUP BY product_id) 
		X ON X.product_id = Z.id ";
	$query=pg_query($dbconn, $sql) or die("employee-grid-data.php: get employees1");
	$totalData = pg_num_rows($query);
	$totalFiltered = $totalData;
/*	if( !empty($requestData['search']['value']) )
	{
		$sql = "select * from (select A.default_code,A.name_template,A.id from product_product A, product_template B where A.product_tmpl_id = B.id AND B.categ_id ='32' AND (A.default_code LIKE '%".$requestData['search']['value']."%' OR A.name_template LIKE '%".$requestData['search']['value']."%' ) ) Z ";
		$sql.=" LEFT JOIN ( SELECT product_id,SUM(product_qty) as masuk FROM stock_move where location_dest_id = '16' and state='done' GROUP BY product_id)";
		$sql.=" Y ON Z.id = Y.product_id ";
		$sql.=" LEFT JOIN ( SELECT product_id,SUM(product_qty) as keluar FROM stock_move where location_id = '16' and state='done' GROUP BY product_id) ";
		$sql.=" X ON X.product_id = Z.id ";
		$query=pg_query($dbconn, $sql) or die("employee-grid-data.php: get employeesas");
		$totalFiltered = pg_num_rows($query);
		$sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
		$query=pg_query($dbconn, $sql) or die("employee-grid-data.php: get employees2");
	}
	else
	{
		$sql = "select * from (select A.default_code,A.name_template,A.id from product_product A, product_template B where A.product_tmpl_id = B.id AND B.categ_id ='32'  ) Z ";
		$sql.=" LEFT JOIN ( SELECT product_id,SUM(product_qty) as masuk FROM stock_move where location_dest_id = '16' and state='done' GROUP BY product_id";
		$sql.=" Y ON Z.id = Y.product_id";
		$sql.=" LEFT JOIN ( SELECT product_id,SUM(product_qty) as keluar FROM stock_move where location_id = '16' and state='done' GROUP BY product_id) ";
		$sql.=" X ON X.product_id = Z.id ";
		$sql.=" ORDER BY ".$columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
		$query=pg_query($dbconn, $sql) or die("employee-grid-data.php: get employees3");
	}
	*/
	$data = array();
	$no=1;
	while( $row=pg_fetch_array($query) )
	{
		$nestedData=array();
		//$nestedData[] = $no;
		$nestedData[] = $row["default_code"];
		$nestedData[] = $row["name_template"];
		$nestedData[] = 'stock';
		//$nestedData[] = $row["categoryname"];
		/*$nestedData[] = preg_replace('/[\x00-\x1F\x80-\xFF]/', '',$row["descshort"]);
		$nestedData[] = "
							<a class='fancybox' href='../assets/img/product/".$row["img"]."'>
								<img src='../assets/img/product/thumbnail/".$row["img"]."' class='img-responsive' style='width:50px; height:auto'>
							</a>
						";
		$nestedData[] = "Rp".number_format($row["price"], 0 , '' , '.' ).",-";
		$nestedData[] = $row["variant"];
		$nestedData[] =
						"
							<div class='divbtnproduct'>
						        <a href='ajax/detailimg.php?id=".$row['id']."' data-hint='Update Image' class='hint--bottom fancybox fancybox.ajax'>
						            <button class='btn btn-info'>
						                <span class='fa fa-picture-o'></span>
						            </button>
						        </a>
						        <a href='ajax/detailproduct.php?id=".$row['id']."' data-hint='Update Product' data-toggle='confirmation' class='confirmbtn hint--bottom fancybox fancybox.ajax'>
						            <button class='btn btn-success'>
						                <span class='fa fa-edit'></span>
						            </button>
						        </a>
										<a href='ajax/variantproduct.php?id=".$row['id']."' data-hint='Variant Product' data-toggle='confirmation' class='confirmbtn hint--bottom fancybox fancybox.ajax'>
						            <button class='btn btn-warning'>
						                <span class='fa fa-list'></span>
						            </button>
						        </a>
				            <div class='btn btn-danger hint--bottom' data-hint='Delete Product' onclick=\"dltprd(".$row['id'].",'".$row['name']."')\">
				                <span class='fa fa-remove'></span>
				            </div>
						    </div>
						";
		*/$data[] = $nestedData;
		$no++;
	}
	$json_data = array
	(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalData ),
			"recordsFiltered" => intval( $totalFiltered ),
			"data"            => $data
	);
	echo json_encode($json_data);
?>
