<?php
session_start();
if($_GET['db'] != ''){
	$conn_string = "host=".$_SESSION['host']." dbname=".$_GET['db']." user=".$_SESSION['user_db']." password=".$_SESSION['pass_db']."";
	$dbconn = pg_connect($conn_string);

	?>
	<form class="form-horizontal" action="inc/excel_po_category_detail.php" method="POST" enctype="multipart/form-data">
		<div class="box-body">	
			<div class="col-md-6">
				<div class="form-group">
				  <label for="inputEmail3" class="col-sm-4 control-label">Parent Filter ?</label>
				  <div class="col-sm-8">
						<label>
						  <input type="checkbox" onClick="CheckthisBox();" id="parent" value="1" name="parent">
	<input type="hidden"  readonly class="form-control" id="parent_field" name="parent_field" placeholder="Reference Number">					  
						</label>
					<!--input type="file" class="form-control" id="date_start" required name="date_start" -->
				  </div>
				</div>	
				<div class="form-group" id="parent_div">
				  <label for="inputEmail3" class="col-sm-4 control-label">Parent Category</label>
				  <div class="col-sm-8">
					<select class="form-control" id="categ_parent" name="categ_parent">
					<option value="">--- Pilih Category ---</option>
					<?php 
						$no = 0;
						$ll = pg_query($dbconn,"SELECT * FROM res_partner_category where parent_id IS NULL ORDER BY name");
						while($llist = pg_fetch_assoc($ll)){ 
						$no++;?>
							<option value="<?php echo $llist['id'];?>"><?php echo $llist['name'];?></option>
						<?php 	
						}
					?>
					</select>
					<!--input type="file" class="form-control" id="date_start" required name="date_start" -->
				  </div>
				</div>		
				<div class="form-group" id="child_div">
				  <label for="inputEmail3" class="col-sm-4 control-label">Child Category</label>
				  <div class="col-sm-8">
					<select class="form-control" id="categ" name="categ">
					<option value="">--- Pilih Category ---</option>
					<?php 
						$no = 0;
						$l = pg_query($dbconn,"SELECT * FROM res_partner_category where parent_id IS NOT NULL ORDER BY name");
						while($list = pg_fetch_assoc($l)){ 
						$no++;?>
							<option value="<?php echo $list['id'];?>"><?php echo $list['name'];?></option>
						<?php 	
						}
					?>
					</select>
					<!--input type="file" class="form-control" id="date_start" required name="date_start" -->
				  </div>
				</div>				
			</div> 
			<div class="col-md-6">
				<div class="form-group" id="groupref">
				  <label for="inputEmail3" class="col-sm-4 control-label">Date Start</label>
				  <div class="col-sm-8">
					<input type="hidden" value="<?php echo $_GET['db'];?>" readonly class="form-control" id="db" name="db" placeholder="Reference Number">

				 	 <input type="date" class="form-control" id="date_start" required name="date_start" >
					<!--input type="file" class="form-control" id="date_start" required name="date_start" -->
				  </div>
				</div>	
				<div class="form-group" id="groupref">
				  <label for="inputEmail3" class="col-sm-4 control-label">Date End</label>
				  <div class="col-sm-8">
				 	 <input type="date" class="form-control" id="date_end" required name="date_end" >
					<!--input type="file" class="form-control" id="date_start" required name="date_start" -->
				  </div>
				</div>					
			</div>
		</div>
		<div class="box-footer">   
			<div onclick="print_test();" class="btn btn-flat btn-success pull-right" class="btn btn-success btn-flat pull-right"><i class="glyphicon glyphicon-download"></i>Print !</div>			<!--div onclick="PDF();" class="btn btn-flat btn-success pull-right"  class="btn btn-success btn-flat pull-right"><i class="glyphicon glyphicon-download"></i>Print !</div-->

			<button type="submit" class="btn btn-success btn-flat pull-right"><i class="glyphicon glyphicon-download"></i>EXCEL !</button> 
			<div onclick="previewCatDetail();" class="btn btn-flat btn-success pull-right" class="btn btn-success btn-flat pull-right"><i class="glyphicon glyphicon-download"></i>Preview !</div>




			

		</div>
	</form>
	<div id="preview_reports"></div>
	<div id="reports">
	</div>
<?php }else{ ?> 
	Database belum dipilih
<?php }	?>
<script>

$(document).ready(function() {
	if ($('#parent').is(":checked"))
	{
	  $('#parent_div').css({'display':''});
	  $('#child_div').css({'display':'none'});
	}else{
	  $('#parent_div').css({'display':'none'});
	  $('#child_div').css({'display':''});
	}
	  $('#reports').css({'display':'none'});

});	

function print_test(){
	var state = $('#state').val();
	var db = $('#db').val(); 
	var cat = $('#categ').val(); 
	var cat_parent = $('#categ_parent').val(); 
	var start = $('#date_start').val(); 
	var end = $('#date_end').val();
	var type = $('#type').val(); 	 
	//var parent = $('#parent').val();
	if ($('#parent').is(":checked"))
	{
	  var parent = 'on';
	}else{
	  var parent = '';
	}	 
	if(end == '' || start == ''){
		alert("Tanggal belum diisi");
	}else{	 

		var url="report/category_po_detail_report.php?cat="+cat+"&start="+start+"&end="+end+"&db="+db+"&parent="+parent+"&cat_parent="+cat_parent;
	    var win = window.open(url, '_blank');
	    win.focus();		
	}	

}
function DetailAccordion(id){
	var start = $('#date_start').val(); 
	var end = $('#date_end').val();
	var type = $('#type').val(); 	 
	var db = $('#db').val(); 
	var state = $('#state').val();

	$('#accordion_detail_'+id).load("serverside/category_po_detail_accordion.php?id="+id+"&start="+start+"&end="+end+"&db="+db);	

}
function previewCatDetail(){
//	var state = $('#state').val();
	var db = $('#db').val(); 
	var cat = $('#categ').val(); 
	var cat_parent = $('#categ_parent').val(); 
	var start = $('#date_start').val(); 
	var end = $('#date_end').val();
//	var type = $('#type').val(); 	 
	//var parent = $('#parent').val();
	if ($('#parent').is(":checked"))
	{
	  var parent = 'on';
	}else{
	  var parent = '';
	}	 
	if(end == '' || start == ''){
		alert("Tanggal belum diisi");
	}else{	 
		$('#preview_reports').load("serverside/category_po_detail_preview.php?cat="+cat+"&start="+start+"&end="+end+"&db="+db+"&parent="+parent+"&cat_parent="+cat_parent);	
	}

}
function CheckthisBox(){
	if ($('#parent').is(":checked"))
	{
	  $('#parent_div').css({'display':''});
	  $('#child_div').css({'display':'none'});
	  $('#parent_field').val('on'); 
	  $('#categ').val(''); 
	}else{
	  $('#parent_div').css({'display':'none'});
	  $('#child_div').css({'display':''});
	  $('#parent_field').val('');
	  $('#categ_parent').val(''); 
	}
}

function PDF(){
	var state = $('#state').val();
	var db = $('#db').val(); 
	var cat = $('#categ').val(); 
	var cat_parent = $('#categ_parent').val(); 
	var start = $('#date_start').val(); 
	var end = $('#date_end').val();
	var type = $('#type').val(); 	 
	//var parent = $('#parent').val();
	if ($('#parent').is(":checked"))
	{
	  var parent = 'on';
	}else{
	  var parent = '';
	}	 
	if(end == '' || start == ''){
		alert("Tanggal belum diisi");
	}else{	 

		$('#reports').load("report/category_detail_report.php?cat="+cat+"&start="+start+"&end="+end+"&db="+db+"&type="+type+"&parent="+parent+"&cat_parent="+cat_parent+"&state="+state);
	
	}

}
</script>