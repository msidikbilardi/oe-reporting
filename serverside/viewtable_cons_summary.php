<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">

<?php 
include "../web_conf.php";


//drop and create table
	date_default_timezone_set('Asia/Jakarta');
	$localhostdb = '192.168.1.70';
	$usernamedb  = 'root';
	$passworddb  = 'Qwert!123';
	$databasedb  = 'oereporting';
		
	$con = mysqli_connect($localhostdb,$usernamedb,$passworddb,$databasedb)or die("cannot connect"); 

	$query = "DROP TABLE IF EXISTS `so_view".$_SERVER['REMOTE_ADDR']."`";
	$submit = mysqli_query($con,$query);

	$query = "CREATE TABLE `so_view".$_SERVER['REMOTE_ADDR']."` (`category_id` VARCHAR(10) NOT NULL,`category` VARCHAR(50) NOT NULL,`nominal` DOUBLE NOT NULL,`db` VARCHAR(50) NOT NULL) ENGINE=InnoDB";

	$submit = mysqli_query($con,$query);
//end drop and create table


//set filter
	if ($_GET['per'] == 'Q1'){
		$start = $_GET['thn'] . "-01-01";
		$end = $_GET['thn'] . "-03-31";
	}else if ($_GET['per'] == 'Q2'){
		$start = $_GET['thn'] . "-04-01";
		$end = $_GET['thn'] . "-06-30";
	}else if ($_GET['per'] == 'Q3'){
		$start = $_GET['thn'] . "-07-01";
		$end = $_GET['thn'] . "-09-30";
	}else if ($_GET['per'] == 'Q4'){
		$start = $_GET['thn'] . "-10-01";
		$end = $_GET['thn'] . "-12-31";
	}

	$so = "yes";// $_GET['so'];
	$db = "TOTALINPRO_2016"; //$_GET['db'];
	$type =  "out_invoice"; //$_GET['type'];
	$state =  "all"; //$_GET['state'];
	$type_report=  "so"; //$_GET['type_report'];
//end set filter


//start variable
	$sum = 0;
	$sumnom = 0;
	$filter = '';
	$filter_prn = '';
	$info = 'No Company Category';
	$no = 1;
	$cat = "";

    if($type_report == 'so'){
      $filter = "AND B.state != 'cancel' AND B.state != 'draft' AND B.state != 'sent'";
    }else{
      $filter = "AND B.state != 'cancel' AND (B.state = 'draft' OR B.state = 'sent' ) ";
    }
	$string_state = 'ALL SALES ORDER';
	$type_string = "Document : SO Category Consolidate Report " . $_GET['per'] . " - " . $_GET['thn'];

//end variable
	//loop database
	$values = '';
	$q_main = mysqli_query($con,"SELECT * FROM( SELECT * FROM `tblmasteruser_detail` where iduser = '".$_GET['usr']."' ) A LEFT JOIN tblmasterdatabase B ON A.dbid = B.id");
	while($main = mysqli_fetch_assoc($q_main)){
		if($main['nama'] == 'GBU'){
			$ip = $_SESSION['ip_vps'];
		}else{
			$ip = $_SESSION['ip_local'];
		}
		$conn_string = "host=".$ip." dbname=".$main['nama']." user=".$_SESSION['user_pg']." password=";
		$dbconn = pg_connect($conn_string);	
		$query = pg_query($dbconn,"SELECT W.id,W.name,sum(Z.sm_price_unit_after_disc3 * Z.product_uom_qty) as after3,sum(Z.sm_price_unit_before_disc * Z.product_uom_qty) as no_categ FROM(
            SELECT A.*,B.name,B.date_order FROM sale_order_line A,sale_order B where A.order_id = B.id $filter AND B.date_order BETWEEN '$start 00:00:00' AND '$end 23:59:59' AND B.user_id = '".$main['idoe']."'
            ) Z 
            LEFT JOIN (select id,default_code,product_tmpl_id from product_product) Y ON Z.product_id = Y.id
            LEFT JOIN (SELECT id,categ_id FROM product_template) X ON Y.product_tmpl_id = X.id
            LEFT JOIN product_category W ON W.id = X.categ_id
            GROUP BY W.id ORDER BY W.name");
        while($data = pg_fetch_assoc($query)){
	        if($data['id']){
	          $nm = $data['name'];
	          $nom = $data['after3'];
	        }else{
	          $nm = 'No Category';
	          $nom = $data['no_categ'];
	        }    
	        $categid = $data['id'];
	        $values = $values . "('$categid', '$nm', $nom, '".$main['second_name']."'),";    	
        	//echo $data['name'];
        }
		
		//echo $main['nama'].' - '.$values;
	}
		$values = substr($values,0,-1) . ";";
		$query = "INSERT INTO `so_view".$_SERVER['REMOTE_ADDR']."`(`category_id`, `category`, `nominal`, `db`) VALUES " . $values;
		//echo $query;
		$submit = mysqli_query($con,$query);	

//start display
?>
<div class="box box-solid">
	<div class="box-header with-border">
		<h3 class="box-title"><?php echo $string_state.' : '.$info.' ('.$start.' to '.$end.')'; ?></h3>
	</div><!-- /.box-header -->
	<div class="box-body">
		<table id="exampleTable" class="display" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>Name</th>
					<th>IDUser</th>
					<th>Category</th>
					<th>IDCat</th>
					<th>Total</th>
					<th>Target</th>
					<th>Selisih</th>
					<th>Pencapaian</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th colspan="7" style="text-align:right">Total:</th>
					<th></th>
					<th></th>
				</tr>
			</tfoot>
			<tbody> <?php
				$sq = "SELECT idusr, usr, target, tahun, idcat, nama, sum(nominal) nominal FROM (
				SELECT A.iduser idusr, A.nama usr, ". $_GET['per'] . " target, tahun, C.idcat idcat, C.nama, D1.second_name db, idoe, sum(nominal) nominal, (SELECT sum(nominal) nominal FROM tblmasteruser A INNER JOIN tblmastertarget B ON A.iduser = B.iduser 
				INNER JOIN tblmastercategory C ON B.idcat = C.idcat
				INNER JOIN tblmastercategory_detail D ON C.idcat = D.idcat
				INNER JOIN tblmasterdatabase D1 ON D.dbid = D1.id
				INNER JOIN `so_view".$_SERVER['REMOTE_ADDR']."` E ON E.category_id = D.idoe AND E.db = D1.second_name
				WHERE A.iduser = '" .$_GET['usr'] ."' AND tahun = " .$_GET['thn'] .") sumall FROM tblmasteruser A INNER JOIN tblmastertarget B ON A.iduser = B.iduser 
				INNER JOIN tblmastercategory C ON B.idcat = C.idcat
				INNER JOIN tblmastercategory_detail D ON C.idcat = D.idcat
				INNER JOIN tblmasterdatabase D1 ON D.dbid = D1.id
				INNER JOIN `so_view".$_SERVER['REMOTE_ADDR']."` E ON E.category_id = D.idoe AND E.db = D1.second_name
				WHERE A.iduser = '" .$_GET['usr'] ."' AND tahun = " .$_GET['thn'] ." GROUP BY A.nama, target, tahun, C.nama, D1.second_name, idoe) A
				GROUP BY idusr, usr, target, tahun, idcat, nama
				ORDER BY nama";

				$rs = mysqli_query($con,$sq);
				//==echo $q2;
				while($res = mysqli_fetch_assoc($rs)){ 
					$nom = $res['nominal'];
					if($nom != 0){
						?>
					<tr data-toggle="tooltip" title="Total <?php echo $res['usr'].' : '.number_format($res['nominal']); ?>">
						<td><?php echo $res['usr']; ?></td>
						<td><?php echo $res['idusr']; ?></td>
						<td><?php echo $res['nama']; ?></td>
						<td><?php echo $res['idcat']; ?></td>
						<td><?php echo number_format($res['nominal']) ?></td>
						<td><?php echo number_format($res['target']) ?></td>
						<td<?php 
								if ($res['target'] - $res['nominal'] < 0){
									echo " style='color:limegreen;'";
								}else{
									echo " style='color:red;'"; 
								}
							?>
						><strong><?php echo number_format($res['nominal'] - $res['target']) ?></strong></td>
						<td><?php echo number_format($res['nominal']/$res['target']*100) . ' %' ?></td>
					</tr>
				<?php 
					}
				}?>
			</tbody>
		</table>
	</div><!-- /.box-body -->
</div>

<div style="display:none">    
    <table id="detailsTable">
        <thead> 
            <tr>
                <th>Category</th>
                <th>Database</th>
                <th>Nominal</th>
            </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>
<!-- 
    <script src="assets/js/jquery.js"></script>
    <script src="assets/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/datatables.min.js" type="text/javascript"></script> -->

<script>
function convertToRupiah(angka){
    var rupiah = '';
    var angkarev = angka.toString().split('').reverse().join('');
    for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
    return rupiah.split('',rupiah.length-1).reverse().join('');
}

function fnFormatDetails(table_id, html) {
    var sOut = "<table id=\"exampleTable_" + table_id + "\">";
    sOut += html;
    sOut += "</table>";
    return sOut;
}

var iTableCounter = 1;
var oTable;
var oInnerTable;
var detailsTableHtml;

//Run On HTML Build
$(document).ready(function () {
	
	// you would probably be using templates here
	detailsTableHtml = $("#detailsTable").html();

	//Insert a 'details' column to the table
	var nCloneTh = document.createElement('th');
	var nCloneTd = document.createElement('td');
	nCloneTd.innerHTML = '<img src="http://i.imgur.com/SD7Dz.png">';
	nCloneTd.className = "center";

	$('#exampleTable thead tr').each(function () {
		this.insertBefore(nCloneTh, this.childNodes[0]);
	});

	$('#exampleTable tbody tr').each(function () {
		this.insertBefore(nCloneTd.cloneNode(true), this.childNodes[0]);
	});
	
   
	//Initialse DataTables, with no sorting on the 'details' column
	var oTable = $('#exampleTable').dataTable({
		"bJQueryUI": true,
		"bPaginate": false,
		"aoColumns": [
			{
			   "mDataProp": null,
			   "sClass": "control center",
			   "sDefaultContent": '<img src="http://i.imgur.com/SD7Dz.png">'
			},
			{ "mDataProp": "Name" },
			{ "mDataProp": "IDUser" },
			{ "mDataProp": "Category" },
			{ "mDataProp": "IDCat" },
			{ "mDataProp": "Total" },
			{ "mDataProp": "Target" },
			{ "mDataProp": "Selisih" },
			{ "mDataProp": "Pencapaian" }
		],
		"columnDefs": [
            {
                "targets": [ 2 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [ 4 ],
                "visible": false,
				"searchable": false
            }
        ],
		"aaSorting": [[1, 'asc']],
		"footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // Total over all pages
            total = api
                .column( 5 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Total over this page

            pageTotal = api
                .column( 5, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
			var skrg = intVal(a) + intVal(b);
                    return skrg;
                }, 0 );
 		
            // Update footer
		total = convertToRupiah(total);
		var nom = convertToRupiah(pageTotal);
            $( api.column( 7 ).footer() ).html(
                ''+nom+' ( '+ total +' total)'
            );
        }
    });

        /* Add event listener for opening and closing details
        * Note that the indicator for showing which row is open is not controlled by DataTables,
        * rather it is done here
        */
		
		var table = $('#exampleTable').DataTable();
		
		$('#exampleTable tbody td').on('click', 'img', function () {
            var nTr = $(this).parents('tr')[0];
            var nTds = this;
			
			var coldata = table.row( nTr ).data();
            
            if (oTable.fnIsOpen(nTr)) {
                /* This row is already open - close it */
                this.src = "http://i.imgur.com/SD7Dz.png";
                oTable.fnClose(nTr);
            }
            else {
                /* Open this row */
                var rowIndex = oTable.fnGetPosition( $(nTds).closest('tr')[0] ); 
			   
                this.src = "http://i.imgur.com/d4ICC.png";
                oTable.fnOpen(nTr, fnFormatDetails(iTableCounter, detailsTableHtml), 'details');
                oInnerTable = $("#exampleTable_" + iTableCounter).dataTable({
                    "bJQueryUI": true,
                    "bSort" : true, // disables sorting
                    "bPaginate": false,
					"ajax": {
						"url": 'serverside/viewtable_cons_summary_detail.php?idusr=' + coldata.IDUser + '&idcat=' + coldata.IDCat + '&thn=<?= $_GET['thn'] ?>&per=<?= $_GET['per'] ?>',
						"dataSrc": ""
					},
					"columns": [
						{ "data": "nama" },
						{ "data": "db" },
						{ "data": "nominal",
						  "render": $.fn.dataTable.render.number( ',', '.', 0, )						
						}
					],
                });
                iTableCounter = iTableCounter + 1;
            }
        });


    });
</script>