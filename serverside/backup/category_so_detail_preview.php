<?php
session_start();
$conn_string = "host=".$_SESSION['host']." dbname=".$_GET['db']." user=".$_SESSION['user_db']." password=".$_SESSION['pass_db']."";
$dbconn = pg_connect($conn_string);
$c = pg_query($dbconn,"SELECT * FROM res_company");
$comp = pg_fetch_array($c);
$db = $_GET['db'];
$start = $_GET['start'];
$end =  $_GET['end'];
$cat_parent =  $_GET['cat_parent'];
$cat =  $_GET['cat'];
$type_report =  $_GET['type_report'];
$parent =  $_GET['parent'];
//$state =  $_GET['state'];
if($parent == 'on'){
  $val = 'where Y.parent_id';
  $category = $cat_parent;
  $prt = 'Parent';
  $filter_prn = $val.'='.$category;
  $t = pg_query($dbconn,"SELECT * FROM res_partner_category where id='$category' ");
  $tags = pg_fetch_array($t);     
  $info = 'Company Category '.$prt.' : '.$tags['name'];
  $filter = '';

}elseif($parent != 'on' && $cat != ''){
  $val = 'AND a.msb_category';
  $category = $cat;
  $prt = '';
  $filter = $val.'='.$category;
  $t = pg_query($dbconn,"SELECT * FROM res_partner_category where id='$category' ");
  $tags = pg_fetch_array($t);
  $info = 'Company Category'.$prt.' : '.$tags['name'];  
  $filter_prn = '';
}else{
  $filter = '';
  $filter_prn = '';
  $info = 'No Company Category';
}


  if($type_report == 'so'){
    $state_list = "b.state != 'cancel' AND b.state != 'draft' AND b.state != 'sent'";
  
    $state_dt = "AND B.state != 'cancel' AND B.state != 'draft' AND B.state != 'sent'";
    $string_state = 'ALL SALES ORDER';
    $type_string = "Document : Sales Order Category Salesperson Report";

  }else{
    $state_list = "b.state != 'cancel' AND (b.state = 'draft' OR b.state = 'sent') ";

    $state_dt = "AND B.state != 'cancel' AND (B.state = 'draft' OR B.state = 'sent')";
    $string_state = 'DRAFT AND SENT QUOTATION';
    $type_string = "Document : Quotation Category Salesperson Report";
  }
  $state_dt = "E.state != 'cancel'";
 // $string_state = 'ALL SALES ORDER';

/*if($state == 'all'){
  $state_list = "b.state != 'cancel'";
  $state_dt = "E.state != 'cancel'";
  $string_state = 'ALL INVOICES';
}else{
  $state_list = "b.state = 'paid'";
  $state_dt = "E.state = 'paid'";
  $string_state = 'ONLY PAID INVOICES';
}

if($type == 'out_invoice'){
  $type_string = "Document : Customer Category Report";
}else{
  $type_string = "Document : Supplier Category Report";
}
*/  
$sum = 0;
$angka = 1;
$no = 1;
?>

<div class="box box-solid">
    <div class="box-header with-border">
      <h3 class="box-title"><?php echo $string_state.' : '.$info.' ('.$start.' to '.$end.')'; ?></h3>
    </div><!-- /.box-header -->
    <div class="box-body">

    <table id="example" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Name</th>
                <th>Category</th>
                <th>Subtotal</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th colspan="2" style="text-align:right">Total:</th>
                <th></th>
            </tr>
        </tfoot>
        <tbody> <?php
      $q = "
SELECT * FROM (
select * from (SELECT a.msb_category,a.name,COALESCE(sum(amount_untaxed),'0') as total, a.id
          FROM res_partner a, sale_order b
          WHERE b.currency_id = '13'  AND b.date_order >= '$start' AND b.date_order <= '$end'  $filter  AND $state_list AND b.partner_id = a.id 
          GROUP BY  a.name,a.id) Z LEFT JOIN (select id as cat_id,name as name_cat,parent_id from res_partner_category) Y ON Z.msb_category = Y.cat_id $filter_prn
) A LEFT JOIN (
			SELECT Z.id,name as cat,SUM(total) as final,
			SUM(not_diskon) as not_diskon 
			FROM
			(SELECT F.id,A.product_id ,sum(A.product_uom_qty*A.sm_price_unit_after_disc3) as total,SUM(A.product_uom_qty*A.price_unit) as not_diskon FROM sale_order_line A,sale_order B,res_partner F where B.partner_id = F.id AND B.date_order   >= '$start' AND B.date_order  <= '$end' AND B.date_order IS NOT NULL  AND B.currency_id = '13' AND $state_list AND A.order_id = B.id group by A.product_id,F.id) Z LEFT JOIN (SELECT A.id,C.name,A.default_code,A.name_template,A.product_tmpl_id FROM product_product A,product_template B,product_category C WHERE B.categ_id = C.id AND A.product_tmpl_id = B.id) Y ON Z.product_id = Y.id GROUP BY name,Z.id
) B ON A.id = B.id
";

    $l = pg_query($dbconn,$q);
    //echo $q;
    while($list = pg_fetch_assoc($l)){ 
			if($list['final'] == ''){
				$nom = $list['not_discount']; 
			}else{
				$nom = $list['final'];
			}
			if($nom != 0){
				?>
			<tr data-toggle="tooltip" title="Total <?php echo $list['partner_name'].' : '.number_format($list['total']); ?>">
				<td><?php echo $list['name']; ?></td>
				<td><?php echo $list['cat']; ?></td>
				<td><?php echo number_format($nom); ?></td>
			</tr>
		<?php 
    } }?>      
          <!--div class="panel box box-<?php echo $warna; ?>" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $list['id']; ?>" onClick="DetailAccordion('<?php echo $list['id'];?>','<?php echo $type_report;?>');" aria-expanded="false" class="collapsed">
            <div class="box-header with-border">
              <div class="box-title col-md-9"><h3 >
              
                 SUMMARY
                
              </h3></div>
              <span class="col-md-3"><h3><?php echo number_format($sum); ?></h3></span>
            </div>

          </div-->  
      </div>
    </div><!-- /.box-body -->
  </div>
    <script src="assets/js/jquery.js"></script>

    <script src="assets/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/datatables.js" type="text/javascript"></script>
    <script src="assets/datatables.min.js" type="text/javascript"></script>


<script>
function convertToRupiah(angka){
    var rupiah = '';
    var angkarev = angka.toString().split('').reverse().join('');
    for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
    return rupiah.split('',rupiah.length-1).reverse().join('');
}


$(document).ready(function() {
    $('#example').DataTable( {
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // Total over all pages
            total = api
                .column( 2 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Total over this page

            pageTotal = api
                .column( 2, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
			var skrg = intVal(a) + intVal(b);
                    return skrg;
                }, 0 );
 		
            // Update footer
		total = convertToRupiah(total);
		var nom = convertToRupiah(pageTotal);
            $( api.column( 2 ).footer() ).html(
                ''+nom+' ( '+ total +' total)'
            );
        }
    } );


} );
</script>