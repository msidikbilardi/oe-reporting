<?php
session_start();
$conn_string = "host=".$_SESSION['host']." dbname=".$_GET['db']." user=".$_SESSION['user_db']." password=".$_SESSION['pass_db']."";
$dbconn = pg_connect($conn_string);
$c = pg_query($dbconn,"SELECT * FROM res_company");
$comp = pg_fetch_array($c);
$db = $_GET['db'];
$start = $_GET['start'];
$end =  $_GET['end'];
$cat_parent =  $_GET['cat_parent'];
$cat =  $_GET['cat'];
$type =  $_GET['type'];
$parent =  $_GET['parent'];
$state =  $_GET['state'];
if($parent == 'on'){
	$val = 'where Y.parent_id';
	$category = $cat_parent;
	$prt = 'Parent';
	$filter_prn = $val.'='.$category;
	$t = pg_query($dbconn,"SELECT * FROM res_partner_category where id='$category' ");
	$tags	= pg_fetch_array($t);			
	$info = 'Company Category'.$prt.' : '.$tags['name'];
	$filter = '';

}elseif($parent != 'on' && $cat != ''){
	$val = 'AND a.msb_category';
	$category = $cat;
	$prt = '';
	$filter = $val.'='.$category;
	$t = pg_query($dbconn,"SELECT * FROM res_partner_category where id='$category' ");
	$tags	= pg_fetch_array($t);
	$info = 'Company Category'.$prt.' : '.$tags['name'];	
	$filter_prn = '';
}else{
	$filter = '';
	$filter_prn = '';
	$info = 'No Company Category';
}
if($state == 'all'){
	$state_list = "b.state != 'cancel'";
	$state_dt = "B.state != 'cancel'";
	$string_state = 'ALL INVOICES';
}else{
	$state_list = "b.state != 'cancel' AND b.state = 'paid'";
	$state_dt = "B.state != 'cancel' AND B.state = 'paid'";
	$string_state = 'ONLY PAID INVOICES';
}
if($type == 'out_invoice'){
	$type_string = "Document : Customer Category Report";
}else{
	$type_string = "Document : Supplier Category Report";
}	
$sum = 0;
$angka = 1;
$no = 1;
?>

<div class="box box-solid">
    <div class="box-header with-border">
      <h3 class="box-title"><?php echo $type_string.' '.$string_state.' : '.$info.' ('.$start.' to '.$end.')'; ?></h3>
    </div><!-- /.box-header -->
    <div class="box-body">



      <!--div class="box-group" id="accordion">
<?php
	    $q = "select * from (SELECT a.msb_category,a.name,COALESCE(sum(amount_untaxed),'0') as total, a.id
	        FROM res_partner a, account_invoice b
	        WHERE b.currency_id = '13'  AND b.date_invoice >= '$start' AND b.date_invoice <= '$end'  $filter AND b.type = '$type' AND $state_list AND b.partner_id = a.id 
	        GROUP BY  a.name,a.id) Z LEFT JOIN (select id as cat_id,name as name_cat,parent_id from res_partner_category) Y ON Z.msb_category = Y.cat_id $filter_prn";

		$l = pg_query($dbconn,$q);
		//echo $q;
		while($list = pg_fetch_assoc($l)){ 
			if($angka == 1){
				$warna ='info';
			}elseif($angka == 2){
				$warna ='danger';
			}elseif($angka == 3){
				$warna ='success';
			}
			?>

	        <div class="panel box box-<?php echo $warna; ?>" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $list['id']; ?>" onClick="DetailAccordion('<?php echo $list['id'];?>');" aria-expanded="false" class="collapsed">
	          <div class="box-header with-border">
	            <h4 class="box-title col-md-10">
	              <a >
	                <?php echo $no.'. '.$list['name']; ?>
	              </a>
	            </h4>
	            <span class="col-md-2"><?php echo number_format($list['total']); ?></span>
	          </div>
	          <div id="collapse<?php echo $list['id']; ?>" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
	          	<div class="box-body">
	          		<div id="accordion_detail_<?php echo $list['id']; ?>"></div>
	            </div>
	          </div>
	        </div>		
		<?php  
		$sum += $list['total'];
		$no++;
		if($angka > 2){
			$angka = 1;
		}else{
			$angka++;
		}
		 } ?>      
	        <div class="panel box box-<?php echo $warna; ?>" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $list['id']; ?>" onClick="DetailAccordion('<?php echo $list['id'];?>');" aria-expanded="false" class="collapsed">
	          <div class="box-header with-border">
	            <div class="box-title col-md-9"><h3 >
	            
	               SUMMARY
	              
	            </h3></div>
	            <span class="col-md-3"><h3><?php echo number_format($sum); ?></h3></span>
	          </div>

	        </div>	
      </div-->
    <table id="example" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Name</th>
                <th>Category</th>
                <th>Subtotal</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th colspan="2" style="text-align:right">Total:</th>
                <th></th>
            </tr>
        </tfoot>
        <tbody>
<?php
	    $q = "
select * from (
select * from (SELECT a.msb_category,a.name as partner_name,COALESCE(sum(amount_untaxed),'0') as total, a.id
	        FROM res_partner a, account_invoice b
	        WHERE b.currency_id = '13'  AND b.date_invoice >= '$start' AND b.date_invoice <= '$end'  $filter AND b.type = '$type' AND $state_list AND b.partner_id = a.id 
	        GROUP BY  a.name,a.id) Z LEFT JOIN (select id as cat_id,name as name_cat,parent_id from res_partner_category) Y ON Z.msb_category = Y.cat_id $filter_prn
) A left join (
	SELECT Z.id,name,SUM(total) as final,
	SUM(not_diskon) as not_diskon 
	FROM
	(SELECT F.id,A.product_id ,sum(A.quantity*A.sm_price_unit_after_disc3) as total,SUM(A.quantity*A.price_unit) as not_diskon FROM account_invoice_line A,account_invoice B,res_partner F where B.partner_id = F.id AND B.date_invoice   >= '$start' AND B.date_invoice  <= '$end' AND B.date_invoice IS NOT NULL  AND B.currency_id = '13'AND B.type = '$type'  AND $state_dt AND A.invoice_id = B.id group by A.product_id,F.id) Z LEFT JOIN (SELECT A.id,C.name,A.default_code,A.name_template,A.product_tmpl_id FROM product_product A,product_template B,product_category C WHERE B.categ_id = C.id AND A.product_tmpl_id = B.id) Y ON Z.product_id = Y.id GROUP BY name,Z.id) B ON B.id = A.id";

		$l = pg_query($dbconn,$q);
		//echo $q;
		while($list = pg_fetch_assoc($l)){ 
			if($list['final'] == ''){
				$nom = $list['not_discount']; 
			}else{
				$nom = $list['final'];
			}
			if($nom != 0){

				?>
			<tr data-toggle="tooltip" title="Total <?php echo $list['partner_name'].' : '.number_format($list['total']); ?>">
				<td><?php echo $list['partner_name']; ?></td>
				<td><?php echo $list['name']; ?></td>
				<td><?php echo number_format($nom); ?></td>
			</tr>
		<?php 
			}
		}
?>
        </tbody>
    </table>
    </div><!-- /.box-body -->
  </div>
    <script src="assets/js/jquery.js"></script>

    <script src="assets/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/datatables.js" type="text/javascript"></script>
    <script src="assets/datatables.min.js" type="text/javascript"></script>


<script>
function convertToRupiah(angka){
    var rupiah = '';
    var angkarev = angka.toString().split('').reverse().join('');
    for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
    return rupiah.split('',rupiah.length-1).reverse().join('');
}


$(document).ready(function() {
    $('#example').DataTable( {
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // Total over all pages
            total = api
                .column( 2 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Total over this page

            pageTotal = api
                .column( 2, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
			var skrg = intVal(a) + intVal(b);
                    return skrg;
                }, 0 );
 		
            // Update footer
		total = convertToRupiah(total);
		var nom = convertToRupiah(pageTotal);
            $( api.column( 2 ).footer() ).html(
                ''+nom+' ( '+ total +' total)'
            );
        }
    } );


} );
</script>