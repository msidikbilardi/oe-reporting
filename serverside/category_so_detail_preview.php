<?php
session_start();
$conn_string = "host=".$_SESSION['host']." dbname=".$_GET['db']." user=".$_SESSION['user_db']." password=".$_SESSION['pass_db']."";
$dbconn = pg_connect($conn_string);
$c = pg_query($dbconn,"SELECT * FROM res_company");
$comp = pg_fetch_array($c);
$db = $_GET['db'];
$start = $_GET['start'];
$end =  $_GET['end'];
$cat_parent =  $_GET['cat_parent'];
$cat =  $_GET['cat'];
$type_report =  $_GET['type_report'];
$parent =  $_GET['parent'];
//$state =  $_GET['state'];
if($parent == 'on'){
  $val = 'c.parent_id';
  $category = $cat_parent;
  $var = 'c.parent_id = '.$category." AND ";


  $prt = 'Parent';
  $filter = $val.'='.$category." AND ";
  $t = pg_query($dbconn,"SELECT * FROM res_partner_category where id='$category' ");
  $tags = pg_fetch_array($t);     
  $info = 'Company Category '.$prt.' : '.$tags['name'];
  $filter = '';

}elseif($parent != 'on' && $cat != ''){
  $val = 'a.msb_category';
  $category = $cat;
	$var = 'a.msb_category = '.$category." AND ";
  $prt = '';
  $filter = $val.'='.$category." AND ";
  $t = pg_query($dbconn,"SELECT * FROM res_partner_category where id='$category' ");
  $tags = pg_fetch_array($t);
  $info = 'Company Category'.$prt.' : '.$tags['name'];  
  $filter_prn = '';
}else{
$var = 'c';
  $filter = '';
  $filter_prn = '';
  $info = 'No Company Category';
}


  if($type_report == 'so'){
    $state_list = "b.state != 'cancel' AND b.state != 'draft' AND b.state != 'sent'";
  
    $state_dt = "AND state != 'draft' AND state != 'sent' AND state != 'cancel' ";
    $string_state = 'ALL SALES ORDER';
    $type_string = "Document : Sales Order Category Salesperson Report";

  }else{
    $state_list = "b.state != 'cancel' AND (b.state = 'draft' OR b.state = 'sent') ";

    $state_dt = "AND state != 'cancel' AND (state = 'draft' OR state = 'sent')";
    $string_state = 'ALL QUOTATION';
    $type_string = "Document : Quotation Category Salesperson Report";
  }
  //$state_dt = "E.state != 'cancel'";
 // $string_state = 'ALL SALES ORDER';

/*if($state == 'all'){
  $state_list = "b.state != 'cancel'";
  $state_dt = "E.state != 'cancel'";
  $string_state = 'ALL INVOICES';
}else{
  $state_list = "b.state = 'paid'";
  $state_dt = "E.state = 'paid'";
  $string_state = 'ONLY PAID INVOICES';
}

if($type == 'out_invoice'){
  $type_string = "Document : Customer Category Report";
}else{
  $type_string = "Document : Supplier Category Report";
}
*/ 
if($type_report == 'so'){
  $filter_detail = "AND B.state != 'cancel' AND B.state != 'draft' AND B.state != 'sent'";
}else{
  $filter_detail = "AND B.state != 'cancel' AND (B.state = 'draft' or B.state = 'sent' )";
}

$sum = 0;
$angka = 1;
$no = 1;
?>

<div class="box box-solid">
    <div class="box-header with-border">
      <h3 class="box-title"><?php echo $string_state.' : '.$info.' ('.$start.' to '.$end.')'; ?></h3>
    </div><!-- /.box-header -->
    <div class="box-body">

    <table id="example" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Name</th>
                <th>Category</th>
                <th>Subtotal</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th colspan="2" style="text-align:right">Total:</th>
                <th></th>
            </tr>
        </tfoot>
        <tbody> <?php
      $q = "
SELECT * FROM (
select * from (SELECT a.msb_category,a.name,COALESCE(sum(amount_untaxed),'0') as total, a.id
          FROM res_partner a, sale_order b
          WHERE b.currency_id = '13'  AND b.date_order >= '$start' AND b.date_order <= '$end'  $filter  AND $state_list AND b.partner_id = a.id 
          GROUP BY  a.name,a.id) Z LEFT JOIN (select id as cat_id,name as name_cat,parent_id from res_partner_category) Y ON Z.msb_category = Y.cat_id $filter_prn
) A LEFT JOIN (
			SELECT Z.id,name as cat,SUM(total) as final,
			SUM(not_diskon) as not_diskon 
			FROM
			(SELECT F.id,A.product_id ,sum(A.product_uom_qty*A.sm_price_unit_after_disc3) as total,SUM(A.product_uom_qty*A.price_unit) as not_diskon FROM sale_order_line A,sale_order B,res_partner F where B.partner_id = F.id AND B.date_order   >= '$start' AND B.date_order  <= '$end' AND B.date_order IS NOT NULL  AND B.currency_id = '13' AND $state_list AND A.order_id = B.id group by A.product_id,F.id) Z LEFT JOIN (SELECT A.id,C.name,A.default_code,A.name_template,A.product_tmpl_id FROM product_product A,product_template B,product_category C WHERE B.categ_id = C.id AND A.product_tmpl_id = B.id) Y ON Z.product_id = Y.id GROUP BY name,Z.id
) B ON A.id = B.id
";


$q2 = "
SELECT * FROM(
SELECT Z.partner_id,W.id,W.name ,sum(Z.sm_price_unit_after_disc3 * Z.product_uom_qty) as after3,sum(Z.sm_price_unit_before_disc * Z.product_uom_qty) as no_categ FROM(
            SELECT A.*,B.name,B.date_order,B.partner_id FROM sale_order_line A,sale_order B where A.order_id = B.id $filter_detail AND B.date_order BETWEEN '$start 00:00:00' AND '$end 23:59:59'
            ) Z 
            LEFT JOIN (select id,default_code,product_tmpl_id from product_product) Y ON Z.product_id = Y.id
            LEFT JOIN (SELECT id,categ_id FROM product_template) X ON Y.product_tmpl_id = X.id
            LEFT JOIN product_category W ON W.id = X.categ_id
            GROUP BY W.id,Z.partner_id ORDER BY W.name
            ) J INNER JOIN
            (SELECT a.msb_category,a.name as partner_name, a.id FROM res_partner a, res_partner_category c WHERE $var a.msb_category = c.id GROUP BY a.name,a.id) K ON J.partner_id = K.id
";
    $l = pg_query($dbconn,$q2);
    //echo $var;
//    echo $q2;
    while($list = pg_fetch_assoc($l)){ 
        if($list['id']){
          $nm = $list['name'];
          $nom = $list['after3'];
        }else{
          $nm = 'No Category';
          $nom = $list['no_categ'];
        }			
				//$nom = $list['final'];
			
			if($nom != 0){
				?>
			<tr data-toggle="tooltip" title="<?php echo $list['partner_name']; ?>">
				<td><?php echo $list['partner_name']; ?></td>
				<td><?php echo $nm; ?></td>
				<td><?php echo number_format($nom); ?></td>
			</tr>
		<?php 
    } }?>      
          <!--div class="panel box box-<?php echo $warna; ?>" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $list['id']; ?>" onClick="DetailAccordion('<?php echo $list['id'];?>','<?php echo $type_report;?>');" aria-expanded="false" class="collapsed">
            <div class="box-header with-border">
              <div class="box-title col-md-9"><h3 >
              
                 SUMMARY
                
              </h3></div>
              <span class="col-md-3"><h3><?php echo number_format($sum); ?></h3></span>
            </div>

          </div-->  
      </div>
    </div><!-- /.box-body -->
  </div>
    <script src="assets/js/jquery.js"></script>

    <script src="assets/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/datatables.js" type="text/javascript"></script>
    <script src="assets/datatables.min.js" type="text/javascript"></script>


<script>
function convertToRupiah(angka){
    var rupiah = '';
    var angkarev = angka.toString().split('').reverse().join('');
    for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
    return rupiah.split('',rupiah.length-1).reverse().join('');
}


$(document).ready(function() {
    $('#example').DataTable( {
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // Total over all pages
            total = api
                .column( 2 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Total over this page

            pageTotal = api
                .column( 2, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
			var skrg = intVal(a) + intVal(b);
                    return skrg;
                }, 0 );
 		
            // Update footer
		total = convertToRupiah(total);
		var nom = convertToRupiah(pageTotal);
            $( api.column( 2 ).footer() ).html(
                ''+nom+' ( '+ total +' total)'
            );
        }
    } );


} );
</script>